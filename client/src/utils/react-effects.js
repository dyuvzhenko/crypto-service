// @flow
import { useEffect } from 'react'

const RESIZE_THROTTLE_DELAY = 400
const MOUSE_WHEEL_THROTTLE_DELAY = 50

export const useOutsideEffectByRef = (ref: Object, handle: Function, shouldApplyEffect: boolean) => {
  function handleClickOutside(event: Object) {
    if (ref.current && !ref.current.contains(event.target)) {
      handle()
    }
  }

  useEffect(() => {
    if (shouldApplyEffect) {
      document.addEventListener('mousedown', handleClickOutside)
    }
    return () => document.removeEventListener('mousedown', handleClickOutside)
  })
}

export const useResizeEffectByRef = (ref: Object, callback: Function) => {
  let throttled = false

  function handleResize() {
    if (!throttled) {
      callback({ width: window.innerWidth, height: window.innerHeight })
      throttled = true
      setTimeout(() => {
        throttled = false
      }, RESIZE_THROTTLE_DELAY)
    }
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  })
}


export const useMouseWheelEffect = (callback: Function) => {
  let throttled = false

  function handleMouseWheel(event: Object) {
    if (!throttled) {
      callback(event)
      throttled = true
      setTimeout(() => {
        throttled = false
      }, MOUSE_WHEEL_THROTTLE_DELAY)
    }
  }

  useEffect(() => {
    window.addEventListener('wheel', handleMouseWheel)
    window.addEventListener('touchmove', handleMouseWheel)
    return () => {
      window.removeEventListener('wheel', handleMouseWheel)
      window.removeEventListener('touchmove', handleMouseWheel)
    }
  })
}
