// @flow
import {
  GRID_LOOK,
  LOCAL_STORAGE_ITEM,
  PAPER_CURRENCY, PAPER_CURRENCY_ARRAY,
  MARKET_DATA_RANGE, MARKET_DATA_RANGE_ARRAY
} from '@/utils/constants'

export const getToken = () => localStorage.getItem(LOCAL_STORAGE_ITEM.TOKEN)
export const removeToken = () => localStorage.removeItem(LOCAL_STORAGE_ITEM.TOKEN)
export const setToken = (token: string) => localStorage.setItem(LOCAL_STORAGE_ITEM.TOKEN, token)

export const isThemeDark = () => localStorage.getItem(LOCAL_STORAGE_ITEM.IS_THEME_DARK)
export const setThemeDark = (isSet: string) => localStorage.setItem(LOCAL_STORAGE_ITEM.IS_THEME_DARK, isSet)

export const getGraphGridShowed = (itemName: string) => localStorage.getItem(itemName) === 'true'

export const getGridLook = (itemName: string) =>
  localStorage.getItem(itemName) === GRID_LOOK.ROWS
    ? GRID_LOOK.ROWS
    : GRID_LOOK.BLOCKS

export const getVsCurrency = (itemName: string) => {
  const vsCurrency = localStorage.getItem(itemName)
  return vsCurrency && PAPER_CURRENCY_ARRAY.some(({ id }: { id: string }) => id === vsCurrency)
    ? vsCurrency
    : PAPER_CURRENCY.USD.id
}

export const getRangeName = (itemName: string) => {
  const rangeName = localStorage.getItem(itemName)
  return rangeName && MARKET_DATA_RANGE_ARRAY.some(({ name }: { name: string }) => name === rangeName)
    ? rangeName
    : MARKET_DATA_RANGE.THREE_MONTHES.name
}
