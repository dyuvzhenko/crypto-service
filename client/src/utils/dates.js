// @flow
/* eslint-disable no-magic-numbers */
import dayjs from 'dayjs'
import { CALENDAR_SHOWED_DAYS_LENGTH } from '@/utils/constants'

export const DATE_FORMAT = {
  PRETIFY: 'HH:mm, D MMM YYYY',
  PRETIFY_SHORTLY: 'MMM DD, YYYY',
  SYSTEM: 'YYYY-MM-DD'
}

export const pretifyDate = (date: string) => dayjs(date).format(DATE_FORMAT.PRETIFY)

export const pretifyDateShortly = (date: string) => dayjs(date).format(DATE_FORMAT.PRETIFY_SHORTLY)

export const isWeekendDayNum = (dayNum: number) => dayNum % 7 === 5 || dayNum % 7 === 6

export const getCalendarMonthInterval = ({ year, month }: { year: number, month: number }) => {
  const monthDate = dayjs().set('year', year).set('month', month)

  let start = monthDate.startOf('month')
  let end = monthDate.endOf('month')

  // For the end of the week adopted Saturday, so we need to correcting the data
  if (end.day() === 6) { // Saturday
    end = end.add(1, 'day')
  }
  if (start.day() === 0) { // Sunday
    start = start.subtract(1, 'day')
  }

  start = start.startOf('week').add(1, 'day')
  end = end.endOf('week').add(1, 'day')

  if (end.diff(start, 'days') + 1 === 28) { // full 4 weeks
    return { start, end: end.add(1, 'week') }
  }

  if (end.diff(start, 'days') + 1 === 35) { // full 5 weeks
    return { start, end: end.add(1, 'week') }
  }

  return { start, end }
}

export const getCalendarMonthDays = (date: string): Array<Object> => {
  const today = dayjs(date)
  const yearNum = today.format('YYYY')
  const monthNum = today.format('MM') - 1
  const { start } = getCalendarMonthInterval({ year: yearNum, month: monthNum })

  return new Array(CALENDAR_SHOWED_DAYS_LENGTH).fill().map((e: any, i: number) => {
    const date = start.add(i, 'days')
    return {
      day: date.format('DD'),
      date: date.format(DATE_FORMAT.SYSTEM),
      selected: false,
      data: null
    }
  })
}

export const isIntervalValid = (_start: ?string, _end: ?string) => {
  const start = dayjs(_start)
  const end = dayjs(_end)
  return start.isSame(end) || start.isBefore(end)
}

export const isDateInTheInterval = (_date: string, { start: _start, end: _end }: { start: ?string, end: ?string }) => {
  const date = dayjs(_date)
  const start = dayjs(_start)
  const end = dayjs(_end)
  return (date.isAfter(start) && date.isBefore(end)) || date.isSame(start) || date.isSame(end)
}

export const isDateInTheIntervalExceptEdges = (_date: string, { start: _start, end: _end }: { start: ?string, end: ?string }) => {
  const date = dayjs(_date)
  const start = dayjs(_start)
  const end = dayjs(_end)
  return date.isAfter(start) && date.isBefore(end)
}
