// @flow
let loadedPolyfills = 0

const loadScript = (src: string, done: Function) => {
  const js = document.createElement('script')
  js.src = src
  js.onload = () => {
    loadedPolyfills--
    if (loadedPolyfills === 0) {
      done()
    }
  }
  js.onerror = () => {
    throw new Error('Failed to load script ' + src)
  }
  // $flow-disable-line
  document.head.appendChild(js)
}

const polyfills = [{
  needed: () => !window.fetch,
  src: 'https://unpkg.com/whatwg-fetch@3.0.0/dist/fetch.umd.js'
}, {
  needed: () => !(window.Promise && Object.values),
  src: 'https://unpkg.com/babel-polyfill@6.26.0/dist/polyfill.min.js'
}]

export function loadPolyfills(done: Function) {
  polyfills.forEach(({ needed, src }: Object) => {
    if (needed()) {
      loadedPolyfills++
      loadScript(src, done)
    }
  })

  if (loadedPolyfills === 0) {
    done()
  }
}
