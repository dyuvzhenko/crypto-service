import expect from 'expect'
import { getCalendarMonthInterval } from '../dates'

describe('utils/dates.js', () => {
  it('should always return interval with 6 valid weeks', () => {
    let year = 2000
    while(year < 2100) {
      year = year + 1

      let month = 0
      while(month < 12) {
        month = month + 1

        const { start, end } = getCalendarMonthInterval({ year, month })
        expect(end.diff(start, 'days') + 1 === 42).toBe(true)
        expect(start.format('dddd')).toBe('Monday')
        expect(end.format('dddd')).toBe('Sunday')
      }
    }
  })
})
