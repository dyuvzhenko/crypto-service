// @flow
import dayjs from 'dayjs'
import { MARKET_DATA_RANGE } from '@/utils/constants'

const SHOWED_POINTS = 6
const mapOffset = {
  [MARKET_DATA_RANGE.ONE_MONTH.name]: 3,
  [MARKET_DATA_RANGE.THREE_MONTHES.name]: 10,
  [MARKET_DATA_RANGE.SIX_MONTHES.name]: 20,
  [MARKET_DATA_RANGE.ONE_YEAR.name]: 40
}

const mapDetailedOffset = {
  [MARKET_DATA_RANGE.ONE_MONTH.name]: 80,
  [MARKET_DATA_RANGE.THREE_MONTHES.name]: 240,
  [MARKET_DATA_RANGE.SIX_MONTHES.name]: 20,
  [MARKET_DATA_RANGE.ONE_YEAR.name]: 40
}

export const getReducedLabels = (points: Array<Object>, rangeName: string, isGraphDetailed?: boolean = false): Array<string> => {
  const rightOffset = (isGraphDetailed ? mapDetailedOffset : mapOffset)[rangeName]
  const divider = Math.ceil((points.length - rightOffset) / SHOWED_POINTS)
  return [
    ...Array(rightOffset).fill().map(() => ''),
    ...points.slice().reverse().slice(rightOffset).map(({ date }: { date: string }, index: number) => {
      if (index % divider === 0) {
        return dayjs(date).format('D MMM')
      }
      return ''
    })
  ].reverse()
}
