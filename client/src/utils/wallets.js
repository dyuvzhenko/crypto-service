// @flow
import React from 'react'
import { CRYPTO_CURRENCY, CRYPTO_CURRENCY_ARRAY, PAPER_CURRENCY_ARRAY } from '@/utils/constants'

import DefaultIcon from '@/assets/icons/wallet-type/_default'
import BitcoinIcon from '@/assets/icons/wallet-type/bitcoin'
import EthereumIcon from '@/assets/icons/wallet-type/ethereum'
import LitecoinIcon from '@/assets/icons/wallet-type/litecoin'
import DashIcon from '@/assets/icons/wallet-type/dash'
import RippleIcon from '@/assets/icons/wallet-type/ripple'
import TetherIcon from '@/assets/icons/wallet-type/tether'

const mapWalletIcon = {
  [CRYPTO_CURRENCY.BTC.id]: <BitcoinIcon />,
  [CRYPTO_CURRENCY.ETH.id]: <EthereumIcon />,
  [CRYPTO_CURRENCY.LTC.id]: <LitecoinIcon />,
  [CRYPTO_CURRENCY.DASH.id]: <DashIcon />,
  [CRYPTO_CURRENCY.XRP.id]: <RippleIcon />,
  [CRYPTO_CURRENCY.TETHER.id]: <TetherIcon />
}

export const getCoinIcon = (coinId: ?string) =>
  coinId ? mapWalletIcon[coinId] || <DefaultIcon /> : <DefaultIcon />

export const getCoinName = (coinId: ?string) => {
  const coin: Object = CRYPTO_CURRENCY_ARRAY.find(({ id }: Object) => id === coinId)
  return coin ? coin.name : 'Unknown'
}

export const getCoinAbbr = (coinId: ?string) => {
  const coin: Object = CRYPTO_CURRENCY_ARRAY.find(({ id }: Object) => id === coinId)
  return coin ? coin.abbr : '??'
}

export const getCoinColor = (coinId: ?string) => {
  const coin: Object = CRYPTO_CURRENCY_ARRAY.find(({ id }: Object) => id === coinId)
  return coin ? coin.color : '#FFF'
}

export const getBalanceAbbr = (paperCoinId: ?string) => {
  const coin: Object = PAPER_CURRENCY_ARRAY.find(({ id }: Object) => id === paperCoinId)
  return coin ? coin.abbr : '??'
}

export const getBalanceName = (paperCoinId: ?string) => {
  const coin: Object = PAPER_CURRENCY_ARRAY.find(({ id }: Object) => id === paperCoinId)
  return coin ? coin.name : '??'
}
