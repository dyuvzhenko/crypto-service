// @flow
import Chartist from 'chartist'

export function TransformPointsIntoLine({ height }: { height: number }) {
  return function transformPointsIntoLine(chart: any) {
    if(chart instanceof Chartist.Line) {
      chart.on('draw', (data: Object) => {
        if (data.type === 'point') {
          const node = data.element.getNode()
          node.setAttribute('y1', 0)
          node.setAttribute('y2', height)
        }
      })
    }
  }
}
