// @flow
import { THEMES } from '@/utils/constants'

export const pretifyNumber = (num: ?number, maximumFractionDigits: number = 0) =>
  Number(num).toLocaleString('en-US', { maximumFractionDigits })

export const isValidNumber = (num: any) => !isNaN(parseFloat(num)) && isFinite(num)

export const appendBodyThemeClass = (isDark: boolean) => {
  const { body } = document
  if (body) {
    body.classList.add(isDark ? THEMES.DARK : THEMES.LIGHT)
    body.classList.remove(!isDark ? THEMES.DARK : THEMES.LIGHT)
  }
}

export const getFileForm = (name: string, file: File) => {
  const form = new FormData()
  form.append(name, file, file.name)
  return form
}

export const getUserPicturePath = (imageName: string) => process.env.NODE_ENV === 'production'
  ? `https://crypto-service-api.herokuapp.com/avatar/${imageName}`
  : `http://localhost:3002/avatar/${imageName}`

export const scrollToTop = () =>
  setTimeout(() => {
    // $flow-disable-line
    document.getElementById('main').scrollTo({
      behavior: 'smooth',
      top: 0
    })
  })
