// @flow
/*eslint no-magic-numbers: ["off"]*/
import dayjs from 'dayjs'
import utcPlugin from 'dayjs/plugin/utc'
dayjs.extend(utcPlugin)

export const API_ENDPOINT = {
  OWN_SERVICE: process.env.NODE_ENV === 'production' ? 'https://crypto-service-api.herokuapp.com/' : '/',
  COIN_GECKO: 'https://api.coingecko.com/api/v3/'
}

export const ROUTES = {
  ENTRY: { default: '/'},
  LOGIN: { default: '/login' },
  SIGNUP: { default: '/signup' },
  DASHBOARD: { default: '/dashboard' },
  WALLETS: { default: '/wallets' },
  MARKETS: { default: '/markets' },
  EVENTS: { default: '/events' },
  TRANSACTIONS: { default: '/transactions', getById: (id: string) => `/transactions/${id}` },
  NEWS: { default: '/news' },
  CALCULATOR: { default: '/calculator' },
  SETTINGS: { default: '/settings' }
}

export const SETTINGS_PAGE = {
  PROFILE: {
    name: 'Profile',
    route: ROUTES.SETTINGS.default + '/profile'
  },
  SECURITY: {
    name: 'Security',
    route: ROUTES.SETTINGS.default + '/security'
  },
  NOTIFICATION: {
    name: 'Notification',
    route: ROUTES.SETTINGS.default + '/notification'
  },
  REFERRALS: {
    name: 'Referrals',
    route: ROUTES.SETTINGS.default + '/referrals'
  },
  EMAIL_SETTINGS: {
    name: 'Email settings',
    route: ROUTES.SETTINGS.default + '/email-settings'
  },
  SESSIONS: {
    name: 'Sessions',
    route: ROUTES.SETTINGS.default + '/sessions'
  }
}

export const SETTINGS_PAGE_ARRAY: Array<Object> = Object.values(SETTINGS_PAGE)

export const THEMES = {
  LIGHT: 'light-theme',
  DARK: 'dark-theme'
}

export const APP_SIZE = {
  MOBILE_WIDTH: 576
}

export const DEFAULT_TRANSACTIONS_REQUESTED_LIST_LENGTH = 20

export const NOTIFICATION_TYPE = {
  SUCCESS: 'success',
  ERROR: 'error',
  INFO: 'info'
}

export const DEFAULT_NOTIFICATION_DURATION = 4

export const LOCAL_STORAGE_ITEM = {
  TOKEN: 'token',
  IS_THEME_DARK: 'is-theme-dark',
  UI_TRANSACTIONS_GRID_LOOK: 'ui-transactions-grid-look',
  UI_MARKETS_GRID_LOOK: 'ui-markets-grid-look',
  UI_MARKETS_GRAPH_GRID_SHOWED: 'ui-markets-graph-grid-showed',
  UI_WALLETS_GRID_LOOK: 'ui-wallets-grid-look',
  UI_WALLETS_GRAPH_GRID_SHOWED: 'ui-wallets-graph-grid-showed',
  UI_DASHBOARD_MARKET_CHARTS_GRID_VS_CURRENCY: 'ui-dashboard-marketChartsGrid-vsCurrency',
  UI_DASHBOARD_MAIN_CHART_RANGE_NAME: 'ui-dashboard-mainChart-rangeName'
}

export const CALENDAR_SHOWED_DAYS_LENGTH = 42

export const COIN_VALUE_MAX_PRECISION = 6

export const PAPER_CURRENCY_MAX_PRECISION = 2

export const SIGNUP_RULES = {
  PASSWORD_LENGTH: 6,
  NAME_LENGTH: 4
}

export const INTERVAL_TO_CHANGE_PUBLIC_SLIDE = 5000

export const CRYPTO_CURRENCY = {
  BTC: { id: 'bitcoin', name: 'Bitcoin', abbr: 'BTC', color: '#F2921B' },
  LTC: { id: 'litecoin', name: 'Litecoin', abbr: 'LTC', color: '#838383' },
  DASH: { id: 'dash', name: 'Dash', abbr: 'DASH', color: '#494AA7' },
  XRP: { id: 'ripple', name: 'Ripple', abbr: 'XRP', color: '#4A90E2' },
  ETH: { id: 'ethereum', name: 'Ethereum', abbr: 'ETH', color: '#979EC1' },

  EOS: { id: 'eos', name: 'EOS', abbr: 'EOS', color: '#555253' },
  TRON: { id: 'tron', name: 'TRON', abbr: 'TRON', color: '#E60815' },
  STELLAR: { id: 'stellar', name: 'Stellar', abbr: 'STELLAR', color: '#555253' },
  TETHER: { id: 'tether', name: 'Tether', abbr: 'TETHER', color: '#1BA27A' },
  CARDANO: { id: 'cardano', name: 'Cardano', abbr: 'CARDANO', color: '#226DD5' },
  MONERO: { id: 'monero', name: 'Monero', abbr: 'MONERO', color: '#B8A58D' },
  CHAINLINK: { id: 'chainlink', name: 'Chainlink', abbr: 'CHAINLINK', color: '#2A5ADA' },
  IOTA: { id: 'iota', name: 'IOTA', abbr: 'IOTA', color: 'gray' },
  NEO: { id: 'neo', name: 'NEO', abbr: 'NEO', color: '#58BF00' },
  COSMOS: { id: 'cosmos', name: 'Cosmos', abbr: 'COSMOS', color: '#2E3148' },
  TEZOS: { id: 'tezos', name: 'Tezos', abbr: 'TEZOS', color: '#2C7DF7' },
  MAKER: { id: 'maker', name: 'Maker', abbr: 'MAKER', color: '#1ABC9C' },
  NEM: { id: 'nem', name: 'NEM', abbr: 'NEM', color: '#67B2E8' },
  DOGECOIN: { id: 'dogecoin', name: 'Dogecoin', abbr: 'DOGECOIN', color: '#C3A634' },
  ZCASH: { id: 'zcash', name: 'Zcash', abbr: 'ZCASH', color: '#ECB244' }
}

export const CRYPTO_CURRENCY_ARRAY: Array<Object> = Object.values(CRYPTO_CURRENCY)

export const WALLET_IDS: Array<string> = Object.values(CRYPTO_CURRENCY).map((obj: Object) => obj.id)

export const PAPER_CURRENCY = {
  USD: { id: 'usd', name: 'US Dollar', abbr: 'USD' },
  EUR: { id: 'eur', name: 'Euro', abbr: 'EUR' }
}

export const PAPER_CURRENCY_ARRAY: Array<Object> = Object.values(PAPER_CURRENCY)

export const MARKET_DATA_RANGE = {
  ONE_MONTH: {
    name: 'ONE_MONTH',
    getInterval: () => ({
      start: dayjs().utc().subtract(30, 'days').startOf('day').unix(),
      end: dayjs().utc().endOf('day').unix()
    })
  },
  THREE_MONTHES: {
    name: 'THREE_MONTHES',
    getInterval: () => ({
      start: dayjs().utc().subtract(90, 'days').startOf('day').unix(),
      end: dayjs().utc().endOf('day').unix()
    })
  },
  SIX_MONTHES: {
    name: 'SIX_MONTHES',
    getInterval: () => ({
      start: dayjs().utc().subtract(180, 'days').startOf('day').unix(),
      end: dayjs().utc().endOf('day').unix()
    })
  },
  ONE_YEAR: {
    name: 'ONE_YEAR',
    getInterval: () => ({
      start: dayjs().utc().subtract(360, 'days').startOf('day').unix(),
      end: dayjs().utc().endOf('day').unix()
    })
  }
}

export const MARKET_DATA_RANGE_ARRAY: Array<Object> = Object.values(MARKET_DATA_RANGE)

export const GRID_LOOK = {
  BLOCKS: 'GRID_LOOK_BLOCKS',
  ROWS: 'GRID_LOOK_ROWS'
}

export const TRANSACTION_STATUS = {
  PENDING: 'PENDING',
  REJECTED: 'REJECTED',
  DONE: 'DONE'
}

export const ARR_DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

export const ARR_MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

export const USER_IMAGE_MAX_SIZE = 1000 * 1000 * 10 // 10Mb
