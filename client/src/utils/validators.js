// @flow
const emailRegexp = /([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/

export const composeValidators = (...validators: Array<Function>) => (value: string) =>
  validators.reduce((error: ?string, validator: Function) => error || validator(value), undefined)

export const required = (value: string) => (value ? undefined : 'Required')

export const mustBeEmail = (value: string) =>
  (emailRegexp.test(value) ? undefined : 'Invalid email')

export const minValueString = (min: number) => (value: string) =>
  value.length >= min ? undefined : `Should be greater than ${min}`
