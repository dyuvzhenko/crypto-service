// @flow
import styles from './notifications.styles.scss'
import React from 'react'
import { connect } from 'react-redux'

import { closeNotification } from './notifications.redux'
import { NOTIFICATION_TYPE } from '@/utils/constants'
import type { NotificationType } from '@/types'
import type { StateType } from '@/types/state'
import CloseIcon from '@/assets/icons/close'

const handleAnimationEnd = (close: Function, id: number) => (elem: ?HTMLElement) => {
  if (elem) {
    elem.addEventListener('animationend', () => close(id), false)
  }
}

const mapStateToProps = (state: StateType) => ({
  list: state.ui.notifications.list
})

const mapDispatchToProps = {
  closeNotification
}

const mapNotificationType = {
  [NOTIFICATION_TYPE.SUCCESS]: styles.success,
  [NOTIFICATION_TYPE.ERROR]: styles.error,
  [NOTIFICATION_TYPE.INFO]: styles.info
}

type PropsType = {|
  list: Array<NotificationType>,
  closeNotification: Function,
  wrapStyles: string
|}

function Notifications({ list, closeNotification, wrapStyles }: PropsType) {
  return (
    <div className={`${styles.wrap} ${wrapStyles}`}>
      {list.map((notification: NotificationType) =>
        <div
          key={notification.id}
          ref={handleAnimationEnd(closeNotification, notification.id)}
          className={`${styles.notification} ${mapNotificationType[notification.type]}`}
          style={{ animationDuration: `${notification.duration}s` }}
        >
          <div className={styles.blockMsg}>
            <span className={styles.text}>{notification.msg}</span>
          </div>
          <div className={styles.blockClose}>
            <div className={styles.icon} onClick={() => closeNotification(notification.id)}>
              <CloseIcon />
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)
