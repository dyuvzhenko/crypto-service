// @flow
import { NOTIFICATIONS as types } from '@/redux/action-types'
import { NOTIFICATION_TYPE, DEFAULT_NOTIFICATION_DURATION } from '@/utils/constants'
import type { NotificationsStateType } from '@/types/state/ui/shared/notifications'
import type { NotificationType } from '@/types'

const regExpCount = /[ ][(](\d+)[)]$/g

const isMsgTheSame = (prevMsg: string, nextMsg: string) =>
  prevMsg === nextMsg || prevMsg.replace(regExpCount, '') === nextMsg

const updateCountInMsg = (msg: string) => {
  const countStr = msg.match(regExpCount)
  if (!countStr) {
    return `${msg} (2)`
  }

  const count = Number(countStr[0].replace(/^[ ][(]/, '').replace(/[)]/, ''))
  const msgWithoutCount = msg.replace(regExpCount, '')
  return `${msgWithoutCount} (${count + 1})`
}

export const closeNotification = (id: number) => ({ type: types.CLOSE, id })
export const addNotification = ({
  msg,
  type = NOTIFICATION_TYPE.SUCCESS,
  duration = DEFAULT_NOTIFICATION_DURATION
}: Object) => ({
  type: types.ADD,
  notification: { type, msg, duration }
})

export const nonWorkingNotification = () => addNotification({
  type: NOTIFICATION_TYPE.INFO,
  msg: 'This functionality is not implemented.'
})

const initialState: NotificationsStateType = {
  id: 0,
  list: []
}

export function reducer(state: NotificationsStateType = initialState, action: Object): NotificationsStateType {
  switch (action.type) {
    case types.ADD:
      if (state.list.length) {
        const lastNofitication = state.list[state.list.length - 1]
        if (isMsgTheSame(lastNofitication.msg, action.notification.msg)) {
          return {
            ...state,
            id: state.id + 1,
            list: [...state.list.filter((n: NotificationType) => n.id !== lastNofitication.id), {
              ...lastNofitication,
              msg: updateCountInMsg(lastNofitication.msg),
              id: state.id
            }]
          }
        }
      }

      return {
        ...state,
        id: state.id + 1,
        list: [...state.list, {
          ...action.notification,
          id: state.id
        }]
      }
    case types.CLOSE:
      return {
        ...state,
        list: state.list.filter((notification: NotificationType) => notification.id !== action.id)
      }
    default:
      return state
  }
}
