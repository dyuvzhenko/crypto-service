// @flow
import styles from './public-preview.styles.scss'
import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import WrapperPublic from '../_wrapper'
import { onMount, slides, setNextSlide, setSlide } from './public-preview.redux'
import { ROUTES, INTERVAL_TO_CHANGE_PUBLIC_SLIDE } from '@/utils/constants'

import OvalImg from '@/assets/images/oval.png'
import type { PublicPreviewSlideType } from '@/types'
import type { StateType } from '@/types/state'

const mapStateToProps = (state: StateType) => ({
  showedSlide: state.ui.publicPreview.slide
})

const mapDispatchToProps = {
  setNextSlide,
  setSlide,
  onMount
}

type PropsType = {|
  showedSlide: PublicPreviewSlideType,
  setNextSlide: Function,
  setSlide: Function,
  onMount: Function
|}

function PublicPreview({ onMount, showedSlide, setSlide, setNextSlide }: PropsType) {
  useEffect(onMount, [])

  const [stopped, stopAnimation] = useState(false)
  const [timerId, setTimerId] = useState(null)

  const resetTimer = () => {
    clearInterval(timerId)
    setTimerId(null)
  }
  if (stopped && timerId) {
    resetTimer()
  } else if (!timerId && !stopped) {
    setTimerId(setInterval(setNextSlide, INTERVAL_TO_CHANGE_PUBLIC_SLIDE))
  }

  useEffect(() => resetTimer, [])

  return (
    <WrapperPublic
      backgroundStyle={`${styles.background} ${showedSlide.bkgStyle}`}
      showAuthButtons={true}
      darkMode={true}
    >
      <div className={styles.page}>
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className={styles.blockSecureSolution}>
              <hr className={styles.leftSeparator} />
              <div className={styles.separatorText}>
                Secure solution for your
                <br />
                digital money
              </div>
            </div>
          </div>
        </div>

        <div className={`row ${styles.blockMainMessage}`}>
          <div className="col-10 col-sm-10 col-md-10 col-lg-1">
            <div className={styles.blockSelectSlide}>
              {slides.map((slide: PublicPreviewSlideType, key: number) =>
                <div
                  key={key}
                  className={`${styles.item} ${showedSlide.text === slide.text ? styles.active : ''}`}
                  onClick={() => {
                    resetTimer()
                    setSlide(slide)
                  }}
                ><img src={OvalImg} />
                </div>
              )}
            </div>
          </div>
          <div className="col-10 col-sm-10 col-md-10 col-xl-7">
            <div className={styles.text}>
              {showedSlide.text}
            </div>
          </div>
        </div>

        <div className="row justify-content-center justify-content-md-start">
          <div className="col-md-2 col-lg-1"></div>
          <div className="col-10 col-sm-10 col-md-8 col-lg-4">
            <Link to={ROUTES.SIGNUP.default}>
              <button
                className={styles.btnGetFreeWallet}
                onMouseOver={() => stopAnimation(true)}
                onMouseLeave={() => stopAnimation(false)}
              >get a free wallet
              </button>
            </Link>
          </div>
        </div>
      </div>
    </WrapperPublic>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicPreview)
