// @flow
import styles from './public-preview.styles.scss'
import { PUBLIC_PREVIEW as types } from '@/redux/action-types'
import type { PublicPreviewStateType } from '@/types/state/ui/pages-public/public-preview'
import type { PublicPreviewSlideType } from '@/types'

const publicImagesUrl = [
  require('@/assets/images/login-background.jpg'),
  require('@/assets/images/preview-slide-1.jpg'),
  require('@/assets/images/preview-slide-2.jpg'),
  require('@/assets/images/preview-slide-3.jpg'),
  require('@/assets/images/preview-slide-4.jpg')
]

export const onMount = () => (dispatch: Function, getState: Function) => {
  if (!getState().ui.publicPreview.imagesPreloaded) {
    dispatch({ type: types.IMAGES_PRELOADED })
    publicImagesUrl.forEach((src: string) => (new Image()).src = src)
  }
}

export const slides: Array<PublicPreviewSlideType> = [
  { num: 0, bkgStyle: styles.slide1, text: 'Exchange between all popular currencies with a couple of clicks' },
  { num: 1, bkgStyle: styles.slide2, text: 'The leading platform for professional cryptocurrency traders' },
  { num: 2, bkgStyle: styles.slide3, text: 'Unlimited free transfers between Cryptonix account' },
  { num: 3, bkgStyle: styles.slide4, text: 'Multi-Currency Wallet That Actually Works' }
]

export const setSlide = (slide: PublicPreviewSlideType) => ({ type: types.SET_SLIDE, slide })
export const setNextSlide = () => ({ type: types.SET_NEXT_SLIDE })

const initialState: PublicPreviewStateType = {
  imagesPreloaded: false,
  slide: slides[0]
}

export function reducer(state: PublicPreviewStateType = initialState, action: Object): PublicPreviewStateType {
  switch (action.type) {
    case types.SET_SLIDE:
      return {
        ...state,
        slide: action.slide
      }
    case types.SET_NEXT_SLIDE:
      const nextNum = state.slide.num + 1
      return {
        ...state,
        slide: nextNum === slides.length ? slides[0] : slides[nextNum]
      }
    case types.IMAGES_PRELOADED:
      return {
        ...state,
        imagesPreloaded: true
      }
    default:
      return state
  }
}
