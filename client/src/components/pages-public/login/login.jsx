// @flow
import styles from '@/styles/shared/login-and-signup.scss'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, Field } from 'react-final-form'

import WrapperPublic from '../_wrapper'
import RequestButton from '@/generic/request-button'
import { ROUTES } from '@/utils/constants'
import { doLogin } from './login.redux'

const mapDispatchToProps = { doLogin }

function Login({ doLogin }: {| doLogin: Function |}) {
  return (
    <WrapperPublic backgroundStyle={styles.background}>
      <Form
        onSubmit={doLogin}
        initialValues={{ email: '', password: '' }}
        render={({ handleSubmit, submitting }: Object) => (
          <form className={styles.page} onSubmit={handleSubmit}>
            <div className="row justify-content-xl-center">
              <div className="col-12 col-sm-12 col-md-6 col-lg-5">
                <div className={styles.blockAuth}>
                  <h2 className={styles.heading}>Log In</h2>
                  <hr className={styles.separatorUnderHeading} />
                  <div className={styles.blockInput}>
                    <Field
                      type="text"
                      name="email"
                      component="input"
                      placeholder="EMAIL"
                      required
                    />
                  </div>

                  <div className={styles.blockInput}>
                    <Field
                      type="password"
                      name="password"
                      component="input"
                      placeholder="PASSWORD"
                      required
                    />
                  </div>

                  <RequestButton
                    pending={submitting}
                    props={{ className: styles.btnSubmit, type: 'submit', disabled: submitting }}
                    text="LOG IN"
                  />
                  <div className={styles.blockForgotPasswordAndSignUp}>
                    <span>Forgot password?</span>
                    <span>Don't have an account?</span>
                    <Link className={styles.signUp} to={ROUTES.SIGNUP.default}>Sign Up</Link>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-lg-5">
                <div className={styles.blockStatistics}>
                  <h2 className={styles.mainHeading}>5K+ WALLETS</h2>
                  <h4 className={styles.secondaryHeading}>300K+ TRANSACTIONS</h4>
                </div>
              </div>
            </div>
          </form>
        )}
      />
    </WrapperPublic>
  )
}

export default connect(null, mapDispatchToProps)(Login)
