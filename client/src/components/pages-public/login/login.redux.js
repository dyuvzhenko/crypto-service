// @flow
import { REQUEST_NAME } from '@/redux/global/requests'
import { actionOnLogin } from '@/redux/scenarios'
import { tryAuthorization } from '@/redux/global/auth'
import { apiOwnService } from '@/api'
import type { GetStateType } from '@/types'

export const doLogin = (values: Object, form: Object, callback: Function) =>
  (dispatch: Function, getState: GetStateType) =>
    dispatch(apiOwnService.doLogin({
      stateName: REQUEST_NAME.GLOBAL.AUTH.DO_LOGIN,
      args: values
    }))
    .then((data: Object) => {
      dispatch(tryAuthorization({ token: data.token, shouldSaveToken: true }))
      if (getState().global.auth.isAuthorized) {
        dispatch(actionOnLogin())
      }
    })
    .finally(callback)
