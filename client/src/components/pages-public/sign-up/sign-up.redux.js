// @flow
/*eslint max-len: ["off"]*/
import { apiOwnService } from '@/api'
import { addNotification } from '@/components/shared/notifications'
import { REQUEST_NAME } from '@/redux/global/requests'

export const doSignUp = (values: Object, form: Object, callback: Function) =>
  (dispatch: Function) =>
    dispatch(apiOwnService.doSignUp({
      stateName: REQUEST_NAME.GLOBAL.AUTH.DO_SIGN_UP,
      args: values
    }))
    .then(({ email }: { email: string }) =>
      dispatch(addNotification({
        msg: `Account with ${email} email has been created. (You don't need to check your email. The email and password you entered are already valid for login.)`
      }))
    )
    .finally(callback)
