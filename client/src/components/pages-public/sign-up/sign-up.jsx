// @flow
import styles from '@/styles/shared/login-and-signup.scss'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, Field } from 'react-final-form'

import WrapperPublic from '../_wrapper'
import RequestButton from '@/generic/request-button'
import { composeValidators, required, mustBeEmail, minValueString } from '@/utils/validators'
import { ROUTES, SIGNUP_RULES } from '@/utils/constants'
import { REQUEST_NAME } from '@/redux/global/requests'
import { doSignUp } from './sign-up.redux'

import type { TryRequestType } from '@/types'
import type { StateType } from '@/types/state'

const mapStateToProps = (state: StateType) => ({
  signupRequest: state.global.requests[REQUEST_NAME.GLOBAL.AUTH.DO_SIGN_UP]
})

const mapDispatchToProps = {
  doSignUp
}

const initialValues = { nickname: '', email: '', password: '' }

const validators = {
  password: composeValidators(required, minValueString(SIGNUP_RULES.PASSWORD_LENGTH)),
  nickname: composeValidators(required, minValueString(SIGNUP_RULES.NAME_LENGTH)),
  email: composeValidators(required, mustBeEmail)
}

const getInputStyles = ({ error, touched }: Object) =>
  `${styles.blockInput} ${(error && touched) ? styles.error : ''}`

type PropsType = {|
  signupRequest: TryRequestType,
  doSignUp: Function
|}

function SignUp({ doSignUp, signupRequest }: PropsType) {
  const { pending, error } = signupRequest
  const isUserCreated = !error && pending === false
  return (
    <WrapperPublic backgroundStyle={styles.background}>
      <Form
        onSubmit={doSignUp}
        initialValues={initialValues}
        render={({ handleSubmit, submitting }: Object) => (
          <form className={styles.page} onSubmit={handleSubmit}>
            <div className="row justify-content-xl-center">
              <div className="col-12 col-sm-12 col-md-6 col-xl-5">
                <div className={styles.blockAuth}>
                  <h2 className={styles.heading}>Sign Up</h2>
                  <hr className={styles.separatorUnderHeading} />
                  {isUserCreated ? (
                    <div>
                      <div className={styles.msgUserCreated}>
                        Your account has been created.
                      </div>
                      <Link className={styles.linkBtnSubmit} to={ROUTES.LOGIN.default}>
                        <button className={styles.btnSubmit}>
                          Return to Log In
                        </button>
                      </Link>
                    </div>
                  ) : (
                    <>
                      <div className="row no-gutters">
                        <div className="col-5 col-sm-5">
                          <Field name="nickname" validate={validators.nickname}>
                            {({ input, meta }: Object) => (
                              <div className={getInputStyles(meta)}>
                                <label className={styles.labelError}>
                                  {meta.error}
                                </label>
                                <input {...input} placeholder="NAME" />
                              </div>
                            )}
                          </Field>
                        </div>

                        <div className="col-7 col-sm-7">
                          <Field name="email" validate={validators.email}>
                            {({ input, meta }: Object) => (
                              <div className={`${styles.paddingLeft} ${getInputStyles(meta)}`}>
                                <label className={styles.labelError}>
                                  {meta.error}
                                </label>
                                <input {...input} placeholder="EMAIL" />
                              </div>
                            )}
                          </Field>
                        </div>
                      </div>

                      <Field name="password" validate={validators.password}>
                        {({ input, meta }: Object) => (
                          <div className={getInputStyles(meta)}>
                            <label className={styles.labelError}>
                              {meta.error}
                            </label>
                            <input
                              {...input}
                              type="password"
                              placeholder="CHOOSE A PASSWORD"
                              autoComplete="new-password"
                            />
                          </div>
                        )}
                      </Field>

                      <RequestButton
                        pending={submitting}
                        props={{ className: styles.btnSubmit, type: 'submit', disabled: submitting }}
                        text={isUserCreated ? 'Return to Log In' : 'CREATE ACCOUNT'}
                      />
                      <div className={styles.blockForgotPasswordAndSignUp}>
                        <span>Already have an account?</span>
                        <Link className={styles.signUp} to={ROUTES.LOGIN.default}>Log in</Link>
                      </div>
                    </>
                  )}
                </div>
              </div>

              <div className="col-md-6 col-xl-5">
                <div className={styles.blockStatistics}>
                  <h2 className={styles.mainHeading}>20K+ CUSTOMERS</h2>
                  <h4 className={styles.secondaryHeading}>60+ EMPLOYEES WORLDWIDE</h4>
                </div>
              </div>
            </div>
          </form>
        )}
      />
    </WrapperPublic>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
