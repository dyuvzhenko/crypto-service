// @flow
import styles from './wrapper.styles.scss'
import * as React from 'react'
import { Link } from 'react-router-dom'
import { ROUTES } from '@/utils/constants'
import Notifications from '@/components/shared/notifications'
import MobileDropdownIcon from '@/assets/icons/mobile-dropdown'

type PropsType = {|
  children: React.Node,
  darkMode?: boolean,
  showAuthButtons?: boolean,
  backgroundStyle: string
|}

export default function WrapperPublic({
  children,
  darkMode = false,
  showAuthButtons = false,
  backgroundStyle
}: PropsType) {
  return (
    <div className={`${backgroundStyle} ${styles.background} ${darkMode ? styles.darkMode : ''}`}>
      <div className={`container ${styles.column}`}>
        <div className={`row ${styles.header}`}>
          <Notifications wrapStyles={styles.wrapNotifications} />
          <div className="col-8 col-sm-7">
            <div className={styles.blockLogo}>
              <a className={styles.logoName} href="/">cryptonix</a>
              <div className={styles.blockLanguages}>
                <span className={`${styles.item} ${styles.active}`}>En</span>
                <span className={styles.item}>中文</span>
              </div>
            </div>
          </div>

          <div className="col-4 col-sm-5">
            {showAuthButtons && (
              <div className={styles.blockHeaderAuth}>
                <Link to={ROUTES.SIGNUP.default}>
                  <button className={styles.btnSignUp}>
                    Sigh up
                  </button>
                </Link>
                <Link to={ROUTES.LOGIN.default}>
                  <button className={styles.btnLogIn}>
                    Log in
                  </button>
                </Link>
                <Link to={ROUTES.LOGIN.default}>
                  <div className={styles.dropdownMenu}>
                    <MobileDropdownIcon />
                  </div>
                </Link>
              </div>
            )}
          </div>
        </div>

        {children}

        <div className={styles.footer}>
          <div className={styles.blockSocialLinks}>
            <a className={styles.item} href="https://www.facebook.com" target="_blank">facebook</a>
            <a className={styles.item} href="https://www.twitter.com" target="_blank">twitter</a>
            <a className={styles.item} href="https://www.google.com" target="_blank">blog</a>
          </div>

          <div className={styles.blockAppService}>
            <a className={styles.item} href="#">Terms of service</a>
            <a className={styles.item} href="#">Contact</a>
            <a className={styles.item} href="#">Support</a>
          </div>
        </div>
      </div>
    </div>
  )
}
