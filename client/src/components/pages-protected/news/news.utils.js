// @flow
/*eslint no-magic-numbers: ["off"]*/
import WideBackgroundDefaultSrc from '@/assets/images/news-wide-background-default.jpg'
import SmallBackgroundDefaultSrc from '@/assets/images/news-small-background-default.jpg'
import { pretifyDateShortly } from '@/utils/dates'
import { MAX_GRID_ARRAY_LENGTH } from './news.redux'
import type { NewsType } from '@/types'

export const getDateInterval = ({ start_date = '', end_date = '' }: NewsType) =>
  pretifyDateShortly(start_date) + (end_date ? ` - ${pretifyDateShortly(end_date)}` : '')

export const setWideBckImageStyle = ({ screenshot = '' }: NewsType) => ({
  backgroundImage: screenshot === 'missing_original.png'
    ? `url(${WideBackgroundDefaultSrc})`
    : `url(${screenshot})`
})

export const setSmallBckImageStyle = ({ screenshot = '' }: NewsType) => ({
  backgroundImage: screenshot === 'missing_original.png'
    ? `url(${SmallBackgroundDefaultSrc})`
    : `url(${screenshot})`
})

export const BREAKPOINTS = {
  MEDIUM: 1250,
  SMALL: 800
}

export const getGridLargeStyle = () => ({ gridTemplateColumns: '25fr 25fr 25fr 23fr' })
export const getGridMediumStyle = () => ({ gridTemplateColumns: '33fr 33fr 33fr' })
export const getGridSmallStyle = () => ({ gridTemplateColumns: '1fr' })

export const getGridBlockLargeStyle = (_num: number) => {
  const num = _num + 1
  const repeatNum = Math.ceil(num / MAX_GRID_ARRAY_LENGTH)
  const multiplier = repeatNum === 1 ? repeatNum : repeatNum * 2 - 1
  if (num % MAX_GRID_ARRAY_LENGTH === 1) {
    return { gridRow: `${multiplier}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 2) {
    return { gridRow: `${multiplier}`, gridColumn: '2 / 4' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 3) {
    return { gridRow: `${multiplier + 1}`, gridColumn: '1 / 3' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 4) {
    return { gridRow: `${multiplier + 1}`, gridColumn: '3' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 0) {
    return { gridRow: `${multiplier} / ${multiplier + 3}`, gridColumn: '4', height: '620px' }
  }
}

export const getGridBlockMediumStyle = (_num: number) => {
  const num = _num + 1
  const repeatNum = Math.ceil(num / MAX_GRID_ARRAY_LENGTH)
  const multiplier = repeatNum === 1 ? repeatNum : repeatNum * 2 - 1
  if (num % MAX_GRID_ARRAY_LENGTH === 1) {
    return { gridRow: `${multiplier}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 2) {
    return { gridRow: `${multiplier}`, gridColumn: '2' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 3) {
    return { gridRow: `${multiplier + 1}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 4) {
    return { gridRow: `${multiplier + 1}`, gridColumn: '2' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 0) {
    return { gridRow: `${multiplier} / ${multiplier + 3}`, gridColumn: '3', height: '620px' }
  }
}

export const getGridBlockSmallStyle = (_num: number) => {
  const num = _num + 1
  const repeatNum = Math.ceil(num / MAX_GRID_ARRAY_LENGTH)
  const multiplier = repeatNum === 1 ? repeatNum : (repeatNum - 1) * MAX_GRID_ARRAY_LENGTH + 1
  if (num % MAX_GRID_ARRAY_LENGTH === 1) {
    return { gridRow: `${multiplier}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 2) {
    return { gridRow: `${multiplier + 1}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 3) {
    return { gridRow: `${multiplier + 2}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 4) {
    return { gridRow: `${multiplier + 3}`, gridColumn: '1' }
  }
  if (num % MAX_GRID_ARRAY_LENGTH === 0) {
    return { gridRow: `${multiplier + 4}`, gridColumn: '1', height: '600px' }
  }
}

export const _setGridStyles = (set: Function, currentGridStyles: Object, width: number) => {
  if (width > BREAKPOINTS.MEDIUM) {
    if (currentGridStyles.getWrapStyle === getGridLargeStyle) {
      return
    }
    set({
      getWrapStyle: getGridLargeStyle,
      getBlockStyle: getGridBlockLargeStyle
    })
  }
  if (width > BREAKPOINTS.SMALL) {
    if (currentGridStyles.getWrapStyle === getGridMediumStyle) {
      return
    }
    set({
      getWrapStyle: getGridMediumStyle,
      getBlockStyle: getGridBlockMediumStyle
    })
  }
  if (width < BREAKPOINTS.SMALL) {
    if (currentGridStyles.getWrapStyle === getGridSmallStyle) {
      return
    }
    set({
      getWrapStyle: getGridSmallStyle,
      getBlockStyle: getGridBlockSmallStyle
    })
  }
}
