// @flow
import dayjs from 'dayjs'
import { apiCoinGecko } from '@/api'
import { DATE_FORMAT } from '@/utils/dates'
import { NOTIFICATION_TYPE } from '@/utils/constants'
import { REQUEST_NAME } from '@/redux/global/requests'
import { NEWS as types } from '@/redux/action-types'
import { addNotification } from '@/components/shared/notifications'
import type { NewsStateType } from '@/types/state/ui/pages-protected/news'
import type { GetStateType, NewsType } from '@/types'

export const MAX_GRID_ARRAY_LENGTH = 5

export const onMount = (expectedCount: number) =>
  (dispatch: Function, getState: GetStateType) => {
    const {
      lastReceivedInterval: { from: currentFrom },
      grid: { length: currentCount }
    } = getState().ui.news

    if (currentCount >= expectedCount) {
      return
    }

    const today = dayjs()
    const interval = currentFrom ? getLastMonth(currentFrom) : {
      from: today.startOf('month').format(DATE_FORMAT.SYSTEM),
      to: today.endOf('month').format(DATE_FORMAT.SYSTEM)
    }
    dispatch(setShowedLength(expectedCount))
    dispatch(getData(expectedCount, interval))
  }

export const setList = (list: Array<NewsType>) => ({
  type: types.SET_LIST,
  list
})

export const setShowedLength = (showedLength: number) => ({
  type: types.SET_SHOWED_LENGTH,
  showedLength
})

export const setGrid = (grid: Array<NewsType>) => ({
  type: types.SET_GRID,
  grid
})

export const setLastReceivedInterval = ({ from, to }: { from: string, to: string }) => ({
  type: types.SET_LAST_RECEIVED_INTERVAL,
  interval: { from, to }
})

const getLastMonth = (from: string) => ({
  from: dayjs(from).subtract(1, 'days').startOf('month').format(DATE_FORMAT.SYSTEM),
  to: dayjs(from).subtract(1, 'days').endOf('month').format(DATE_FORMAT.SYSTEM)
})

export const getDataOnScroll = () => (dispatch: Function, getState: Function) => {
  if (getState().global.requests[REQUEST_NAME.UI.NEWS.GET_LIST].pending) {
    return
  }
  const { list, showedLength, lastReceivedInterval: { from } } = getState().ui.news
  const expectedCount = showedLength + MAX_GRID_ARRAY_LENGTH
  dispatch(setShowedLength(expectedCount))
  if (expectedCount <= list.length) {
    dispatch(calculateGrid())
  } else {
    dispatch(getData(expectedCount, getLastMonth(from)))
  }
}

const getData = (count: number, { from, to }: { from: string, to: string }) =>
  (dispatch: Function, getState: GetStateType) => {
    /* Unfortunately, the api is not able to output data in descending order, so there is a kind of logic here */
    dispatch(requestForData({ from, to })).then(({ data }: { data: Array<NewsType> }) => {
      dispatch(setLastReceivedInterval({ from, to }))

      if (!data.length && dayjs('2014-01-01').isAfter(dayjs().startOf('month').format(DATE_FORMAT.SYSTEM))) {
        dispatch(addNotification({ msg: 'No more news', type: NOTIFICATION_TYPE.ERROR }))
        return
      }

      const filteredData = data.slice(0 - data.length)
      const newList = [...getState().ui.news.list, ...filteredData]
      dispatch(setList(newList))
      dispatch(calculateGrid())

      if (filteredData.length < count) {
        dispatch(getData(count - filteredData.length, getLastMonth(from)))
      }
    })
  }

const calculateGrid = () => (dispatch: Function, getState: Function) => {
  const { list, showedLength } = getState().ui.news
  dispatch(setGrid(list.slice(0, showedLength)))
}

/* Could not find free endpoint for `news`, so here lives `events` */
const requestForData = ({ from, to }: { from: string, to: string }) =>
  apiCoinGecko.getEvents({
    stateName: REQUEST_NAME.UI.NEWS.GET_LIST,
    args: { from, to }
  })

const initialState: NewsStateType = {
  list: [],
  lastReceivedInterval: {
    from: null,
    to: null
  },
  showedLength: 0,
  grid: []
}

export function reducer(state: NewsStateType = initialState, action: Object): NewsStateType {
  switch (action.type) {
    case types.SET_LIST:
      return {
        ...state,
        list: action.list
      }
    case types.SET_GRID:
      return {
        ...state,
        grid: action.grid
      }
    case types.SET_SHOWED_LENGTH:
      return {
        ...state,
        showedLength: action.showedLength
      }
    case types.SET_LAST_RECEIVED_INTERVAL:
      return {
        ...state,
        lastReceivedInterval: action.interval
      }
    default:
      return state
  }
}
