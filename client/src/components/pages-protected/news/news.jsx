// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './news.styles.scss'
import React, { useEffect, useState, useRef } from 'react'
import { connect } from 'react-redux'

import {
  getDateInterval,
  setWideBckImageStyle, setSmallBckImageStyle,
  getGridLargeStyle, getGridBlockLargeStyle, _setGridStyles
} from './news.utils'
import { MAX_GRID_ARRAY_LENGTH, onMount, getDataOnScroll } from './news.redux'
import { REQUEST_NAME } from '@/redux/global/requests'
import { useResizeEffectByRef, useMouseWheelEffect } from '@/utils/react-effects'
import { nonWorkingNotification } from '@/components/shared/notifications'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'
import RequestWrapper from '@/generic/request-wrapper'
import WrapperProtected from '../_wrapper'
import type { NewsStateType } from '@/types/state/ui/pages-protected/news'
import type { StateType, NewsType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  pending: state.global.requests[REQUEST_NAME.UI.NEWS.GET_LIST].pending,
  newsState: state.ui.news
})

const mapDispatchToProps = {
  nonWorkingNotification,
  getDataOnScroll,
  onMount
}

type PropsType = {|
  newsState: NewsStateType,
  nonWorkingNotification: Function,
  getDataOnScroll: Function,
  onMount: Function,
  pending: boolean
|}

function News({ newsState: { grid, showedLength }, onMount, getDataOnScroll, pending, nonWorkingNotification }: PropsType) {
  const ref: any = useRef(null)
  const gridRef: any = useRef(null)
  const [gridStyles, setGridStyles] = useState({
    getWrapStyle: getGridLargeStyle,
    getBlockStyle: getGridBlockLargeStyle
  })

  useEffect(() => {
    _setGridStyles(setGridStyles, gridStyles, ref.current.offsetWidth)
    onMount(Math.floor(ref.current.offsetHeight / gridRef.current.offsetHeight) * MAX_GRID_ARRAY_LENGTH)
  }, [])

  useMouseWheelEffect(() => {
    const mainElem = document.getElementById('main')
    if (mainElem && mainElem.scrollTop + mainElem.clientHeight + 50 >= mainElem.scrollHeight) {
      getDataOnScroll()
    }
  })

  useResizeEffectByRef(ref, ({ width }: { width: number }) => {
    _setGridStyles(setGridStyles, gridStyles, width)
  })

  return (
    <WrapperProtected title="News">
      <RequestWrapper pending={pending} fixed={true}>
        <div className={styles.wrap} ref={ref}>
          <div className={`col-main ${styles.topPanel}`}>
            <div className={styles.rowOverview}>
              <span className={styles.title}>
                Overview
              </span>
              <div className={styles.icon}>
                <DotsVerticalIcon />
              </div>
            </div>
            <div className={styles.addNews} onClick={nonWorkingNotification}>
              Add news
            </div>
          </div>
          <div className={styles.grid} style={gridStyles.getWrapStyle()} ref={gridRef}>
            {pending && !grid.length && Array(showedLength).fill().map((_: any, i: number) =>
              <div className={`${styles.gridBlock} ${styles.skeleton}`} style={gridStyles.getBlockStyle(i)} key={i} />
            )}
            {grid.map((elem: NewsType, gridKey: number) =>
              <div className={styles.gridBlock} style={gridStyles.getBlockStyle(gridKey)} key={gridKey}>
                {(gridKey % MAX_GRID_ARRAY_LENGTH === 1 || gridKey % MAX_GRID_ARRAY_LENGTH === 2) ? (
                  <div className={styles.wideBlock} style={setWideBckImageStyle(elem)}>
                    <div className={styles.wrapContent}>
                      <div className={styles.content}>
                        <a className={styles.desc} href={elem.website} target="_blank">
                          {elem.description}
                        </a>
                        <div className={styles.date}>
                          {getDateInterval(elem)}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : gridKey % MAX_GRID_ARRAY_LENGTH === 4 ? (
                  <div className={styles.tallBlock}>
                    <div className={styles.img} style={setSmallBckImageStyle(elem)} />
                    <div className={styles.wrapContent}>
                      <div className={styles.content}>
                        <a className={styles.title} href={elem.website} target="_blank">
                          {elem.title}
                        </a>
                        <div className={styles.desc}>
                          {elem.description}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className={styles.smallBlock} style={setSmallBckImageStyle(elem)}>
                    <div className={styles.wrapContent}>
                      <div className={styles.content}>
                        <a className={styles.desc} href={elem.website} target="_blank">
                          {elem.title}
                        </a>
                        <div className={styles.date}>
                          {getDateInterval(elem)}
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </RequestWrapper>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(News)
