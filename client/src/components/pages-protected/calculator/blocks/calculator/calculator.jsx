// @flow
import styles from './calculator.styles.scss'
import React from 'react'

import { getCoinName, getCoinAbbr, getBalanceName, getBalanceAbbr } from '@/utils/wallets'
import { CRYPTO_CURRENCY_ARRAY, PAPER_CURRENCY_ARRAY } from '@/utils/constants'
import Select from '@/generic/select'
import RequestWrapper from '@/generic/request-wrapper'
import LongArrowRightIcon from '@/assets/icons/long-arrow-right'

const calculatorButtons = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0', '.', 'C']
const getTextFrom = (coinId: string) => coinId ? `${getCoinName(coinId)} (${getCoinAbbr(coinId)})` : null
const getTextTo = (coinId: string) => coinId ? `${getBalanceName(coinId)} (${getBalanceAbbr(coinId)})` : null

type PropsType = {|
  changeAmount: Function,
  setSelectionStart: Function,
  handleCalcButton: Function,
  handleSetExchangePair: Function,
  calculatorState: Object,
  pending: boolean
|}

function BlockCalculator({
  changeAmount, setSelectionStart, handleCalcButton, handleSetExchangePair,
  pending, calculatorState: { exchangePair, amount, result }
}: PropsType) {
  return (
    <div className={styles.wrap}>
      <RequestWrapper pending={pending}>
        <div className={styles.blockExchange}>
          <div className={styles.from}>
            <div className={styles.label}>
              from
            </div>
            <div className={styles.select}>
              <Select
                selectedItem={getTextFrom(exchangePair.from)}
                options={CRYPTO_CURRENCY_ARRAY.map(({ id }: { id: string }) => ({ text: getTextFrom(id), value: id }))}
                handleClick={(val: string) => handleSetExchangePair({ from: val })}
              />
            </div>
          </div>
          <div className={styles.exchangeIcon}>
            <LongArrowRightIcon />
            <LongArrowRightIcon />
          </div>
          <div className={styles.to}>
            <div className={styles.label}>
              to
            </div>
            <div className={styles.select}>
              <Select
                selectedItem={getTextTo(exchangePair.to)}
                options={PAPER_CURRENCY_ARRAY.map(({ id }: { id: string }) => ({ text: getTextTo(id), value: id }))}
                handleClick={(val: string) => handleSetExchangePair({ to: val })}
              />
            </div>
          </div>
        </div>

        <div className={styles.blockAmount}>
          <label>Enter amount</label>
          <input
            value={amount}
            onFocus={() => setSelectionStart(null)}
            onBlur={(e: Object) => setSelectionStart(e.target.selectionStart)}
            onChange={(e: Object) => changeAmount(e.target.value)}
          />
        </div>

        <div className={styles.blockControls}>
          {calculatorButtons.map((btnName: string) =>
            <div
              onClick={() => handleCalcButton(btnName)}
              className={`${styles.item} ${btnName === 'C' ? styles.resetBtn : ''}`}
              key={`calc-btn-${btnName}`}
            >{btnName}
            </div>
          )}
        </div>

        <div className={styles.blockResult}>
          {result}
        </div>
      </RequestWrapper>
    </div>
  )
}

export default BlockCalculator
