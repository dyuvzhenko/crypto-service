// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './graph.styles.scss'
import React from 'react'
import Chartist from 'chartist'

import { getCoinIcon, getCoinName, getBalanceAbbr } from '@/utils/wallets'
import { getReducedLabels } from '@/utils/charts'
import RequestWrapper from '@/generic/request-wrapper'
import type { PointType } from '@/types'

const mapGraphColor = (graphList: Array<PointType>) => {
  if (!graphList.length) {
    return ''
  }

  const growth = graphList[graphList.length - 1].value - graphList[graphList.length - 2].value
  if (growth < 0) {
    return styles.negative
  }
  if (growth > 0) {
    return styles.positive
  }

  return styles.neutral
}

const options = {
  fullWidth: true,
  showPoint: false,
  lineSmooth: false,
  height: 480,
  axisX: {
    showGrid: false,
    showLabel: true
  },
  axisY: {
    showGrid: true,
    showLabel: true
  }
}

const responsiveOptions = [
  ['(min-width: 900px) and (max-width: 1700px)', { // $calculator-breakpoint-small and $calculator-breakpoint-large
    height: 580
  }]
]

type PropsType = {|
  pending: boolean,
  rangeName: string,
  graphList: Array<PointType>,
  cryptoCoinId: ?string,
  paperCoinId: ?string
|}

function BlockGraph({ pending, rangeName, graphList, cryptoCoinId, paperCoinId }: PropsType) {
  setTimeout(() => {
    new Chartist.Line(
      '#ct-chart-exchange',
      {
        series: [graphList],
        labels: getReducedLabels(graphList, rangeName)
      },
      options,
      responsiveOptions
    )
  })

  return (
    <div className={styles.wrap}>
      <RequestWrapper pending={pending}>
        <div className={styles.blockGraph}>
          <div className={styles.heading}>
            <div className={styles.walletType}>
              <div className={styles.icon}>
                {getCoinIcon(cryptoCoinId)}
              </div>
              <div className={styles.coinName}>
                {getCoinName(cryptoCoinId)}
              </div>
            </div>
            {!!graphList.length && (
              <div className={styles.blockLastValue}>
                <div className={styles.paperCoinId}>
                  {getBalanceAbbr(paperCoinId)}
                </div>
                <div className={styles.amount}>
                  {graphList[graphList.length - 1].value}
                </div>
              </div>
            )}
          </div>


          <div className={`${styles.wrapGraph} ${mapGraphColor(graphList)}`}>
            <div className="ct-chart" id="ct-chart-exchange"></div>
          </div>
        </div>
      </RequestWrapper>
    </div>
  )
}

export default React.memo<PropsType>(BlockGraph)
