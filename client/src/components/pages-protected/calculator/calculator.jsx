// @flow
import styles from './calculator.styles.scss'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import WrapperProtected from '../_wrapper'
import { REQUEST_NAME } from '@/redux/global/requests'
import {
  onMount, resetState,
  setSelectionStart, changeAmount,
  handleCalcButton, handleSetExchangePair
} from './calculator.redux'
import BlockCalculator from './blocks/calculator'
import BlockGraph from './blocks/graph'
import type { CalculatorStateType } from '@/types/state/ui/pages-protected/calculator'
import type { StateType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  calculatorState: state.ui.calculator,
  requestGraphPending: state.global.requests[REQUEST_NAME.UI.CALCULATOR.GET_GRAPH_LIST].pending,
  requestExchangePending: state.global.requests[REQUEST_NAME.UI.CALCULATOR.GET_USD_EXCHANGE].pending
    || state.global.requests[REQUEST_NAME.UI.CALCULATOR.GET_EUR_EXCHANGE].pending
})

const mapDispatchToProps = {
  changeAmount,
  setSelectionStart,
  handleCalcButton,
  handleSetExchangePair,
  resetState,
  onMount
}

type PropsType = {|
  calculatorState: CalculatorStateType,
  requestExchangePending: boolean,
  requestGraphPending: boolean,
  changeAmount: Function,
  setSelectionStart: Function,
  handleCalcButton: Function,
  handleSetExchangePair: Function,
  resetState: Function,
  onMount: Function
|}

function Calculator({
  onMount, resetState, changeAmount, setSelectionStart, handleCalcButton, handleSetExchangePair,
  calculatorState, requestExchangePending, requestGraphPending
}: PropsType) {
  useEffect(() => {
    onMount()
    return resetState
  }, [])

  return (
    <WrapperProtected title="Calculator">
      <div className={styles.wrap}>
        <BlockCalculator
          pending={requestExchangePending}
          changeAmount={changeAmount}
          setSelectionStart={setSelectionStart}
          handleCalcButton={handleCalcButton}
          handleSetExchangePair={handleSetExchangePair}
          calculatorState={calculatorState}
        />

        <BlockGraph
          pending={requestGraphPending}
          rangeName={calculatorState.rangeName}
          graphList={calculatorState.graphList}
          cryptoCoinId={calculatorState.exchangePair.from}
          paperCoinId={calculatorState.exchangePair.to}
        />
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculator)
