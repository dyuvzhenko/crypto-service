// @flow
import { CALCULATOR as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import {
  CRYPTO_CURRENCY_ARRAY, PAPER_CURRENCY_ARRAY,
  PAPER_CURRENCY_MAX_PRECISION, MARKET_DATA_RANGE
} from '@/utils/constants'
import { getCoinsMarketChart } from '@/redux/stable-data/market-charts'
import { getCoinsValueByUSD, getCoinsValueByEUR } from '@/redux/stable-data/exchange'
import { getCoinAbbr, getBalanceAbbr } from '@/utils/wallets'
import { isValidNumber } from '@/utils/common'
import type { CalculatorStateType } from '@/types/state/ui/pages-protected/calculator'
import type { GetStateType } from '@/types'

export const onMount = () => (dispatch: Function, getState: GetStateType) => {
  const { requests } = getState().global
  const vsCurrencyList = CRYPTO_CURRENCY_ARRAY.map(({ id }: { id: string }) => id)
  if (requests[REQUEST_NAME.UI.CALCULATOR.GET_USD_EXCHANGE].pending === null) {
    dispatch(getCoinsValueByUSD(vsCurrencyList, REQUEST_NAME.UI.CALCULATOR.GET_USD_EXCHANGE))
  }
  if (requests[REQUEST_NAME.UI.CALCULATOR.GET_EUR_EXCHANGE].pending === null) {
    dispatch(getCoinsValueByEUR(vsCurrencyList, REQUEST_NAME.UI.CALCULATOR.GET_EUR_EXCHANGE))
  }

  dispatch(setExchangePair({ from: CRYPTO_CURRENCY_ARRAY[0].id, to: PAPER_CURRENCY_ARRAY[0].id }))
  dispatch(getGraph())
}

export const changeAmount = (value: string) => (dispatch: Function, getState: GetStateType) => {
  if (isValidNumber(value) || value === '') {
    const { from, to } = getState().ui.calculator.exchangePair
    if (from && to) {
      const multiplier = getState().stableData.exchange[to][from]
      const resultValue = Number((Number(value) * multiplier).toFixed(PAPER_CURRENCY_MAX_PRECISION))
      const result = `${value || 0} ${getCoinAbbr(from)} = ${resultValue || 0} ${getBalanceAbbr(to)}`
      dispatch(setAmount({ amount: value, result }))
    }
  }
}

export const handleCalcButton = (btnName: string) =>
  (dispatch: Function, getState: GetStateType) => {
    if (btnName === 'C') {
      dispatch(changeAmount(''))
      return
    }
    const { selectionStart, amount } = getState().ui.calculator
    const selection = selectionStart || 0
    dispatch(setSelectionStart(selection + 1))
    dispatch(changeAmount(
      amount.slice(0, selection) +
      btnName +
      amount.slice(selection, amount.length)
    ))
  }

export const handleSetExchangePair = ({ from = null, to = null }: Object) =>
  (dispatch: Function, getState: GetStateType) => {
    const { amount } = getState().ui.calculator
    dispatch(setExchangePair({ from, to }))
    dispatch(changeAmount(amount))
    dispatch(getGraph())
  }

export const getGraph = () => (dispatch: Function, getState: GetStateType) => {
  const { rangeName, exchangePair: { from, to } } = getState().ui.calculator
  if (!from) {
    return
  }

  dispatch(getCoinsMarketChart(REQUEST_NAME.UI.CALCULATOR.GET_GRAPH_LIST, rangeName, [from]))
  .then(() => {
    const { points } = getState().stableData.marketCharts[from][to][rangeName]
    dispatch({ type: types.SET_GRAPH_LIST, graphList: points })
  })
}

export const resetState = () => ({ type: types.RESET_STATE })
export const setSelectionStart = (selectionStart: ?number) => ({ type: types.SET_SELECTION_START, selectionStart })
const setExchangePair = ({ from = null, to = null }: Object) => ({ type: types.SET_EXCHANGE_PAIR, from, to })
const setAmount = ({ amount, result }: Object) => ({ type: types.CHANGE_AMOUNT, amount, result })

const initialState: CalculatorStateType = {
  exchangePair: {
    from: null,
    to: null
  },
  rangeName: MARKET_DATA_RANGE.THREE_MONTHES.name,
  graphList: [],
  selectionStart: null,
  amount: '',
  result: ''
}

export function reducer(state: CalculatorStateType = initialState, action: Object): CalculatorStateType {
  switch (action.type) {
    case types.RESET_STATE:
      return initialState
    case types.SET_EXCHANGE_PAIR:
      return {
        ...state,
        exchangePair: {
          from: action.from || state.exchangePair.from,
          to: action.to || state.exchangePair.to
        }
      }
    case types.CHANGE_AMOUNT:
      return {
        ...state,
        amount: action.amount,
        result: action.result
      }
    case types.SET_SELECTION_START:
      return {
        ...state,
        selectionStart: action.selectionStart
      }
    case types.SET_GRAPH_LIST:
      return {
        ...state,
        graphList: action.graphList
      }
    default:
      return state
  }
}
