// @flow
import dayjs from 'dayjs'

import { apiCoinGecko } from '@/api'
import { EVENTS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { DATE_FORMAT, getCalendarMonthDays, isDateInTheInterval } from '@/utils/dates'
import type { EventsStateType } from '@/types/state/ui/pages-protected/events'
import type { SortedEventType, GetStateType } from '@/types'

export const onMount = () => refreshState(dayjs().format(DATE_FORMAT.SYSTEM), true)

export const selectPrevMonth = () => (dispatch: Function, getState: Function) => {
  const { showedMonth } = getState().ui.events
  dispatch(refreshState(dayjs(showedMonth).subtract(1, 'month').format(DATE_FORMAT.SYSTEM)))
}

export const selectNextMonth = () => (dispatch: Function, getState: Function) => {
  const { showedMonth } = getState().ui.events
  dispatch(refreshState(dayjs(showedMonth).add(1, 'month').format(DATE_FORMAT.SYSTEM)))
}

export const selectMonthManually = ({ year, month }: { year: number, month: number }) =>
  (dispatch: Function) => {
    const showedMonth = `${year}-${month + 1}`
    dispatch(refreshState(dayjs(showedMonth).format(DATE_FORMAT.SYSTEM)))
  }

const refreshState = (_date: string, setListImmediately: boolean = false) => (dispatch: Function) => {
  const date = dayjs(_date)
  const list = getCalendarMonthDays(date.format(DATE_FORMAT.SYSTEM))
  if (setListImmediately) {
    dispatch(setList(list))
  }
  dispatch(apiCoinGecko.getEvents({
    stateName: REQUEST_NAME.UI.EVENTS.GET_LIST,
    args: {
      from: list[0].date,
      to: list[list.length - 1].date
    }
  })).then(({ data }: Object) => {
    if (!setListImmediately) {
      dispatch(setList(list))
    }
    dispatch(setShowedMonth(date.format('YYYY-MM')))
    dispatch(setInterval({
      start: date.startOf('month').format(DATE_FORMAT.SYSTEM),
      end: date.endOf('month').format(DATE_FORMAT.SYSTEM)
    }))
    dispatch(markSelected())
    dispatch(fillCalendarList(data))
  })
}

const fillCalendarList = (dataList: Array<Object>) =>
  (dispatch: Function, getState: GetStateType) => {
    const { list } = getState().ui.events
    const filledList = list.map((item: SortedEventType) => ({
      ...item,
      data: dataList.find((data: Object) => isDateInTheInterval(item.date, {
        start: data.start_date,
        end: data.end_date
      }))
    }))
    dispatch(setList(filledList))
  }

const markSelected = () => (dispatch: Function, getState: GetStateType) => {
  const { list, selectedInterval: { start, end } } = getState().ui.events
  const sortedList: Array<SortedEventType> = list.map((elem: SortedEventType) => {
    const selected = isDateInTheInterval(elem.date, { start, end })
    return { ...elem, selected }
  })
  dispatch(setList(sortedList))
}

const setList = (list: Array<SortedEventType>) => ({ type: types.SET_LIST, list })

const setShowedMonth = (showedMonth: string) => ({ type: types.SET_SHOWED_MONTH, showedMonth })

const setInterval = ({ start, end }: { start: string, end: string }) => ({
  type: types.SET_SELECTED_INTERVAL,
  selectedInterval: { start, end }
})

const initialState: EventsStateType = {
  showedMonth: null,
  selectedInterval: {
    start: null,
    end: null,
  },
  list: [],
}

export function reducer(state: EventsStateType = initialState, action: Object): EventsStateType {
  switch (action.type) {
    case types.SET_LIST:
      return {
        ...state,
        list: action.list
      }
    case types.SET_SHOWED_MONTH:
      return {
        ...state,
        showedMonth: action.showedMonth
      }
    case types.SET_SELECTED_INTERVAL:
      return {
        ...state,
        selectedInterval: action.selectedInterval
      }
    default:
      return state
  }
}
