// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './events.styles.scss'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import dayjs from 'dayjs'

import { onMount, selectPrevMonth, selectNextMonth, selectMonthManually } from './events.redux'
import { nonWorkingNotification } from '@/components/shared/notifications'
import { REQUEST_NAME } from '@/redux/global/requests'
import { ARR_DAYS } from '@/utils/constants'
import { isWeekendDayNum } from '@/utils/dates'
import WrapperProtected from '../_wrapper'
import RequestWrapper from '@/generic/request-wrapper'
import ChevronLeftIcon from '@/assets/icons/chevron-left'
import ChevronRightIcon from '@/assets/icons/chevron-right'
import CalendarMonthPicker from '@/generic/calendar-month-picker'
import type { EventsStateType } from '@/types/state/ui/pages-protected/events'
import type { StateType, SortedEventType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  pending: state.global.requests[REQUEST_NAME.UI.EVENTS.GET_LIST].pending,
  eventsState: state.ui.events
})

const mapDispatchToProps = {
  nonWorkingNotification,
  selectMonthManually,
  selectPrevMonth,
  selectNextMonth,
  onMount
}

type PropsType = {|
  eventsState: EventsStateType,
  nonWorkingNotification: Function,
  selectMonthManually: Function,
  selectPrevMonth: Function,
  selectNextMonth: Function,
  onMount: Function,
  pending: boolean
|}

function Events({
  onMount, nonWorkingNotification,
  selectPrevMonth, selectNextMonth, selectMonthManually,
  eventsState, pending
}: PropsType) {
  useEffect(onMount, [])

  const { list, showedMonth } = eventsState
  const showedDate: any = showedMonth && dayjs(showedMonth)
  return (
    <WrapperProtected title="Events">
      <div className={styles.wrap}>
        <div className={styles.topPanel}>
          <div className={styles.blockFlipWithBtns}>
            {showedDate && showedDate.format('MMMM YYYY')}
            <div className={styles.controls}>
              <div className={`${styles.btn} ${pending ? styles.disabled : ''}`} onClick={selectPrevMonth}>
                <ChevronLeftIcon />
              </div>
              <div className={`${styles.btn} ${pending ? styles.disabled : ''}`} onClick={selectNextMonth}>
                <ChevronRightIcon />
              </div>
            </div>
          </div>
          <div className={styles.btnAddEvent} onClick={nonWorkingNotification}>Add event</div>
          <div className={styles.blockFlipWithCalendar}>
            <div className={styles.blockMonth}>
              <div className={styles.desc}>Select</div>
              <CalendarMonthPicker
                selectedYear={showedDate && showedDate.year()}
                selectedMonth={showedDate && showedDate.month()}
                handleSelect={selectMonthManually}
              />
            </div>
          </div>
        </div>

        <div className={styles.wrapGrid}>
          <RequestWrapper pending={pending} showSpinner={false}>
            <div className={styles.mainGridCalendar}>
              {ARR_DAYS.map((dayName: string, i: number) =>
                <div
                  className={isWeekendDayNum(i) ? styles.blockWeekendName : styles.blockWeekdayName}
                  key={i}
                >{dayName}
                </div>
              )}
              {list.map((item: SortedEventType, i: number) =>
                <div className={`${styles.blockDay} ${item.selected ? '' : styles.nonSelected}`} key={i}>
                  <div className={`${styles.heading} ${isWeekendDayNum(i) ? styles.dayIsWeekend : ''}`}>
                    <div className={styles.dayNum}>
                      {item.day}
                    </div>
                    <div className={styles.locationName}>
                      {item.data && (
                        `${item.data.city ? item.data.city : ''}${item.data.city && item.data.country ? ', ' : ''}${item.data.country ? item.data.country : ''}`
                      )}
                    </div>
                  </div>
                  <a className={styles.content} {...item.data ? { target: '_blank', href: item.data.website } : {}}>
                    {item.data && item.data.title}
                  </a>
                </div>
              )}
            </div>
          </RequestWrapper>
        </div>
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Events)
