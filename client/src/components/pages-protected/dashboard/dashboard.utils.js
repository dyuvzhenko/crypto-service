// @flow
/*eslint no-magic-numbers: ["off"]*/
import { createSelector } from 'reselect'

import { PAPER_CURRENCY } from '@/utils/constants'
import { isValidNumber } from '@/utils/common'
import type { StateType, WalletType } from '@/types'

const getSortedWalletsList = (list: Array<WalletType>, exchange: Object) => {
  let sum = 0
  const sortedList: Array<Object> = list
    .map((wallet: WalletType) => {
      const rate = exchange[wallet.coin_id]
      if (isValidNumber(rate)) {
        const usdValue = wallet.value * rate
        sum = sum + usdValue
        return { ...wallet, usdValue, percentage: 0 }
      }
      return { ...wallet, usdValue: 0, percentage: 0 }
    })
    .map((wallet: Object) => ({
      ...wallet,
      usdValue: Number(wallet.usdValue.toFixed(2)) || 0,
      percentage: Number(((wallet.usdValue * 100) / sum).toFixed(1)) || (wallet.usdValue === 0 ? 0 : 0.1)
    }))
    .sort((a: Object, b: Object) => b.percentage - a.percentage)

  return sortedList
}

const walletsListSelector = (state: StateType) => state.stableData.wallets.list
const exchangeUSDSelector = (state: StateType) => state.stableData.exchange[PAPER_CURRENCY.USD.id]

export const walletsProportionsSelector = createSelector(
  walletsListSelector,
  exchangeUSDSelector,
  getSortedWalletsList
)
