// @flow
/*eslint no-magic-numbers: ["off"]*/
import commonStyles from '@/styles/common.scss'
import styles from './block-main-chart.styles.scss'
import React from 'react'
import Chartist from 'chartist'
import ChartistTooltip from 'chartist-plugin-tooltips-updated'
import { TransformPointsIntoLine } from '@/utils/chartist-plugins'

import RequestWrapper from '@/generic/request-wrapper'
import { MARKET_DATA_RANGE } from '@/utils/constants'
import { getReducedLabels } from '@/utils/charts'
import { pretifyDateShortly } from '@/utils/dates'
import { getBalanceAbbr } from '@/utils/wallets'
import type { MarketChartType, PointType } from '@/types'

const getMeta = (point: PointType, paperCoinId: string) =>
  pretifyDateShortly(point.date) + '\n' +
  point.value + ' ' + getBalanceAbbr(paperCoinId)

const controls = [
  { text: '1m', value: MARKET_DATA_RANGE.ONE_MONTH.name },
  { text: '3m', value: MARKET_DATA_RANGE.THREE_MONTHES.name },
  { text: '6m', value: MARKET_DATA_RANGE.SIX_MONTHES.name },
  { text: '1y', value: MARKET_DATA_RANGE.ONE_YEAR.name }
]

const mapGraphColor = (growth: number) => {
  if (growth < 0) {
    return styles.negative
  }
  if (growth > 0) {
    return styles.positive
  }

  return styles.neutral
}

const graphOptions = {
  plugins: [
    TransformPointsIntoLine({ height: 320 }),
    ChartistTooltip({
      class: commonStyles.tooltipOverwrite,
      transformTooltipTextFnc: () => null
    })
  ],
  fullWidth: true,
  showPoint: true,
  lineSmooth: false,
  axisX: {
    showGrid: false,
    showLabel: true,
    offset: 20
  },
  axisY: {
    labelInterpolationFnc: (value: number) => (value / 1000) + 'k',
    showGrid: true,
    showLabel: true,
    offset: 30
  }
}

type PropsType = {|
  pending: boolean,
  getMainChart: Function,
  mainChart: {
    paperCoinId: string,
    cryptoCoinId: string,
    rangeName: string,
    chartData: ?MarketChartType
  }
|}

function BlockMainChart({ pending, getMainChart, mainChart }: PropsType) {
  if (mainChart.chartData && !pending) {
    setTimeout(() => {
      new Chartist.Line(
        `.${styles.graph}`,
        {
          // $flow-disable-line
          labels: getReducedLabels(mainChart.chartData.points, mainChart.chartData.rangeName),
          series: [
            // $flow-disable-line
            mainChart.chartData.points.map((point: PointType) => ({
              meta: getMeta(point, mainChart.paperCoinId),
              value: point.value
            }))
          ]
        },
        graphOptions
      )
    })
  }

  return (
    <RequestWrapper pending={pending}>
      <div className={styles.blockChart}>
        <div className={styles.topPanel}>
          <div className={styles.heading}>
            Bitcoin Chart
          </div>
          <div className={styles.controls}>
            {controls.map(({ text, value }: Object, i: number) => (
              <div
                key={i}
                onClick={() => getMainChart(value)}
                className={`${styles.item} ${value === mainChart.rangeName ? styles.active : ''}`}
              >{text}
              </div>
            ))}
          </div>
        </div>

        {mainChart.chartData && (
          <div className={`${styles.graph} ${mapGraphColor(mainChart.chartData.lastGrowth)}`} />
        )}
      </div>
    </RequestWrapper>
  )
}

export default React.memo<PropsType>(BlockMainChart)
