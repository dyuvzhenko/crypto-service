// @flow
import styles from './block-make-transaction.styles.scss'
import React from 'react'
import { Form, Field } from 'react-final-form'
import { getCoinAbbr } from '@/utils/wallets'
import { isValidNumber } from '@/utils/common'
import RequestWrapper from '@/generic/request-wrapper'
import Select from '@/generic/select'
import type { WalletType } from '@/types'

type PropsType = {|
  trySendCoins: Function,
  tryReceiveCoins: Function,
  setWalletForMakeTransaction: Function,
  walletForMakeTransaction: ?WalletType,
  list: Array<WalletType>,
  pending: boolean
|}

function BlockMakeTransaction({
  pending,
  list,
  trySendCoins,
  tryReceiveCoins,
  setWalletForMakeTransaction,
  walletForMakeTransaction,
}: PropsType) {
  return (
    <RequestWrapper pending={pending}>
      <Form
        onSubmit={(values: Object) => trySendCoins({ ...values, wallet: walletForMakeTransaction })}
        initialValues={{ amount: '', sendTo: '' }}
        render={({ handleSubmit, submitting, values }: Object) => {
          const disableSendCoins = submitting || !values.amount || !values.sendTo || !walletForMakeTransaction
          const disableReceiveCoins = submitting || !values.amount
          return (
            <div className={styles.blockMakeTransaction}>
              <div className={styles.rowSelectAmount}>
                <div className={styles.blockWalletSelect}>
                  <span className={styles.fieldName}>Wallet</span>
                  <div className={styles.wrapSelect}>
                    <Select
                      handleClick={setWalletForMakeTransaction}
                      setSelectedItem={setWalletForMakeTransaction}
                      selectedItem={walletForMakeTransaction ? getCoinAbbr(walletForMakeTransaction.coin_id) : null}
                      options={list.map((wallet: WalletType) => ({
                        text: getCoinAbbr(wallet.coin_id),
                        value: wallet
                      }))}
                    />
                  </div>
                </div>

                <div className={styles.blockAmount}>
                  <span className={styles.fieldName}>Amount</span>
                  <Field name="amount">
                    {({ input }: Object) => (
                      <input
                        {...input}
                        className={styles.inputRow}
                        onChange={(e: Object) => {
                          e.preventDefault()
                          const { value } = e.target
                          if (isValidNumber(value) || value === '') {
                            input.onChange(value)
                          }
                        }}
                      />
                    )}
                  </Field>
                </div>
              </div>
              <div className={styles.blockSendTo}>
                <span className={styles.fieldName}>Send to</span>
                <Field name="sendTo">
                  {({ input }: Object) => <input className={styles.inputRow} {...input} />}
                </Field>
              </div>

              <div className={styles.controls}>
                <div
                  className={styles.btnReceive}
                  onClick={disableReceiveCoins
                    ? null
                    : () => tryReceiveCoins({
                      ...values,
                      wallet: walletForMakeTransaction
                    })
                  }
                  disabled={disableReceiveCoins}
                >Receive coin
                </div>
                <button
                  className={styles.btnSend}
                  onClick={handleSubmit}
                  disabled={disableSendCoins}
                >Send
                </button>
              </div>
            </div>
          )
        }}
      />
    </RequestWrapper>
  )
}

export default BlockMakeTransaction
