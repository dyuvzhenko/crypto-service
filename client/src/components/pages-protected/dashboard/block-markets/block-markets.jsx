// @flow
import styles from './block-markets.styles.scss'
import React from 'react'
import Chartist from 'chartist'

import RequestWrapper from '@/generic/request-wrapper'
import { getBalanceAbbr, getCoinAbbr } from '@/utils/wallets'
import { PAPER_CURRENCY } from '@/utils/constants'
import type { MarketChartType } from '@/types'

const getControlsStyle = (vsCurrency: string, selectedVsCurrency: string) =>
  `${styles.item} ${vsCurrency === selectedVsCurrency ? styles.active : ''}`

const mapGraphColor = (growth: number) => {
  if (growth < 0) {
    return styles.negative
  }
  if (growth > 0) {
    return styles.positive
  }

  return styles.neutral
}

const graphOptions = {
  fullWidth: true,
  showPoint: false,
  lineSmooth: false,
  height: '40px',
  chartPadding: {
    top: 5,
    right: 0,
    bottom: 5,
    left: 15
  },
  axisX: {
    showGrid: false,
    showLabel: false,
    offset: 0
  },
  axisY: {
    showGrid: false,
    showLabel: false,
    offset: 0
  }
}

type PropsType = {|
  pending: boolean,
  setVsCurrency: Function,
  grid: {
    vsCurrency: string,
    list: Array<MarketChartType>
  }
|}

function BlockMarkets({ pending, setVsCurrency, grid: { vsCurrency, list } }: PropsType) {
  const showedList = list.filter((item: MarketChartType) => item.paperCoinId === vsCurrency)
  return (
    <RequestWrapper pending={pending}>
      <div className={styles.blockMarkets}>
        <div className={styles.topPanel}>
          <div className={styles.heading}>
            Markets
          </div>
          <div className={styles.controls}>
            <div
              className={getControlsStyle(PAPER_CURRENCY.USD.id, vsCurrency)}
              onClick={() => setVsCurrency(PAPER_CURRENCY.USD.id)}
            >{PAPER_CURRENCY.USD.abbr}
            </div>
            <div
              className={getControlsStyle(PAPER_CURRENCY.EUR.id, vsCurrency)}
              onClick={() => setVsCurrency(PAPER_CURRENCY.EUR.id)}
            >{PAPER_CURRENCY.EUR.abbr}
            </div>
          </div>
        </div>

        <div className={styles.list}>
          {showedList.map((chart: MarketChartType) => {
            const exchangePair = `${getCoinAbbr(chart.cryptoCoinId)}/${getBalanceAbbr(chart.paperCoinId)}`
            setTimeout(() => {
              new Chartist.Line(
                `#${chart.cryptoCoinId}`,
                { series: [chart.points] },
                graphOptions
              )
            })

            return (
              <div className={styles.item} key={chart.cryptoCoinId}>
                <div className={styles.blockInfo}>
                  <div className={styles.firstLine}>
                    {exchangePair}
                    <span className={styles.percentage}>
                      {chart.lastGrowth}%
                    </span>
                  </div>
                  <div className={styles.secondLine}>
                    {chart.lastValue}
                    <span className={styles.paperCoin}>
                      {getBalanceAbbr(chart.paperCoinId)}
                    </span>
                  </div>
                </div>
                <div
                  className={`${styles.blockGraph} ${mapGraphColor(chart.lastGrowth)}`}
                  id={chart.cryptoCoinId}
                />
              </div>
            )
          })}
        </div>
      </div>
    </RequestWrapper>
  )
}

export default React.memo<PropsType>(BlockMarkets)
