// @flow
import styles from './block-upcoming-events.styles.scss'
import React from 'react'

import RequestWrapper from '@/generic/request-wrapper'
import { pretifyDateShortly } from '@/utils/dates'
import type { GridType, EventType } from '@/types'

const MAX_SHOWED_EVENTS = 3

type PropsType = {|
  pending: boolean,
  grid: GridType
|}

function BlockUpcomingEvents({ grid, pending }: PropsType) {
  return (
    <div className={styles.blockUpcomingEvents}>
      <RequestWrapper pending={pending}>
        <div className={styles.wrap}>
          <div className={styles.header}>Upcoming Events</div>
          <div className={styles.list}>
            {grid.list.slice(0, MAX_SHOWED_EVENTS).map((upcomingEvent: EventType, key: number) =>
              <div className={styles.item} key={key}>
                <a href={upcomingEvent.website} target="_blank" className={styles.text}>
                  {upcomingEvent.description}
                </a>
                <div className={styles.blockDateAndPlace}>
                  <div className={styles.date}>
                    {pretifyDateShortly(upcomingEvent.start_date)}
                  </div>
                  <div className={styles.place}>
                    {(upcomingEvent.city || '') + (upcomingEvent.city ? ', ' : '') + (upcomingEvent.country || '')}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </RequestWrapper>
    </div>
  )
}

export default BlockUpcomingEvents
