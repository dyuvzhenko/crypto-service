// @flow
import styles from './block-balance.styles.scss'
import React from 'react'
import { Link } from 'react-router-dom'
import { pretifyNumber, isValidNumber } from '@/utils/common'
import { ROUTES, PAPER_CURRENCY } from '@/utils/constants'
import RequestWrapper from '@/generic/request-wrapper'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'

type PropsType = {|
  transactionsTotal: ?number,
  walletsCount: number,
  balance: Object,
  pending: boolean,
  getMoney: Function
|}

function BlockBalance({
  transactionsTotal, walletsCount, balance,
  pending, getMoney
}: PropsType) {
  const usdWalletCount = balance[PAPER_CURRENCY.USD.id]
  const eurWalletCount = balance[PAPER_CURRENCY.EUR.id]
  return (
    <div className={styles.blockBalance}>
      <div className={styles.topPanel}>
        <div className={styles.row}>
          <span className={styles.title}>
            Overview
          </span>
          <div className={styles.icon}>
            <DotsVerticalIcon />
          </div>
        </div>
      </div>

      <RequestWrapper pending={pending}>
        <div className={styles.wrapContent}>
          <div className={styles.transactionsAndWalletsCountRow}>
            <div className={styles.transactionsTotal}>
              <Link to={ROUTES.TRANSACTIONS.default} className={styles.count}>
                {pretifyNumber(transactionsTotal)}
              </Link>
              <div className={styles.desc}>Transactions</div>
            </div>
            <Link to={ROUTES.WALLETS.default} className={styles.walletsCount}>
              <span className={styles.count}>{walletsCount}</span>
              <span className={styles.desc}>Wallets</span>
            </Link>
          </div>

          <div className={styles.splitColumns}>
            <div className={styles.columnBalances}>
              <div className={styles.heading}>Current Balance</div>
              {isValidNumber(usdWalletCount) && (
                <div className={styles.mainItem}>
                  <div className={styles.count}>{pretifyNumber(usdWalletCount, 0)}</div>
                  <div className={styles.desc}>{PAPER_CURRENCY.USD.abbr}</div>
                </div>
              )}
              {isValidNumber(eurWalletCount) && (
                <div className={usdWalletCount ? styles.secondItem : styles.mainItem}>
                  <div className={styles.count}>{pretifyNumber(eurWalletCount, 0)}</div>
                  <div className={styles.desc}>{PAPER_CURRENCY.EUR.abbr}</div>
                </div>
              )}
            </div>
            <div className={styles.columnBtnToBuy}>
              <button className={styles.btnToBuy} onClick={getMoney}>
                buy
              </button>
            </div>
          </div>
        </div>
      </RequestWrapper>
    </div>
  )
}

export default BlockBalance
