// @flow
import styles from './block-transactions.styles.scss'
import React from 'react'
import { Link } from 'react-router-dom'

import RequestWrapper from '@/generic/request-wrapper'
import { getCoinAbbr } from '@/utils/wallets'
import { pretifyNumber } from '@/utils/common'
import { pretifyDate } from '@/utils/dates'
import { ROUTES, TRANSACTION_STATUS, COIN_VALUE_MAX_PRECISION } from '@/utils/constants'
import { GRID_COUNT } from '../dashboard.redux'
import type { GridType, TransactionType } from '@/types'

import ArrowLeftIcon from '@/assets/icons/arrow-left'
import ArrowRightIcon from '@/assets/icons/arrow-right'

const mapIconTransactionStyle = (transaction: TransactionType) => {
  const colorStyle = transaction.isIncome
    ? styles.red
    : transaction.status === TRANSACTION_STATUS.PENDING
      ? styles.blue
      : styles.green

  return `${styles.icon} ${colorStyle}`
}

const mapItemStyle = (isIncome: ?boolean, selected: ?boolean) =>
  `${styles.item} ${isIncome === selected ? styles.active : ''}`

type PropsType = {|
  pending: boolean,
  grid: GridType,
  getTransactionsByIncome: Function
|}

function BlockTransactions({ pending, grid, getTransactionsByIncome }: PropsType) {
  return (
    <div className={styles.blockTransactions}>
      <div className={styles.header}>
        <div className={styles.title}>Transactions</div>
        <div className={`${styles.controls} ${pending ? styles.pending : ''}`}>
          <span
            className={mapItemStyle(null, grid.isIncome)}
            onClick={() => getTransactionsByIncome({ isIncome: null })}
          >All
          </span>
          <span
            className={mapItemStyle(false, grid.isIncome)}
            onClick={() => getTransactionsByIncome({ isIncome: false })}
          >SENT
          </span>
          <span
            className={mapItemStyle(true, grid.isIncome)}
            onClick={() => getTransactionsByIncome({ isIncome: true })}
          >RECEIVED
          </span>
        </div>
      </div>
      <div className={styles.content}>
        <RequestWrapper pending={pending}>
          {pending && !grid.list.length && Array(GRID_COUNT.TRANSACTIONS).fill().map((_: any, i: number) =>
            <div className={`${styles.item} ${styles.skeleton}`} key={i} />
          )}
          {grid.list.map((transaction: TransactionType) =>
            <div className={styles.item} key={transaction.id}>
              <div className={styles.topPanel}>
                <Link
                  className={styles.date}
                  to={ROUTES.TRANSACTIONS.getById(transaction.id)}
                >{pretifyDate(transaction.date)}
                </Link>
                <div className={styles.info}>
                  <div className={mapIconTransactionStyle(transaction)}>
                    {transaction.isIncome ? <ArrowLeftIcon /> : <ArrowRightIcon />}
                  </div>
                  {pretifyNumber(transaction.amount, COIN_VALUE_MAX_PRECISION)}
                  {' '}
                  {getCoinAbbr(transaction.coin_id)}
                </div>
              </div>
              <div className={styles.bottomPanel}>
                {transaction.to_token}
              </div>
            </div>
          )}
        </RequestWrapper>
      </div>
    </div>
  )
}

export default BlockTransactions
