// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './block-wallets-proportion.styles.scss'
import React from 'react'
import Chartist from 'chartist'

import { getCoinName } from '@/utils/wallets'
import RequestWrapper from '@/generic/request-wrapper'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'
import type { SortedWalletType } from '@/types'

type PropsType = {|
  pending: boolean,
  nonWorkingNotification: Function,
  list: Array<SortedWalletType>
|}

const getSeries = (percentage: number) => ({
  series: [percentage, 100 - percentage]
})

const options = {
  donut: true,
  donutWidth: 3,
  donutSolid: true,
  startAngle: 0,
  showLabel: false,
  width: 42,
  height: 42
}

function BlockWalletsProportion({ pending, list, nonWorkingNotification }: PropsType) {
  return (
    <div className={styles.wrap}>
      <RequestWrapper pending={pending}>
        <div className={styles.blockWalletsProportion}>
          {list.map((wallet: SortedWalletType) => {
            const graphId = `ct-chart-${wallet.id}`
            setTimeout(() => {
              new Chartist.Pie(`#${graphId}`, getSeries(wallet.percentage), options)
            })

            return (
              <div className={styles.item} title={`${wallet.usdValue} USD`} key={wallet.id}>
                <div className={styles.pie}>
                  <div className={styles.graph} id={graphId}></div>
                </div>
                <div className={styles.percentage}>{`${wallet.percentage} %`}</div>
                <div className={styles.walletName}>{getCoinName(wallet.coin_id)}</div>
                <div className={styles.options} onClick={nonWorkingNotification}>
                  <DotsVerticalIcon />
                </div>
              </div>
            )
          })}
        </div>
      </RequestWrapper>
    </div>
  )
}

export default React.memo<PropsType>(BlockWalletsProportion)
