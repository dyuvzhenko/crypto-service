// @flow
import styles from './dashboard.styles.scss'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import WrapperProtected from '../_wrapper'
import BlockBalance from './block-balance'
import BlockWallets from './block-wallets'
import BlockWalletsProportion from './block-wallets-proportion'
import BlockMakeTransaction from './block-make-transaction'
import BlockMainChart from './block-main-chart'
import BlockMarkets from './block-markets'
import BlockTransactions from './block-transactions'
import BlockUpcomingEvents from './block-upcoming-events'
import { REQUEST_NAME } from '@/redux/global/requests'
import { walletsProportionsSelector } from './dashboard.utils'
import {
  onMount, onUnmount,
  getTransactionsByIncome, getMoney, getMainChart,
  setWalletForMakeTransaction,
  trySendCoins, tryReceiveCoins,
  setVsCurrencyForMarketCharts,
  selectWalletCoinId,
  redirectToCreateWallet
} from './dashboard.redux'
import { nonWorkingNotification } from '@/components/shared/notifications'
import type { StateType, WalletType, SortedWalletType } from '@/types'
import type { DashboardStateType } from '@/types/state/ui/pages-protected/dashboard'

const mapStateToProps = (state: StateType) => ({
  walletsProportionsList: walletsProportionsSelector(state),
  dashboardState: state.ui.dashboard,
  walletsList: state.stableData.wallets.list,
  balance: state.stableData.balance.data,
  requests: state.global.requests
})

const mapDispatchToProps = {
  nonWorkingNotification,
  getTransactionsByIncome,
  setWalletForMakeTransaction,
  setVsCurrencyForMarketCharts,
  redirectToCreateWallet,
  trySendCoins,
  tryReceiveCoins,
  selectWalletCoinId,
  getMainChart,
  getMoney,
  onUnmount,
  onMount
}

type PropsType = {|
  walletsProportionsList: Array<SortedWalletType>,
  dashboardState: DashboardStateType,
  onMount: Function,
  onUnmount: Function,
  walletsList: Array<WalletType>,
  nonWorkingNotification: Function,
  getTransactionsByIncome: Function,
  setWalletForMakeTransaction: Function,
  setVsCurrencyForMarketCharts: Function,
  redirectToCreateWallet: Function,
  trySendCoins: Function,
  tryReceiveCoins: Function,
  selectWalletCoinId: Function,
  getMainChart: Function,
  getMoney: Function,
  balance: Object,
  requests: Object
|}

function Dashboard({
  walletsProportionsList,
  dashboardState: {
    selectedWalletCoinId,
    walletForMakeTransaction,
    allTransactionsTotal,
    transactionsGrid,
    upcomingEventsGrid,
    marketChartsGrid,
    mainChart
  },
  onMount,
  onUnmount,
  requests,
  getMoney,
  getMainChart,
  trySendCoins,
  tryReceiveCoins,
  selectWalletCoinId,
  redirectToCreateWallet,
  setWalletForMakeTransaction,
  setVsCurrencyForMarketCharts,
  getTransactionsByIncome,
  walletsList, balance,
  nonWorkingNotification
}: PropsType) {
  useEffect(() => {
    onMount()
    return onUnmount
  }, [])

  return (
    <WrapperProtected title="Dashboard">
      <div className={styles.wrapGrid}>
        <div className={styles.gridPartBlockBalance}>
          <BlockBalance
            pending={
              requests[REQUEST_NAME.UI.DASHBOARD.GET_MONEY].pending ||
              requests[REQUEST_NAME.UI.DASHBOARD.GET_TRANSACTIONS].pending ||
              requests[REQUEST_NAME.STABLE_DATA.WALLETS.GET_ALL].pending ||
              requests[REQUEST_NAME.STABLE_DATA.BALANCE.GET_ALL].pending
            }
            transactionsTotal={allTransactionsTotal}
            walletsCount={walletsList.length}
            getMoney={getMoney}
            balance={balance}
          />
        </div>

        <div className={styles.gridPartBlockWallets}>
          <BlockWallets
            pending={requests[REQUEST_NAME.STABLE_DATA.WALLETS.GET_ALL].pending}
            redirectToCreateWallet={redirectToCreateWallet}
            selectedWalletCoinId={selectedWalletCoinId}
            selectWalletCoinId={selectWalletCoinId}
            list={walletsList}
          />
        </div>

        <div className={styles.gridPartBlockWalletsProportion}>
          <BlockWalletsProportion
            pending={
              requests[REQUEST_NAME.UI.DASHBOARD.GET_COINS_VALUE].pending ||
              requests[REQUEST_NAME.STABLE_DATA.WALLETS.GET_ALL].pending
            }
            list={walletsProportionsList}
            nonWorkingNotification={nonWorkingNotification}
          />
        </div>

        <div className={styles.gridPartBlockMakeTransaction}>
          <BlockMakeTransaction
            pending={
              requests[REQUEST_NAME.UI.DASHBOARD.SEND_COINS].pending ||
              requests[REQUEST_NAME.UI.DASHBOARD.RECEIVE_COINS].pending
            }
            list={walletsList}
            trySendCoins={trySendCoins}
            tryReceiveCoins={tryReceiveCoins}
            setWalletForMakeTransaction={setWalletForMakeTransaction}
            walletForMakeTransaction={walletForMakeTransaction}
          />
        </div>

        <div className={styles.gridPartBlockChart}>
          <BlockMainChart
            pending={requests[REQUEST_NAME.UI.DASHBOARD.GET_MAIN_CHART].pending}
            getMainChart={getMainChart}
            mainChart={mainChart}
          />
        </div>

        <div className={styles.gridPartBlockMarkets}>
          <BlockMarkets
            pending={requests[REQUEST_NAME.UI.DASHBOARD.GET_MARKET_CHARTS].pending}
            setVsCurrency={setVsCurrencyForMarketCharts}
            grid={marketChartsGrid}
          />
        </div>

        <div className={styles.gridPartBlockTransactions}>
          <BlockTransactions
            pending={
              requests[REQUEST_NAME.UI.DASHBOARD.FILTER_TRANSACTIONS].pending ||
              requests[REQUEST_NAME.UI.DASHBOARD.GET_TRANSACTIONS].pending
            }
            getTransactionsByIncome={getTransactionsByIncome}
            grid={transactionsGrid}
          />
        </div>

        <div className={styles.gridPartBlockUpcomingEvents}>
          <BlockUpcomingEvents
            pending={requests[REQUEST_NAME.UI.DASHBOARD.GET_UPCOMING_EVENTS].pending}
            grid={upcomingEventsGrid}
          />
        </div>
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
