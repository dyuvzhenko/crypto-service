// @flow
import { AUTH, DASHBOARD as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import {
  ROUTES, NOTIFICATION_TYPE, LOCAL_STORAGE_ITEM,
  WALLET_IDS, PAPER_CURRENCY, CRYPTO_CURRENCY, CRYPTO_CURRENCY_ARRAY, MARKET_DATA_RANGE
} from '@/utils/constants'
import { history } from '@/utils/history'
import { apiOwnService, apiCoinGecko } from '@/api'
import { getVsCurrency, getRangeName } from '@/utils/local-storage'
import { getCoinsValueByUSD } from '@/redux/stable-data/exchange'
import { getCoinsMarketChart } from '@/redux/stable-data/market-charts'
import { addNotification } from '@/components/shared/notifications'
import { toggleWalletForm } from '@/components/pages-protected/wallets'
import { setData as setBalanceData } from '@/redux/stable-data/balance'
import { refreshListWithOneWallet } from '@/redux/stable-data/wallets'
import type { DashboardStateType } from '@/types/state/ui/pages-protected/dashboard'
import type { TransactionType, WalletType, EventType, MarketChartType, GetStateType } from '@/types'

const REQUEST = REQUEST_NAME.UI.DASHBOARD
export const GRID_COUNT = { TRANSACTIONS: 5, MARKET_CHARTS: 7 }

export const onMount = () =>
  (dispatch: Function, getState: GetStateType) => {
    const requests = getState().global.requests
    if (requests[REQUEST.GET_TRANSACTIONS].pending === null) {
      dispatch(getTransactions(null, null, REQUEST.GET_TRANSACTIONS))
    }
    if (requests[REQUEST.GET_UPCOMING_EVENTS].pending === null) {
      dispatch(getUpcomingEvents())
    }
    if (requests[REQUEST.GET_COINS_VALUE].pending === null) {
      dispatch(getCoinsValueByUSD(WALLET_IDS, REQUEST.GET_COINS_VALUE))
    }
    if (requests[REQUEST.GET_MARKET_CHARTS].pending === null) {
      dispatch(getMarketCharts())
    }
    if (requests[REQUEST.GET_MAIN_CHART].pending === null) {
      const { rangeName } = getState().ui.dashboard.mainChart
      dispatch(getMainChart(rangeName))
    }
  }

export const onUnmount = () =>
  (dispatch: Function, getState: GetStateType) => {
    const { list } = getState().stableData.wallets
    const { selectedWalletCoinId } = getState().ui.dashboard
    if (list.length && list[0].coin_id !== selectedWalletCoinId) {
      dispatch(selectWalletCoinId(list[0].coin_id))
    }
  }

export const getTransactionsByIncome = ({ isIncome }: { isIncome: ?boolean }) =>
  (dispatch: Function, getState: GetStateType) => {
    const prevIsIncome = getState().ui.dashboard.transactionsGrid.isIncome
    dispatch(setTransactionsIncomeStatus(isIncome))
    dispatch(getTransactions(isIncome, prevIsIncome, REQUEST.FILTER_TRANSACTIONS))
  }

export const getMoney = () => (dispatch: Function) =>
  dispatch(apiOwnService.getMoney({
    stateName: REQUEST.GET_MONEY
  }))
  .then(({ usd, eur }: Object) => {
    dispatch(setBalanceData({ usd, eur }))
    dispatch(addNotification({
      msg: 'Congratulations! A thousand fake money was transferred to you for each balance (in USD and EUR).'
    }))
  })

export const trySendCoins = ({ amount, sendTo, wallet }: Object) => (dispatch: Function) => {
  if (Number(amount) > wallet.value) {
    const msg = `Not enough funds to complete this transaction. Current value of selected wallet: ${wallet.value} ${wallet.coin_id}`
    dispatch(addNotification({ msg, type: NOTIFICATION_TYPE.ERROR }))
    return
  }
  dispatch(selectWalletCoinId(wallet.coin_id))
  dispatch(sendCoins(wallet.id, amount, sendTo))
}

export const tryReceiveCoins = ({ amount, wallet }: Object) => (dispatch: Function) => {
  dispatch(selectWalletCoinId(wallet.coin_id))
  dispatch(receiveCoins(wallet.id, amount))
}

export const redirectToCreateWallet = () => (dispatch: Function) => {
  dispatch(toggleWalletForm(true))
  history.push({ pathname: ROUTES.WALLETS.default })
}

export const setWalletForMakeTransaction = (wallet: WalletType) =>
  ({ type: types.SET_WALLET_FOR_MAKE_TRANSACTION, wallet })

export const selectWalletCoinId = (walletCoinId: string) =>
  ({ type: types.SELECT_WALLET_COIN_ID, walletCoinId })

export const setVsCurrencyForMarketCharts = (vsCurrency: string) =>
  ({ type: types.SET_VS_CURRENCY_FOR_MARKET_CHARTS, vsCurrency })

const setTransactionsIncomeStatus = (isIncome: ?boolean = null) =>
  ({ type: types.SET_TRANSACTIONS_INCOME_STATUS, isIncome })

const setAllTransactionsTotal = (total: number) =>
  ({ type: types.SET_ALL_TRANSACTIONS_TOTAL, total })

const setMainChart = ({ rangeName, chartData }: { rangeName: string, chartData: MarketChartType }) =>
  ({ type: types.SET_MAIN_CHART, rangeName, chartData })

const getTransactions = (isIncome: ?boolean = null, prevIsIncome: ?boolean = null, stateName: string) =>
  (dispatch: Function) =>
    dispatch(apiOwnService.getTransactions({
      stateName,
      args: {
        count: GRID_COUNT.TRANSACTIONS,
        isIncome
      }
    }))
    .then(({ list, total }: { list: Array<TransactionType>, total: number }) => {
      dispatch({
        type: types.SET_LIST_TRANSACTIONS,
        transactionsGrid: {
          isIncome,
          total,
          list
        }
      })
      if (isIncome === null) {
        dispatch(setAllTransactionsTotal(total))
      }
    })
    .catch(() => dispatch(setTransactionsIncomeStatus(prevIsIncome)))

const getUpcomingEvents = () => (dispatch: Function) =>
  dispatch(apiCoinGecko.getEvents({
    stateName: REQUEST.GET_UPCOMING_EVENTS,
    args: { upcomingEventsOnly: true }
  }))
  .then(({ data, count }: { data: Array<EventType>, count: number }) =>
    dispatch({
      type: types.SET_LIST_UPCOMING_EVENTS,
      upcomingEventsGrid: { count, list: data }
    })
  )

const getMarketCharts = () => (dispatch: Function) => {
  const coinsIdList = CRYPTO_CURRENCY_ARRAY.slice(0, GRID_COUNT.MARKET_CHARTS).map(({ id }: { id: string }) => id)
  dispatch(getCoinsMarketChart(REQUEST.GET_MARKET_CHARTS, MARKET_DATA_RANGE.THREE_MONTHES.name, coinsIdList))
  .then((data: Array<Object>) => {
    dispatch({
      type: types.SET_LIST_MARKET_CHARTS,
      list: data
    })
  })
}

export const getMainChart = (rangeName: string) => (dispatch: Function, getState: GetStateType) => {
  const { cryptoCoinId, paperCoinId } = getState().ui.dashboard.mainChart
  const existedChart = getState().stableData.marketCharts[cryptoCoinId][paperCoinId][rangeName]
  if (existedChart.points.length) {
    dispatch(setMainChart({ rangeName, chartData: existedChart }))
    return
  }
  dispatch(getCoinsMarketChart(REQUEST.GET_MAIN_CHART, rangeName, [cryptoCoinId], [paperCoinId]))
  .then((data: Array<Object>) => {
    dispatch(setMainChart({ rangeName, chartData: data[0] }))
  })
}

const sendCoins = (walletId: string, amount: string, sendTo: string) => (dispatch: Function) =>
  dispatch(apiOwnService.sendCoins({
    stateName: REQUEST.SEND_COINS,
    args: { walletId, amount, sendTo }
  }))
  .then(({ refreshedWallet }: { refreshedWallet: WalletType }) => {
    dispatch(refreshListWithOneWallet(refreshedWallet))
    dispatch(setWalletForMakeTransaction(refreshedWallet))
    dispatch(getTransactionsByIncome({ isIncome: false }))
    dispatch(addNotification({
      msg: 'The transaction has been accepted for processing.'
    }))
  })

const receiveCoins = (walletId: string, amount: string) => (dispatch: Function) =>
  dispatch(apiOwnService.receiveCoins({
    stateName: REQUEST.RECEIVE_COINS,
    args: { walletId, amount }
  }))
  .then(({ refreshedWallet }: { refreshedWallet: WalletType }) => {
    dispatch(refreshListWithOneWallet(refreshedWallet))
    dispatch(setWalletForMakeTransaction(refreshedWallet))
    dispatch(getTransactionsByIncome({ isIncome: true }))
    dispatch(addNotification({
      msg: `Congratulations! A fake ${amount} ${refreshedWallet.coin_id} was transferred to you.`
    }))
  })

const initialState: DashboardStateType = {
  selectedWalletCoinId: null,
  walletForMakeTransaction: null,
  allTransactionsTotal: null,
  transactionsGrid: {
    list: [],
    total: null,
    isIncome: null
  },
  upcomingEventsGrid: {
    list: [],
    count: null
  },
  marketChartsGrid: {
    vsCurrency: PAPER_CURRENCY.USD.id,
    list: []
  },
  mainChart: {
    cryptoCoinId: CRYPTO_CURRENCY.BTC.id,
    paperCoinId: PAPER_CURRENCY.USD.id,
    rangeName: MARKET_DATA_RANGE.THREE_MONTHES.name,
    chartData: null
  }
}

export function reducer(state: DashboardStateType = initialState, action: Object): DashboardStateType {
  switch (action.type) {
    case AUTH.SET_USER_DATA:
      return {
        ...state,
        marketChartsGrid: {
          ...state.marketChartsGrid,
          vsCurrency: getVsCurrency(LOCAL_STORAGE_ITEM.UI_DASHBOARD_MARKET_CHARTS_GRID_VS_CURRENCY)
        },
        mainChart: {
          ...state.mainChart,
          rangeName: getRangeName(LOCAL_STORAGE_ITEM.UI_DASHBOARD_MAIN_CHART_RANGE_NAME)
        }
      }
    case types.SET_LIST_TRANSACTIONS:
      return {
        ...state,
        transactionsGrid: action.transactionsGrid
      }
    case types.SET_ALL_TRANSACTIONS_TOTAL:
      return {
        ...state,
        allTransactionsTotal: action.total
      }
    case types.SET_TRANSACTIONS_INCOME_STATUS:
      return {
        ...state,
        transactionsGrid: {
          ...state.transactionsGrid,
          isIncome: action.isIncome
        }
      }
    case types.SET_LIST_UPCOMING_EVENTS:
      return {
        ...state,
        upcomingEventsGrid: action.upcomingEventsGrid
      }
    case types.SET_LIST_MARKET_CHARTS:
      return {
        ...state,
        marketChartsGrid: {
          ...state.marketChartsGrid,
          list: action.list
        }
      }
    case types.SET_VS_CURRENCY_FOR_MARKET_CHARTS:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_DASHBOARD_MARKET_CHARTS_GRID_VS_CURRENCY, action.vsCurrency)
      return {
        ...state,
        marketChartsGrid: {
          ...state.marketChartsGrid,
          vsCurrency: action.vsCurrency
        }
      }
    case types.SET_WALLET_FOR_MAKE_TRANSACTION:
      return {
        ...state,
        walletForMakeTransaction: action.wallet
      }
    case types.SELECT_WALLET_COIN_ID:
      return {
        ...state,
        selectedWalletCoinId: action.walletCoinId
      }
    case types.SET_MAIN_CHART:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_DASHBOARD_MAIN_CHART_RANGE_NAME, action.rangeName)
      return {
        ...state,
        mainChart: {
          ...state.mainChart,
          rangeName: action.rangeName,
          chartData: action.chartData
        }
      }
    default:
      return state
  }
}
