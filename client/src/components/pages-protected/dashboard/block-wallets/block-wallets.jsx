// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './block-wallets.styles.scss'
import React, { useState } from 'react'
import ScrollBooster from 'scrollbooster'

import { getCoinIcon, getCoinName, getCoinAbbr } from '@/utils/wallets'
import RequestWrapper from '@/generic/request-wrapper'
import type { WalletType } from '@/types'

const SKELETONS_LENGTH = 4

// $flow-disable-line
const getWalletId = (coinId: string) => `wallet-${coinId}`

type PropsType = {|
  pending: boolean,
  list: Array<WalletType>,
  selectedWalletCoinId: ?string,
  selectWalletCoinId: Function,
  redirectToCreateWallet: Function
|}

function BlockWallets({
  pending, list,
  selectedWalletCoinId, selectWalletCoinId,
  redirectToCreateWallet
}: PropsType) {
  if (!selectedWalletCoinId && list.length) {
    selectWalletCoinId(list[0].coin_id)
  }

  const [scroller, setScroller] = useState(null)
  if (list.length && !scroller) {
    setTimeout(() => {
      const viewport = document.querySelector(`.${styles.listWrap}`)
      // $flow-disable-line
      const content = viewport.querySelector(`.${styles.list}`)
      setScroller(new ScrollBooster({
        viewport,
        content,
        mode: 'x',
        bounce: true,
        emulateScroll: true,
        onUpdate: (data: Object) =>
          // $flow-disable-line
          viewport.scrollLeft = data.position.x
      }))
    })
  }

  const [isEnabledManualPositioning, enableManualPositioning] = useState(true)
  const [currentCoinId, setCurrentCoinId] = useState(null)
  if (selectedWalletCoinId && !currentCoinId) {
    setCurrentCoinId(selectedWalletCoinId)
  }
  if (currentCoinId && currentCoinId !== selectedWalletCoinId) {
    if (isEnabledManualPositioning) {
      const elem = document.getElementById(getWalletId(selectedWalletCoinId))
      // $flow-disable-line
      scroller.setPosition({ x: elem.offsetLeft - 100, y: 0 })
    }
    setCurrentCoinId(selectedWalletCoinId)
    enableManualPositioning(true)
  }

  return (
    <div className={styles.blockWallets}>
      <div className={styles.topPanel}>
        <span className={styles.heading}>My wallets</span>
        <span className={styles.addWallet} onClick={redirectToCreateWallet}>Add wallet</span>
      </div>
      <div className={styles.listWrap}>
        <RequestWrapper pending={pending}>
          <div className={styles.list}>
            {pending && !list.length && Array(SKELETONS_LENGTH).fill().map((_: any, i: number) =>
              <div className={`${styles.item} ${styles.inactive} ${styles.skeleton}`} key={i} />
            )}
            {list.map(({ id, coin_id, value }: WalletType) =>
              <div
                key={id}
                id={getWalletId(coin_id)}
                onClick={() => {
                  enableManualPositioning(false)
                  selectWalletCoinId(coin_id)
                }}
                className={`${styles.item} ${selectedWalletCoinId === coin_id ? styles.active : styles.inactive}`}
              >
                <div className={styles.heading}>
                  <div className={styles.walletType}>{getCoinAbbr(coin_id)}</div>
                  <div className={styles.walletTypeIcon}>
                    {getCoinIcon(coin_id)}
                  </div>
                </div>
                <div className={styles.footer}>
                  <div className={styles.walletName}>
                    {getCoinName(coin_id)}
                  </div>
                  {selectedWalletCoinId === coin_id && (
                    <div className={styles.walletValue}>
                      {value}
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        </RequestWrapper>
      </div>
    </div>
  )
}

export default BlockWallets
