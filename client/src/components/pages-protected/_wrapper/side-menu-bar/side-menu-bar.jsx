// @flow
import styles from './side-menu-bar.styles.scss'
import React, { useRef } from 'react'

import { useOutsideEffectByRef, useResizeEffectByRef } from '@/utils/react-effects'
import { ROUTES, APP_SIZE } from '@/utils/constants'
import OpenMessages from '../open-messages'

import LogoIcon from '@/assets/icons/logo'
import DashboardIcon from '@/assets/icons/icons-side-menu/dashboard'
import WalletsIcon from '@/assets/icons/icons-side-menu/wallets'
import MarketsIcon from '@/assets/icons/icons-side-menu/markets'
import EventsIcon from '@/assets/icons/icons-side-menu/events'
import TransactionsIcon from '@/assets/icons/icons-side-menu/transactions'
import CalculatorIcon from '@/assets/icons/icons-side-menu/calculator'
import NewsIcon from '@/assets/icons/icons-side-menu/news'
import SettingsIcon from '@/assets/icons/icons-side-menu/settings'

const getItemStyle = (pathname: string, route: string) =>
  `${styles.item} ${pathname.match(route) ? styles.active : ''}`

type PropsType = {|
  toggleMobileSideMenu: Function,
  mobileSideMenuOpened: boolean,
  onSideMenuClick: Function,
  messagesCount: number,
  nonWorkingNotification: Function,
  isDarkTheme: boolean,
  setDarkTheme: Function,
  pathname: string,
  doLogout: Function
|}

export default function SideMenuBar({
  toggleMobileSideMenu, mobileSideMenuOpened, onSideMenuClick,
  messagesCount, nonWorkingNotification,
  isDarkTheme, setDarkTheme,
  pathname, doLogout
}: PropsType) {
  const mobileSideMenuRef = useRef(null)
  useOutsideEffectByRef(mobileSideMenuRef, toggleMobileSideMenu, mobileSideMenuOpened)
  useResizeEffectByRef(mobileSideMenuRef, ({ width }: { width: number }) => {
    if (mobileSideMenuOpened && width > APP_SIZE.MOBILE_WIDTH) {
      toggleMobileSideMenu()
    }
  })
  return (
    <div className={`${styles.wrap} ${mobileSideMenuOpened ? styles.mobileMenuOpened : ''}`}>
      <div className={styles.sideMenuBar} ref={mobileSideMenuRef}>
        <div className={styles.blockLogo}>
          <a href="/" className={styles.logoWrap}>
            <LogoIcon />
          </a>
        </div>

        <ul className={styles.blockMenu}>
          <div
            className={getItemStyle(pathname, ROUTES.DASHBOARD.default)}
            onClick={() => onSideMenuClick(ROUTES.DASHBOARD.default)}
          >
            <DashboardIcon />
            <span className={styles.itemDesc}>Dashboard</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.WALLETS.default)}
            onClick={() => onSideMenuClick(ROUTES.WALLETS.default)}
          >
            <WalletsIcon />
            <span className={styles.itemDesc}>Wallets</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.MARKETS.default)}
            onClick={() => onSideMenuClick(ROUTES.MARKETS.default)}
          >
            <MarketsIcon />
            <span className={styles.itemDesc}>Markets</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.EVENTS.default)}
            onClick={() => onSideMenuClick(ROUTES.EVENTS.default)}
          >
            <EventsIcon />
            <span className={styles.itemDesc}>Events</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.TRANSACTIONS.default)}
            onClick={() => onSideMenuClick(ROUTES.TRANSACTIONS.default)}
          >
            <TransactionsIcon />
            <span className={styles.itemDesc}>Transactions</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.CALCULATOR.default)}
            onClick={() => onSideMenuClick(ROUTES.CALCULATOR.default)}
          >
            <CalculatorIcon />
            <span className={styles.itemDesc}>Calculator</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.NEWS.default)}
            onClick={() => onSideMenuClick(ROUTES.NEWS.default)}
          >
            <NewsIcon />
            <span className={styles.itemDesc}>News</span>
          </div>
          <div
            className={getItemStyle(pathname, ROUTES.SETTINGS.default)}
            onClick={() => onSideMenuClick(ROUTES.SETTINGS.default)}
          >
            <SettingsIcon />
            <span className={styles.itemDesc}>Settings</span>
          </div>
          <div className={styles.mobileItems}>
            <li className={styles.mobileItem} onClick={() => setDarkTheme(!isDarkTheme)}>
              {isDarkTheme ? 'Set light theme' : 'Set dark theme'}
            </li>
            <li className={styles.mobileItem} onClick={doLogout}>
              Log out
            </li>
          </div>
        </ul>

        <OpenMessages
          messagesCount={messagesCount}
          nonWorkingNotification={nonWorkingNotification}
          isMobile={false}
        />
      </div>
    </div>
  )
}
