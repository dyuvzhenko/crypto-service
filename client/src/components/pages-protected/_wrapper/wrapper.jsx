// @flow
import styles from './wrapper.styles.scss'
import * as React from 'react'
import { connect } from 'react-redux'
import { doLogout } from '@/redux/global/auth'
import { setDarkTheme, toggleMenu, toggleMobileSideMenu, onSideMenuClick } from './wrapper.redux'
import { nonWorkingNotification } from '@/components/shared/notifications'
import type { WrapperProtectedStateType } from '@/types/state/ui/pages-protected/_wrapper'
import type { StateType, UserInfoType } from '@/types'

import SideMenuBar from './side-menu-bar'
import HeaderMainRow from './header-main-row'
import HeaderMenuDropdown from './header-menu-dropdown'
import Notifications from '@/components/shared/notifications'

const mapStateToProps = (state: StateType) => ({
  pathname: state.router.location.pathname,
  messagesCount: state.stableData.messages.list.length,
  wrapperProtected: state.ui.wrapperProtected,
  userInfo: state.global.auth.userInfo
})

const mapDispatchToProps = {
  setDarkTheme,
  onSideMenuClick,
  nonWorkingNotification,
  toggleMobileSideMenu,
  toggleMenu,
  doLogout
}

type PropsType = {|
  wrapperProtected: WrapperProtectedStateType,
  userInfo: UserInfoType,
  setDarkTheme: Function,
  toggleMenu: Function,
  toggleMobileSideMenu: Function,
  onSideMenuClick: Function,
  doLogout: Function,
  nonWorkingNotification: Function,
  children: React.Node,
  setDarkTheme: Function,
  messagesCount: number,
  pathname: string,
  titleRoute: string,
  nestedTitle: string,
  title: string
|}

function WrapperProtected({
  wrapperProtected: { isDarkTheme, menuOpened, mobileSideMenuOpened },
  setDarkTheme, toggleMenu, toggleMobileSideMenu, onSideMenuClick, doLogout, nonWorkingNotification,
  userInfo, pathname, messagesCount,
  title, titleRoute, nestedTitle, children
}: PropsType) {
  return (
    <div className={styles.background}>
      <SideMenuBar
        onSideMenuClick={onSideMenuClick}
        toggleMobileSideMenu={toggleMobileSideMenu}
        mobileSideMenuOpened={mobileSideMenuOpened}
        messagesCount={messagesCount}
        nonWorkingNotification={nonWorkingNotification}
        isDarkTheme={isDarkTheme}
        setDarkTheme={setDarkTheme}
        pathname={pathname}
        doLogout={doLogout}
      />

      <div className={styles.mainColumn}>
        <div className={styles.header}>
          <div className={`col-main ${styles.headerLeftPart}`}>
            <HeaderMainRow
              title={title}
              titleRoute={titleRoute}
              nestedTitle={nestedTitle}
              messagesCount={messagesCount}
              toggleMobileSideMenu={toggleMobileSideMenu}
              nonWorkingNotification={nonWorkingNotification}
            />
          </div>

          <div className={`col-aside ${styles.headerRightPart}`}>
            <HeaderMenuDropdown
              toggle={toggleMenu}
              opened={menuOpened}
              setDarkTheme={setDarkTheme}
              isDarkTheme={isDarkTheme}
              doLogout={doLogout}
              userName={userInfo.nickname}
              userImage={userInfo.image}
            />
            <Notifications wrapStyles={styles.wrapNotifications} />
          </div>
        </div>

        <div className={styles.content}>
          {children}
        </div>
      </div>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(WrapperProtected)
