// @flow
import styles from './header-menu-dropdown.styles.scss'
import React, { useRef } from 'react'
import { getUserPicturePath } from '@/utils/common'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import DefaultUserPictureIcon from '@/assets/icons/default-user-picture'
import MenuIcon from '@/assets/icons/menu'

type PropsType = {|
  toggle: Function,
  opened: boolean,
  isDarkTheme: boolean,
  setDarkTheme: Function,
  userName: string,
  userImage: ?string,
  doLogout: Function
|}

export default function HeaderMenuDropdown({
  toggle, opened,
  isDarkTheme, setDarkTheme,
  userName, userImage, doLogout
}: PropsType) {
  const ref = useRef(null)
  useOutsideEffectByRef(ref, toggle, opened)
  return (
    <div className={styles.blockMainMenu} ref={ref}>
      <div>
        <div className={`${styles.dropdownBtn} ${opened ? styles.opened : ''}`} onClick={toggle}>
          <div className={styles.menuIcon}>
            <MenuIcon />
          </div>
          <div className={styles.userName}>
            <div>
              {userName}
            </div>
          </div>
          <div className={styles.userImage}>
            {userImage ? <img src={getUserPicturePath(userImage)} /> : <DefaultUserPictureIcon />}
          </div>
        </div>
        {opened && (
          <ul className={styles.dropdownMenu}>
            <li
              className={styles.item}
              onClick={() => setDarkTheme(!isDarkTheme)}
            >{isDarkTheme ? 'Set light theme' : 'Set dark theme'}
            </li>
            <li
              className={styles.item}
              onClick={doLogout}
            >Log out
            </li>
          </ul>
        )}
      </div>
    </div>
  )
}
