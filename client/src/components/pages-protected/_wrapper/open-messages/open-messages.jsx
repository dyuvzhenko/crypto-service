// @flow
import styles from './open-messages.styles.scss'
import React from 'react'
import MessagesIcon from '@/assets/icons/icons-side-menu/messages'

type PropsType = {|
  messagesCount: number,
  nonWorkingNotification: Function,
  isMobile: boolean
|}

export default function OpenMessages({ messagesCount, nonWorkingNotification, isMobile }: PropsType) {
  return (
    <div className={`${styles.blockOpenMessages} ${isMobile ? styles.mobile : ''}`}>
      <div className={styles.iconWrap} onClick={nonWorkingNotification}>
        <MessagesIcon />
      </div>
      <div className={styles.messagesCount}>{messagesCount}</div>
    </div>
  )
}
