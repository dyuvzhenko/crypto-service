// @flow
import styles from './header-main-row.styles.scss'
import React from 'react'

import { history } from '@/utils/history'
import OpenMessages from '../open-messages'
import LogoIcon from '@/assets/icons/logo'
import MobileDropdownIcon from '@/assets/icons/mobile-dropdown'
import ExclamationIcon from '@/assets/icons/exclamation'
import SearchIcon from '@/assets/icons/search'

type PropsType = {|
  title: string,
  titleRoute: string,
  nestedTitle: string,
  messagesCount: number,
  toggleMobileSideMenu: Function,
  nonWorkingNotification: Function
|}

export default function HeaderMainRow({
  title, titleRoute, nestedTitle, messagesCount,
  toggleMobileSideMenu, nonWorkingNotification
}: PropsType) {
  return (
    <div className={styles.headerMainRow}>
      <div className={styles.blockHeaderName}>
        <div className={styles.mobileBlockLogo}>
          <a href="/" className={styles.logoWrap}>
            <LogoIcon />
          </a>
        </div>
        <div
          className={`${styles.headerName} ${nestedTitle ? styles.clickable : ''}`}
          onClick={nestedTitle ? () => history.push({ pathname: titleRoute }) : null}
        >{title}
        </div>
        {nestedTitle && (
          <>
            <div className={styles.separator}>/</div>
            <div className={styles.nestedTitle}>{nestedTitle}</div>
          </>
        )}
      </div>

      <div className={styles.blockSearchInput}>
        <input
          placeholder="Search..."
          onKeyPress={(e: Object) => e.key === 'Enter' ? nonWorkingNotification() : null}
        />
        <div className={styles.icon} onClick={nonWorkingNotification}>
          <SearchIcon />
        </div>
      </div>

      <div className={styles.blockSupport}>
        <div className={styles.infoBtn}>
          <div className={styles.icon}>
            <ExclamationIcon />
          </div>
          <a className={styles.text} target="_blank" href="https://www.coingecko.com/api/documentations/v3">
            Powered by CoinGecko API
          </a>
        </div>
      </div>

      <div className={styles.mobileHeader}>
        <OpenMessages
          messagesCount={messagesCount}
          nonWorkingNotification={nonWorkingNotification}
          isMobile={true}
        />
        <div className={styles.mobileDropdownWrap} onClick={toggleMobileSideMenu}>
          <MobileDropdownIcon />
        </div>
      </div>
    </div>
  )
}
