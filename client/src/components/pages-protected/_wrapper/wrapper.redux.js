// @flow
import { WRAPPER_PROTECTED as types } from '@/redux/action-types'
import { history } from '@/utils/history'
import { appendBodyThemeClass } from '@/utils/common'
import { isThemeDark, setThemeDark } from '@/utils/local-storage'
import type { WrapperProtectedStateType } from '@/types/state/ui/pages-protected/_wrapper'
import type { GetStateType } from '@/types'

export const detectUserTheme = () => (dispatch: Function) => {
  const isDark = isThemeDark() === String(true)
  dispatch({ type: types.SET_DARK_THEME, payload: isDark })
  appendBodyThemeClass(isDark)
}

export const setDarkTheme = (isDark: boolean) => (dispatch: Function) => {
  dispatch({ type: types.SET_DARK_THEME, payload: isDark })
  appendBodyThemeClass(isDark)
  setThemeDark(String(isDark))
}

export const onSideMenuClick = (pathname: string) =>
  (dispatch: Function, getState: GetStateType) => {
    history.push({ pathname })
    if (getState().ui.wrapperProtected.mobileSideMenuOpened) {
      dispatch(toggleMobileSideMenu())
    }
  }

export const toggleMenu = () => ({ type: types.TOGGLE_MENU })
export const toggleMobileSideMenu = () => ({ type: types.TOGGLE_MOBILE_SIDE_MENU })

const initialState: WrapperProtectedStateType = {
  isDarkTheme: false,
  mobileSideMenuOpened: false,
  menuOpened: false
}

export function reducer(
  state: WrapperProtectedStateType = initialState,
  action: Object = {}
): WrapperProtectedStateType {
  switch (action.type) {
    case types.SET_DARK_THEME:
      return {
        ...state,
        isDarkTheme: action.payload,
        menuOpened: false
      }
    case types.TOGGLE_MENU:
      return {
        ...state,
        menuOpened: !state.menuOpened
      }
    case types.TOGGLE_MOBILE_SIDE_MENU:
      return {
        ...state,
        mobileSideMenuOpened: !state.mobileSideMenuOpened
      }
    default:
      return state
  }
}
