// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './settings.styles.scss'
import React, { useEffect, useState, useRef }  from 'react'
import { Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import { history } from '@/utils/history'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import { SETTINGS_PAGE } from '@/utils/constants'
import { onMount } from './settings.redux'
import WrapperProtected from '../_wrapper'
import ProfilePage from './pages/profile'
import SecurityPage from './pages/security'
import NotificationPage from './pages/notification'
import ReferralsPage from './pages/referrals'
import EmailSettingsPage from './pages/email-settings'
import SessionsPage from './pages/sessions'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'
import type { SettingsStateType } from '@/types/state/ui/pages-protected/settings'
import type { StateType } from '@/types'

const SETTINGS_PAGE_ARR: Array<Object> = Object.values(SETTINGS_PAGE)

const mapStateToProps = (state: StateType) => ({
  settingsState: state.ui.settings
})

const mapDispatchToProps = {
  onMount
}

type PropsType = {|
  settingsState: SettingsStateType,
  location: Location,
  onMount: Function
|}

function Settings({
  location: { pathname },
  settingsState: { accountProgress },
  onMount
}: PropsType) {
  useEffect(() => onMount(pathname), [])
  const foundPage: Object = SETTINGS_PAGE_ARR.find((page: Object) => page.route === pathname)

  const [opened, toggle] = useState(false)
  const ref = useRef(null)
  useOutsideEffectByRef(ref, () => opened && toggle(false), opened)

  return (
    <WrapperProtected title="Settings">
      <div className={styles.wrap}>
        <div className={`col-main ${styles.pageWrapper}`}>
          <Switch>
            <Route path={SETTINGS_PAGE.PROFILE.route} component={ProfilePage} />
            <Route path={SETTINGS_PAGE.SECURITY.route} component={SecurityPage} />
            <Route path={SETTINGS_PAGE.NOTIFICATION.route} component={NotificationPage} />
            <Route path={SETTINGS_PAGE.REFERRALS.route} component={ReferralsPage} />
            <Route path={SETTINGS_PAGE.EMAIL_SETTINGS.route} component={EmailSettingsPage} />
            <Route path={SETTINGS_PAGE.SESSIONS.route} component={SessionsPage} />
          </Switch>
        </div>

        <div className={`col-aside ${styles.wrapPagesList}`}>
          <div className={`${styles.blockPagesList} ${opened ? styles.opened : ''}`}>
            <div className={styles.selectedItem} onClick={() => toggle(true)}>
              <DotsVerticalIcon />
              {foundPage && foundPage.name}
            </div>
            <div className={styles.list} ref={ref}>
              {SETTINGS_PAGE_ARR.map(({ name, route }: { name: string, route: string }) =>
                <div
                  className={`${styles.item} ${route === pathname ? styles.active : ''}`}
                  onClick={() => {
                    history.push({ pathname: route })
                    toggle(false)
                  }}
                  key={name}
                >{name}
                </div>
              )}
            </div>
          </div>
        </div>

        <div className={`col-aside ${styles.wrapAccountProgress}`}>
          <div className={styles.blockAccountProgress}>
            <div className={styles.desc}>
              Your profile is
              {'\n'}
              {accountProgress}% complete
            </div>
            <div className={styles.bar}>
              <div className={styles.barFill} style={{ width: `${accountProgress}%` }}>
                {accountProgress}%
              </div>
            </div>
            {accountProgress !== 100 && (
              <div className={styles.update}>
                Update
              </div>
            )}
          </div>

        </div>
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
