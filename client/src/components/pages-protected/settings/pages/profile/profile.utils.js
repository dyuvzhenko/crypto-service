// @flow
/*eslint no-magic-numbers: ["off"]*/
import dayjs from 'dayjs'
import { composeValidators, required, mustBeEmail, minValueString } from '@/utils/validators'
import { ARR_MONTHS, SIGNUP_RULES } from '@/utils/constants'

export const getDateObject = (date: ?string) => ({
  year: date && date.slice(0, 4) || null,
  month: date && date.slice(5, 7) || null,
  day: date && date.slice(8, 10) || null
})

const DEFAULT_OPTION = { text: '-', value: null }

export const getYears = () => {
  const currentYear = dayjs().format('YYYY')
  return [
    DEFAULT_OPTION,
    ...Array(100).fill().map((e: any, i: number) => ({ text: currentYear - i, value: currentYear - i }))
  ]
}

export const getMonths = () => [
  DEFAULT_OPTION,
  ...ARR_MONTHS.map((text: string, value: number) => ({ text, value }))
]

export const getDays = ({ year, month }: { month: ?number, year: ?number } = {}) => {
  const date = year && month && dayjs(`${year}-${month}`)
  return [
    DEFAULT_OPTION,
    ...Array(
      (date && date.isValid()) ? date.daysInMonth() : 31
    ).fill().map((e: any, i: number) => ({ text: i + 1, value: i + 1 }))
  ]
}

export const recalculateDays = ({ day, year, month }: { day: ?number, month: ?number, year: ?number } = {}) => {
  if (!day) {
    return null
  }
  const maxValue = (year && month) ? dayjs(`${year}-${month}`).daysInMonth() : 31
  return day <= maxValue ? day : maxValue
}

const dateValidator = ({ day, month, year }: { day: ?number, month: ?number, year: ?number }) => {
  if (day || month || year) {
    if (day && month && year) {
      return dayjs(`${year}-${month}-${day}`).isValid() ? undefined : 'Invalid date'
    } else {
      return 'Date must be complete'
    }
  }
  return undefined
}

export const validators = {
  currentPassword: required,
  newPassword: composeValidators(required, minValueString(SIGNUP_RULES.PASSWORD_LENGTH)),
  nickname: composeValidators(required, minValueString(SIGNUP_RULES.NAME_LENGTH)),
  email: composeValidators(required, mustBeEmail),
  birthdate: dateValidator
}
