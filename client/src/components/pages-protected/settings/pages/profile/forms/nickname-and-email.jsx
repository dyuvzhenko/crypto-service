// @flow
import styles from '../profile.styles.scss'
import commonStyles from '@/styles/common.scss'
import React, { useState } from 'react'
import { Prompt } from 'react-router-dom'
import { Form, Field } from 'react-final-form'

import { validators } from '../profile.utils'
import RequestButton from '@/generic/request-button'
import type { UserInfoType } from '@/types'

const isFormValuesTheSame = (values: Object, initialValues: Object) =>
  initialValues.nickname === values.nickname && initialValues.email === values.email

type PropsType = {|
  requestPending: boolean,
  userInfo: UserInfoType,
  updateNicknameAndEmail: Function
|}

export default function NicknameAndEmailForm({ requestPending, userInfo, updateNicknameAndEmail }: PropsType) {
  const [initialValues] = useState({
    nickname: userInfo.nickname,
    email: userInfo.email
  })

  return (
    <Form
      onSubmit={updateNicknameAndEmail}
      initialValues={initialValues}
      render={({ form, initialValues, values, handleSubmit, valid }: Object) => (
        <div className={styles.defaultRow}>
          <Field name="nickname" validate={validators.nickname}>
            {({ input, meta }: Object) => (
              <div className={styles.fieldRow}>
                <div className={styles.desc}>
                  <div className={styles.title}>
                    Nick Name
                    {meta.error && (
                      <span className={styles.error}>
                        ({meta.error})
                      </span>
                    )}
                  </div>
                  <div className={styles.additionalInfo}>
                    This name will be part of your public profile.
                  </div>
                </div>
                <div className={styles.singleInput}>
                  <input
                    {...input}
                    className={commonStyles.input}
                    placeholder="Ivanushka"
                  />
                </div>
              </div>
            )}
          </Field>

          <Field name="email" validate={validators.email}>
            {({ input, meta }: Object) => (
              <div className={styles.fieldRow}>
                <div className={styles.desc}>
                  <div className={styles.title}>
                    Email
                    {meta.error && (
                      <span className={styles.error}>
                        ({meta.error})
                      </span>
                    )}
                  </div>
                </div>
                <div className={styles.singleInput}>
                  <input
                    {...input}
                    className={commonStyles.input}
                    placeholder="ivanov@ivan.com"
                  />
                </div>
              </div>
            )}
          </Field>

          <div className={`${styles.control} ${valid ? '' : styles.disabled}`}>
            {!isFormValuesTheSame(values, initialValues) && (
              <>
                <Prompt when={true} message="All changes will be lost" />
                <button
                  className={`${commonStyles.button} ${styles.resetBtn}`}
                  onClick={form.reset}
                >Reset
                </button>
              </>
            )}
            <RequestButton
              props={{
                className: `${commonStyles.button} ${styles.btnNickName}`,
                disabled: requestPending,
                onClick: handleSubmit
              }}
              pending={requestPending}
              text="Save"
            />
          </div>
        </div>
      )}
    />
  )
}
