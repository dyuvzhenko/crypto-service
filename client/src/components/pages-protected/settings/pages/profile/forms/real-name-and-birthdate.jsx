// @flow
import styles from '../profile.styles.scss'
import commonStyles from '@/styles/common.scss'
import React, { useState } from 'react'
import { Prompt } from 'react-router-dom'
import { Form, Field } from 'react-final-form'
import dayjs from 'dayjs'

import { getDateObject, getYears, getMonths, getDays, recalculateDays, validators } from '../profile.utils'
import RequestButton from '@/generic/request-button'
import Select from '@/generic/select'
import type { UserInfoType } from '@/types'

const isFormValuesTheSame = (values: Object, initialValues: Object) =>
  values.first_name === initialValues.first_name
    && values.last_name === initialValues.last_name
    && values.birthdate.year == initialValues.birthdate.year
    && values.birthdate.month == initialValues.birthdate.month
    && values.birthdate.day == initialValues.birthdate.day

type PropsType = {|
  updateRealNameAndBirthdate: Function,
  requestPending: boolean,
  userInfo: UserInfoType
|}

export default function RealNameAndBirthdateForm({
  updateRealNameAndBirthdate,
  requestPending,
  userInfo
}: PropsType) {
  const [initialValues] = useState({
    first_name: userInfo.first_name,
    last_name: userInfo.last_name,
    birthdate: getDateObject(userInfo.birthdate)
  })

  return (
    <Form
      onSubmit={updateRealNameAndBirthdate}
      initialValues={initialValues}
      render={({ form, handleSubmit, valid, initialValues, values, errors }: Object) => (
        <div className={styles.defaultRow}>
          <div className={styles.fieldRow}>
            <div className={styles.desc}>
              <div className={styles.title}>
                Personal Detail
                {errors.birthdate && (
                  <span className={styles.error}>
                    ({errors.birthdate})
                  </span>
                )}
              </div>
              <div className={styles.additionalInfo}>
                Your personal information is never to other users.
              </div>
            </div>
            <div className={styles.twoInputs}>
              <label>Legal Name</label>
              <div className={styles.wrapInputs}>
                <Field name="first_name">
                  {({ input }: Object) => (
                    <div>
                      <input {...input} className={commonStyles.input} placeholder="Ivan" />
                    </div>
                  )}
                </Field>
                <Field name="last_name">
                  {({ input }: Object) => (
                    <div>
                      <input {...input} className={commonStyles.input} placeholder="Ivanov" />
                    </div>
                  )}
                </Field>
              </div>
            </div>
          </div>

          <div className={styles.fieldRow}>
            <div className={styles.desc}></div>
            <div className={styles.birthdaySelects}>
              <label>Date of birth</label>
              <Field name="birthdate" validate={validators.birthdate}>
                {() => (
                  <div className={styles.rowSelects}>
                    <div className={styles.selectBox}>
                      <Select
                        options={getMonths()}
                        parentStyles={{ selectedItem: styles.selectedItem, item: styles.item }}
                        handleClick={(month: number) => form.change('birthdate', {
                          ...values.birthdate,
                          day: recalculateDays({ ...values.birthdate, month: month + 1 }),
                          month: month === null ? null : month + 1
                        })}
                        selectedItem={values.birthdate.month
                          ? dayjs().month(values.birthdate.month - 1).format('MMMM')
                          : 'Month'
                        }
                      />
                    </div>
                    <div className={styles.selectBox}>
                      <Select
                        options={getDays(values.birthdate)}
                        handleClick={(day: number) => form.change('birthdate', { ...values.birthdate, day })}
                        parentStyles={{ selectedItem: styles.selectedItem, item: styles.item }}
                        selectedItem={values.birthdate.day || 'Day'}
                      />
                    </div>
                    <div className={styles.selectBox}>
                      <Select
                        options={getYears()}
                        parentStyles={{ selectedItem: styles.selectedItem, item: styles.item }}
                        selectedItem={values.birthdate.year || 'Year'}
                        handleClick={(year: number) => form.change('birthdate', {
                          ...values.birthdate,
                          day: recalculateDays({ ...values.birthdate, year }),
                          year,
                        })}
                      />
                    </div>
                  </div>
                )}
              </Field>
            </div>
          </div>

          <div className={`${styles.control} ${valid ? '' : styles.disabled}`}>
            {!isFormValuesTheSame(values, initialValues) && (
              <>
                <Prompt when={true} message="All changes will be lost" />
                <button
                  className={`${commonStyles.button} ${styles.resetBtn}`}
                  onClick={form.reset}
                >Reset
                </button>
              </>
            )}
            <RequestButton
              props={{
                className: `${commonStyles.button} ${styles.btnPersonalDetail}`,
                disabled: requestPending,
                onClick: handleSubmit
              }}
              pending={requestPending}
              text="Save"
            />
          </div>
        </div>
      )}
    />
  )
}
