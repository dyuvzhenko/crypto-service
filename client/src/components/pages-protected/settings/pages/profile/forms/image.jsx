// @flow
import styles from '../profile.styles.scss'
import React from 'react'

import { getUserPicturePath } from '@/utils/common'
import RequestWrapper from '@/generic/request-wrapper'
import DownloadIcon from '@/assets/icons/download'
import DefaultUserPictureIcon from '@/assets/icons/default-user-picture'
import type { UserInfoType } from '@/types'

type PropsType = {|
  userInfo: UserInfoType,
  requestPending: boolean,
  tryDownloadUserPicture: Function
|}

export default function ImageForm({ userInfo, requestPending, tryDownloadUserPicture }: PropsType) {
  return (
    <div className={styles.fieldPicture}>
      <RequestWrapper pending={requestPending}>
        <div className={styles.requestSubWrapper}>
          <div className={styles.userImage}>
            {userInfo.image ? <img src={getUserPicturePath(userInfo.image)} /> : <DefaultUserPictureIcon />}
          </div>
          <div className={styles.wrapPictureDesc}>
            <div className={styles.desc}>
              <div className={styles.title}>Change Picture</div>
              <div className={styles.restrict}>Max file is 10Mb</div>
            </div>
            <div className={styles.upload}>
              <label htmlFor="picture-upload">
                <DownloadIcon />
                Upload
              </label>
              <input
                id="picture-upload"
                type="file"
                accept="image/*"
                multiple={false}
                onChange={(event: Object) => tryDownloadUserPicture(event.target.files[0])}
              />
            </div>
          </div>
        </div>
      </RequestWrapper>
    </div>
  )
}
