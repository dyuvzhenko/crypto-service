// @flow
import styles from '../profile.styles.scss'
import commonStyles from '@/styles/common.scss'
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Form, Field } from 'react-final-form'

import { validators } from '../profile.utils'
import { SETTINGS_PAGE } from '@/utils/constants'
import RequestButton from '@/generic/request-button'
import Modal from '@/generic/modal'

type PropsType = {|
  requestPending: boolean,
  updatePassword: Function
|}

export default function PasswordForm({ requestPending, updatePassword }: PropsType) {
  const [openedPasswordModal, togglePasswordModal] = useState(false)
  const [initialValues] = useState({
    currentPassword: '',
    newPassword: '',
    confirmPassword: ''
  })

  return (
    <div className={styles.fieldPassword}>
      <div className={styles.desc}>
        <div className={styles.title}>Change Password</div>
        <div className={styles.additionalInfo}>
          Enable 2-factor authentication
          {'\n'}
          on the <Link to={SETTINGS_PAGE.SECURITY.route} className={styles.active}>security page</Link>
        </div>
      </div>
      <div className={styles.btnChangePassword}>
        <button className={commonStyles.button} onClick={() => togglePasswordModal(true)}>
          Change Password
        </button>
        <Modal
          isOpen={openedPasswordModal}
          onClose={() => togglePasswordModal(false)}
          title="Change Password"
        >
          <Form
            onSubmit={(...args: Array<any>) => updatePassword(...args)(togglePasswordModal)}
            initialValues={initialValues}
            render={({ values, handleSubmit, valid }: Object) => (
              <div className={styles.passwordModal}>
                <Field name="currentPassword" validate={validators.currentPassword}>
                  {({ input, meta }: Object) => (
                    <div className={styles.passwordFieldRow}>
                      <div className={styles.desc}>
                        Current Password
                        {(meta.touched && meta.error) && (
                          <span className={styles.error}>
                            ({meta.error})
                          </span>
                        )}
                      </div>
                      <div className={styles.passwordInput}>
                        <input
                          {...input}
                          className={commonStyles.input}
                          autoComplete="new-password"
                          placeholder="******"
                          type="password"
                        />
                      </div>
                    </div>
                  )}
                </Field>

                <Field name="newPassword" validate={validators.newPassword}>
                  {({ input, meta }: Object) => (
                    <div className={styles.passwordFieldRow}>
                      <div className={styles.desc}>
                        New Password
                        {(meta.touched && meta.error) && (
                          <span className={styles.error}>
                            ({meta.error})
                          </span>
                        )}
                      </div>
                      <div className={styles.passwordInput}>
                        <input
                          {...input}
                          className={commonStyles.input}
                          autoComplete="new-password"
                          placeholder="******"
                          type="password"
                        />
                      </div>
                    </div>
                  )}
                </Field>

                <Field name="confirmPassword">
                  {({ input, meta }: Object) => (
                    <div className={styles.passwordFieldRow}>
                      <div className={styles.desc}>
                        Confirm New Password
                        {(meta.touched && values.newPassword !== values.confirmPassword) && (
                          <span className={styles.error}>
                            (Must match)
                          </span>
                        )}
                      </div>
                      <div className={styles.passwordInput}>
                        <input
                          {...input}
                          className={commonStyles.input}
                          autoComplete="new-password"
                          placeholder="******"
                          type="password"
                        />
                      </div>
                    </div>
                  )}
                </Field>

                <div className={`${styles.control} ${(valid && values.newPassword === values.confirmPassword) ? '' : styles.disabled}`}>
                  <RequestButton
                    props={{
                      className: `${commonStyles.button} ${styles.btnPassword}`,
                      disabled: requestPending,
                      onClick: handleSubmit
                    }}
                    pending={requestPending}
                    text="Save"
                  />
                </div>
              </div>
            )}
          />
        </Modal>
      </div>
    </div>
  )
}
