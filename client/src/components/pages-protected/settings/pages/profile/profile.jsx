// @flow
import styles from './profile.styles.scss'
import React from 'react'
import { connect } from 'react-redux'

import { REQUEST_NAME } from '@/redux/global/requests'
import {
  tryDownloadUserPicture,
  updateNicknameAndEmail,
  updateRealNameAndBirthdate,
  updatePassword
} from '../../settings.redux'

import ImageForm from './forms/image'
import PasswordForm from './forms/password'
import NicknameAndEmailForm from './forms/nickname-and-email'
import RealNameAndBirthdateForm from './forms/real-name-and-birthdate'
import type { StateType, UserInfoType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  mainPending: state.global.requests[REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_NICKNAME_AND_EMAIL].pending,
  secondPending: state.global.requests[REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_REAL_NAME_AND_BIRTHDATE].pending,
  passwordPending: state.global.requests[REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_NEW_PASSWORD].pending,
  uploadPicturePending: state.global.requests[REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_USER_PICTURE].pending,
  userInfo: state.global.auth.userInfo
})

const mapDispatchToProps = {
  tryDownloadUserPicture,
  updateNicknameAndEmail,
  updateRealNameAndBirthdate,
  updatePassword
}

type PropsType = {|
  userInfo: UserInfoType,
  tryDownloadUserPicture: Function,
  updatePassword: Function,
  updateNicknameAndEmail: Function,
  updateRealNameAndBirthdate: Function,
  mainPending: boolean,
  secondPending: boolean,
  passwordPending: boolean,
  uploadPicturePending: boolean
|}

function ProfilePage({
  userInfo,
  tryDownloadUserPicture, updatePassword,
  updateNicknameAndEmail, updateRealNameAndBirthdate,
  mainPending, secondPending, passwordPending, uploadPicturePending
}: PropsType) {
  return (
    <div className={styles.wrap}>
      <div className={styles.topRow}>
        <ImageForm
          requestPending={uploadPicturePending}
          tryDownloadUserPicture={tryDownloadUserPicture}
          userInfo={userInfo}
        />
        <PasswordForm
          requestPending={passwordPending}
          updatePassword={updatePassword}
        />
      </div>

      <NicknameAndEmailForm
        requestPending={mainPending}
        updateNicknameAndEmail={updateNicknameAndEmail}
        userInfo={userInfo}
      />

      <RealNameAndBirthdateForm
        requestPending={secondPending}
        updateRealNameAndBirthdate={updateRealNameAndBirthdate}
        userInfo={userInfo}
      />
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage)
