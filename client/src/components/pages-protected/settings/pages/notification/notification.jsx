// @flow
import styles from './notification.styles.scss'
import React from 'react'

function NotificationPage() {
  return (
    <div className={styles.wrap}>
      Notification Page
    </div>
  )
}

export default NotificationPage
