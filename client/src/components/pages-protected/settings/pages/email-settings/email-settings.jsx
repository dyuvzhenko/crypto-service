// @flow
import styles from './email-settings.styles.scss'
import React from 'react'

function EmailSettingsPage() {
  return (
    <div className={styles.wrap}>
      Email Settings Page
    </div>
  )
}

export default EmailSettingsPage
