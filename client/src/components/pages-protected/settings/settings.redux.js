// @flow
import dayjs from 'dayjs'
import { apiOwnService } from '@/api'
import { setToken } from '@/utils/local-storage'
import { setUserAuthData } from '@/redux/global/auth'
import { SETTINGS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { DATE_FORMAT } from '@/utils/dates'
import { history } from '@/utils/history'
import { USER_IMAGE_MAX_SIZE, SETTINGS_PAGE, SETTINGS_PAGE_ARRAY, NOTIFICATION_TYPE } from '@/utils/constants'
import { getDateObject } from './pages/profile/profile.utils'
import { addNotification } from '@/components/shared/notifications'
import type { SettingsStateType } from '@/types/state/ui/pages-protected/settings'
import type { GetStateType, UserInfoType } from '@/types'

export const onMount = (pathname: string) => (dispatch: Function) => {
  if (!SETTINGS_PAGE_ARRAY.some(({ route }: { route: string }) => route === pathname)) {
    history.push({ pathname: SETTINGS_PAGE.PROFILE.route })
  }
  dispatch(calculateProgress())
}

export const setAccountProgress = (accountProgress: number) => ({ type: types.SET_ACCOUNT_PROGRESS, accountProgress })

export const tryDownloadUserPicture = (file: File) => (dispatch: Function) => {
  if (file.size > USER_IMAGE_MAX_SIZE) {
    dispatch(addNotification({
      msg: 'The file exceeds the allowed size!',
      type: NOTIFICATION_TYPE.ERROR
    }))
    return
  }
  dispatch(apiOwnService.updateUserPicture({
    stateName: REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_USER_PICTURE,
    args: { file }
  }))
  .then(({ token, userInfo }: { token: string, userInfo: UserInfoType }) => {
    dispatch(applyChanges(userInfo, token))
    dispatch(calculateProgress())
  })
}

export const updateNicknameAndEmail = (values: Object, form: Object, callback: Function) =>
  (dispatch: Function) => {
    dispatch(apiOwnService.updateUserInfo({
      stateName: REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_NICKNAME_AND_EMAIL,
      args: values
    }))
    .then(({ token, userInfo }: { token: string, userInfo: UserInfoType }) => {
      dispatch(applyChanges(userInfo, token))
      dispatch(calculateProgress())
      form.reset({
        nickname: userInfo.nickname,
        email: userInfo.email
      })
      callback()
    })
  }

export const updateRealNameAndBirthdate = (values: Object, form: Object, callback: Function) =>
  (dispatch: Function) => {
    const date = dayjs(`${values.birthdate.year}-${values.birthdate.month}-${values.birthdate.day}`)
    dispatch(apiOwnService.updateUserInfo({
      stateName: REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_REAL_NAME_AND_BIRTHDATE,
      args: {
        first_name: values.first_name || '',
        last_name: values.last_name || '',
        birthdate: date.isValid() ? date.format(DATE_FORMAT.SYSTEM) : null
      }
    }))
    .then(({ token, userInfo }: { token: string, userInfo: UserInfoType }) => {
      dispatch(applyChanges(userInfo, token))
      dispatch(calculateProgress())
      form.reset({
        first_name: userInfo.first_name,
        last_name: userInfo.last_name,
        birthdate: getDateObject(userInfo.birthdate)
      })
      callback()
    })
  }

export const updatePassword = (values: Object, form: Object, callback: Function) =>
  (dispatch: Function) =>
    (togglePasswordModal: Function) => {
      if (values.newPassword !== values.confirmPassword) {
        dispatch(addNotification({ msg: 'Password must match' }))
        return
      }
      dispatch(apiOwnService.updateUserPassword({
        stateName: REQUEST_NAME.UI.SETTINGS.PROFILE.SAVE_NEW_PASSWORD,
        args: {
          currentPassword: values.currentPassword,
          newPassword: values.newPassword
        }
      }))
      .then(({ token, userInfo }: { token: string, userInfo: UserInfoType }) => {
        dispatch(applyChanges(userInfo, token))
        dispatch(calculateProgress())
        togglePasswordModal(false)
        callback()
      })
    }

const applyChanges = (userInfo: UserInfoType, token: string) => (dispatch: Function) => {
  dispatch(addNotification({ msg: 'Your personal data has been updated.' }))
  dispatch(setUserAuthData(userInfo, token))
  setToken(token)
}

const userInfoFieldsValue = {
  image: 40,
  email: 15,
  nickname: 15,
  first_name: 10,
  last_name: 10,
  birthdate: 10
}

export const calculateProgress = () => (dispatch: Function, getState: GetStateType) => {
  const { userInfo } = getState().global.auth
  const accountProgress = Object.keys(userInfoFieldsValue).reduce((acc: number, field: string) => {
    if (userInfo && userInfo[field]) {
      acc = acc + userInfoFieldsValue[field]
    }
    return acc
  }, 0)
  dispatch(setAccountProgress(accountProgress))
}

const initialState: SettingsStateType = {
  accountProgress: 0
}

export function reducer(state: SettingsStateType = initialState, action: Object): SettingsStateType {
  switch (action.type) {
    case types.SET_ACCOUNT_PROGRESS:
      return {
        ...state,
        accountProgress: action.accountProgress
      }
    default:
      return state
  }
}
