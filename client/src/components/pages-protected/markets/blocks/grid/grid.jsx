// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './grid.styles.scss'
import React from 'react'
import Chartist from 'chartist'

import { REQUESTED_LENGTH } from '../../markets.redux'
import { GRID_LOOK } from '@/utils/constants'
import { getBalanceAbbr } from '@/utils/wallets'
import RequestWrapper from '@/generic/request-wrapper'
import Pagination from '@/generic/pagination'
import type { MarketChartType } from '@/types'

const mapGraphColor = (growth: number) => {
  if (growth < 0) {
    return styles.negative
  }
  if (growth > 1) {
    return styles.positive
  }

  return styles.neutral
}

const getGraphOptions = (graphGridShowed: boolean) => ({
  fullWidth: true,
  showPoint: false,
  lineSmooth: false,
  height: 200,
  axisX: {
    showGrid: false,
    showLabel: false,
    offset: 10
  },
  axisY: {
    showGrid: graphGridShowed,
    showLabel: graphGridShowed,
    offset: graphGridShowed ? 40 : 15
  }
})

type PropsType = {|
  requestPending: boolean,
  toggleDetailedGraph: Function,
  getMarketCharts: Function,
  graphGridShowed: boolean,
  gridLook: string,
  grid: Object
|}

function MarketsGrid({
  requestPending, gridLook, graphGridShowed, grid,
  getMarketCharts, toggleDetailedGraph
}: PropsType) {
  return (
    <RequestWrapper fixed={true} pending={requestPending}>
      <div className={`${styles.grid} ${gridLook === GRID_LOOK.ROWS ? styles.lookRows : styles.lookBlocks}`}>
        {requestPending && !grid.list.length && Array(REQUESTED_LENGTH).fill().map((_: any, i: number) =>
          <div className={`${styles.item} ${styles.skeleton}`} key={i} />
        )}
        {grid.list.map((chart: MarketChartType, index: number) => {
          setTimeout(() => {
            new Chartist.Line(
              `#ct-chart-${index}`,
              { series: [chart.points] },
              getGraphOptions(graphGridShowed)
            )
          })

          return (
            <div
              key={index}
              className={`${styles.item} ${graphGridShowed ? styles.extended : ''}`}
              onClick={() => toggleDetailedGraph(true, { from: chart.cryptoCoinId, to: chart.paperCoinId })}
            >
              <div className={styles.heading}>
                <div className={styles.coinNames}>
                  {`${chart.cryptoCoinId}/${chart.paperCoinId}`}
                </div>
                <div className={styles.lastValue}>
                  {chart.lastValue}
                  <span className={styles.currency}>
                    {` ${chart.paperCoinId}`}
                  </span>
                </div>
              </div>

              <div className={`${styles.wrapGraph} ${mapGraphColor(chart.lastGrowth)}`}>
                <div className="ct-chart" id={`ct-chart-${index}`}></div>
              </div>

              <div className={styles.footer}>
                {graphGridShowed && (
                  <div className={styles.gridLabel}>Data for the past 6 months</div>
                )}
                <div className={styles.blockData}>
                  <div className={styles.volume}>
                    Volume:
                    <span className={styles.value}>{` ${chart.lastVolume} `}</span>
                    {getBalanceAbbr(chart.paperCoinId)}
                  </div>
                  <div className={`${styles.percentageGrowth} ${mapGraphColor(chart.lastGrowth)}`}>
                    {chart.lastGrowth}%
                  </div>
                </div>
              </div>
            </div>
          )
        })}
      </div>

      <div className={styles.wrapPagination}>
        {!!grid.list.length && grid.pages > 0 && (
          <Pagination
            onPageChange={(page: number) => getMarketCharts({ page })}
            pages={grid.pages}
            page={grid.page}
          />
        )}
      </div>
    </RequestWrapper>
  )
}

export default React.memo<PropsType>(MarketsGrid)
