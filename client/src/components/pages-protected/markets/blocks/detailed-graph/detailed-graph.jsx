// @flow
import styles from './detailed-graph.styles.scss'
import React from 'react'
import Chartist from 'chartist'

import { getMeta, controls, mapGraphColor, mapLastGrowthColor, getGraphOptions } from './detailed-graph.utils'
import { getReducedLabels } from '@/utils/charts'
import { getCoinName, getCoinAbbr, getBalanceAbbr } from '@/utils/wallets'
import MinimizeIcon from '@/assets/icons/minimize'
import RequestWrapper from '@/generic/request-wrapper'
import CalendarDayPicker from '@/generic/calendar-day-picker'
import Modal from '@/generic/modal'
import type { MarketChartType, PointType } from '@/types'

type PropsType = {|
  pending: boolean,
  toggleDetailedGraph: Function,
  setSelectedInterval: Function,
  getMarketDetailedChart: Function,
  isModalOpen: boolean,
  rangeName: string,
  graphData: ?MarketChartType,
  exchangePair: {
    from: string,
    to: string
  },
  selectedInterval: {
    start: ?string,
    end: ?string
  }
|}

function DetailedGraph({
  pending, isModalOpen, rangeName, graphData, exchangePair, selectedInterval,
  toggleDetailedGraph, setSelectedInterval, getMarketDetailedChart
}: PropsType) {
  if (isModalOpen) {
    setTimeout(() => {
      new Chartist.Line(
        `.${styles.graph}`,
        {
          labels: graphData ? getReducedLabels(graphData.points, rangeName, true) : [],
          series: [
            !graphData ? [] : graphData.points.map((point: PointType) => ({
              meta: getMeta(point, exchangePair),
              value: point.value
            }))
          ]
        },
        getGraphOptions(selectedInterval)
      )
    })
  }

  return (
    <Modal
      isOpen={isModalOpen}
      showHeading={false}
      onClose={() => toggleDetailedGraph(false)}
      contentStyles={styles.modalStyles}
      title="detailed-graph"
    >
      <RequestWrapper pending={pending}>
        <div className={styles.wrap}>
          <div className={styles.mainHeading}>
            <div className={styles.title}>{getCoinName(exchangePair.from)} Chart</div>
            <div className={styles.controlsRange}>
              {controls.map(({ text, value }: Object, i: number) => (
                <div
                  key={i}
                  onClick={() => getMarketDetailedChart(value)}
                  className={`${styles.item} ${(value === rangeName) ? styles.active : ''}`}
                >{text}
                </div>
              ))}
            </div>
            <div className={styles.rowInterval}>
              <div className={styles.blockMonth}>
                <div className={styles.desc}>From</div>
                <CalendarDayPicker
                  positionStyle={styles.positionForLeftCalendar}
                  selectedDate={selectedInterval.start}
                  showIntervalForwardTo={selectedInterval.end}
                  handleSelect={(date: string) => setSelectedInterval({ start: date })}
                />
              </div>
              <div className={styles.blockMonth}>
                <div className={styles.desc}>To</div>
                <CalendarDayPicker
                  positionStyle={styles.positionForRightCalendar}
                  selectedDate={selectedInterval.end}
                  showIntervalBackwardTo={selectedInterval.start}
                  handleSelect={(date: string) => setSelectedInterval({ end: date })}
                />
              </div>
            </div>
            <div className={styles.minimizeIcon} onClick={() => toggleDetailedGraph(false)}>
              <MinimizeIcon />
            </div>
          </div>
          <div className={styles.secondHeading}>
            <div className={styles.blockExchange}>
              {getCoinAbbr(exchangePair.from)}
              /
              {getBalanceAbbr(exchangePair.to)}
            </div>
            <div className={`${styles.blockLastGrowth} ${graphData ? mapLastGrowthColor(graphData.lastGrowth) : ''}`}>
              {graphData ? `${graphData.lastGrowth}%` : null}
            </div>
            <div className={styles.blockLastValue}>
              {graphData ? graphData.lastValue : null}
              {' '}
              <span className={styles.exchange}>
                {getBalanceAbbr(exchangePair.to)}
              </span>
            </div>
          </div>

          <div className={`${styles.graph} ${mapGraphColor(graphData ? graphData.lastGrowth : 0)}`} />
        </div>
      </RequestWrapper>
    </Modal>
  )
}

export default React.memo<PropsType>(DetailedGraph)
