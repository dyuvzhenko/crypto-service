// @flow
/*eslint no-magic-numbers: ["off"]*/
import dayjs from 'dayjs'
import commonStyles from '@/styles/common.scss'
import styles from './detailed-graph.styles.scss'
import ChartistTooltip from 'chartist-plugin-tooltips-updated'
import { TransformPointsIntoLine } from '@/utils/chartist-plugins'

import { MARKET_DATA_RANGE } from '@/utils/constants'
import { getCoinName, getBalanceAbbr } from '@/utils/wallets'
import type { PointType } from '@/types'

export const getMeta = (point: PointType, { from, to }: Object) =>
  getCoinName(from) + ' Price: ' + point.value + ' ' + getBalanceAbbr(to) + '\n' +
  dayjs(point.date).format('HH:MM, dddd, MMM DD, YYYY')

export const controls = [
  { text: '1m', value: MARKET_DATA_RANGE.ONE_MONTH.name },
  { text: '3m', value: MARKET_DATA_RANGE.THREE_MONTHES.name },
  { text: '6m', value: MARKET_DATA_RANGE.SIX_MONTHES.name },
  { text: '1y', value: MARKET_DATA_RANGE.ONE_YEAR.name }
]

export const mapGraphColor = (growth: number) => {
  if (growth < 0) {
    return styles.negative
  }
  if (growth > 0.5) {
    return styles.positive
  }

  return styles.neutral
}

export const mapLastGrowthColor = (growth: number) => {
  if (growth < 0) {
    return styles.negativeGrowth
  }
  if (growth > 0.5) {
    return styles.positiveGrowth
  }

  return styles.neutralGrowth
}

const millisecondsToShowLabels = 1000 * 60 * 60 * 24 * 5

export const getGraphOptions = ({ start, end }: Object) => ({
  plugins: [
    TransformPointsIntoLine({ height: 500 }),
    ChartistTooltip({
      class: commonStyles.tooltipOverwrite,
      transformTooltipTextFnc: () => null
    })
  ],
  fullWidth: true,
  showPoint: true,
  lineSmooth: false,
  height: 500,
  chartPadding: {
    right: 0,
    left: 5
  },
  axisX: {
    showGrid: false,
    showLabel: (start && end) ? dayjs(end).diff(dayjs(start)) > millisecondsToShowLabels : true
  },
  axisY: {
    labelInterpolationFnc: (value: number) => value > 1000 ? (value / 1000) + 'k' : value,
    showGrid: true,
    showLabel: true
  }
})
