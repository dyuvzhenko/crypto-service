// @flow
import dayjs from 'dayjs'

import { AUTH, MARKETS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { getCoinsMarketChart } from '@/redux/stable-data/market-charts'
import { getGraphGridShowed, getGridLook } from '@/utils/local-storage'
import { isIntervalValid } from '@/utils/dates'
import { scrollToTop } from '@/utils/common'
import {
  MARKET_DATA_RANGE,
  GRID_LOOK, LOCAL_STORAGE_ITEM,
  PAPER_CURRENCY, PAPER_CURRENCY_ARRAY,
  CRYPTO_CURRENCY, CRYPTO_CURRENCY_ARRAY
} from '@/utils/constants'
import type { MarketsStateType } from '@/types/state/ui/pages-protected/markets'
import type { GetStateType } from '@/types'

export const REQUESTED_LENGTH = 12
const CURRENCIES_REQUESTED_LENGTH = Math.ceil(REQUESTED_LENGTH / Object.keys(PAPER_CURRENCY).length)

export const onMount = () => (dispatch: Function, getState: GetStateType) => {
  const cryptoCurrencies = CRYPTO_CURRENCY_ARRAY.slice(0, CURRENCIES_REQUESTED_LENGTH)
  const coinsIdList = cryptoCurrencies.map(({ id }: { id: string }) => id)

  if (!getState().ui.markets.grid.list.length) {
    dispatch(getCoinsMarketChart(REQUEST_NAME.UI.MARKETS.GET_LIST, MARKET_DATA_RANGE.SIX_MONTHES.name, coinsIdList))
    .then((data: Array<Object>) => {
      dispatch(setGrid({
        list: data,
        page: 0
      }))
    })
  }
}

export const getMarketCharts = ({ page }: { page: number }) => (dispatch: Function, getState: GetStateType) => {
  const coinsIdList = CRYPTO_CURRENCY_ARRAY.slice(
    page * CURRENCIES_REQUESTED_LENGTH,
    (page + 1) * CURRENCIES_REQUESTED_LENGTH
  ).map(({ id }: { id: string }) => id)

  dispatch(getCoinsMarketChart(REQUEST_NAME.UI.MARKETS.GET_LIST, MARKET_DATA_RANGE.SIX_MONTHES.name, coinsIdList))
  .then((data: Array<Object>) => {
    if (page !== getState().ui.markets.grid.page) {
      scrollToTop()
    }
    dispatch(setGrid({
      list: data,
      page
    }))
  })
}

export const getMarketDetailedChart = (_rangeName?: ?string, interval?: Object = {}) =>
  (dispatch: Function, getState: GetStateType) => {
    const { rangeName, exchangePair: { from, to } } = getState().ui.markets.detailedGraph

    if (_rangeName) {
      dispatch({
        type: types.SET_DETAILED_GRAPH_INTERVAL,
        payload: { start: null, end: null }
      })
    }

    dispatch(getCoinsMarketChart(REQUEST_NAME.UI.MARKETS.GET_DETAILED_GRAPH, _rangeName || rangeName, [from], [to], true, interval))
    .then((data: Array<Object>) => {
      dispatch({ type: types.SET_DETAILED_GRAPH_DATA, graphData: data[0] })
      if (_rangeName) {
        dispatch({ type: types.SET_DETAILED_GRAPH_RANGE_NAME, rangeName: _rangeName })
      }
    })
  }

export const toggleGraphGrid = (isShowed: boolean) => ({ type: types.TOGGLE_GRAPH_GRID, isShowed })
export const setGridLook = (gridLook: string) => ({ type: types.SET_GRID_LOOK, gridLook })
export const setGrid = (grid: Object) => ({ type: types.SET_GRID, grid })

export const toggleDetailedGraph = (isShowed: boolean, exchangePair?: Object = {}) => (dispatch: Function) => {
  dispatch({ type: types.TOGGLE_DETAILED_GRAPH, isShowed, exchangePair })
  if (isShowed) {
    dispatch(getMarketDetailedChart())
  }
}

export const setSelectedInterval = ({ start = null, end = null }: Object) =>
  (dispatch: Function, getState: GetStateType) => {
    const { selectedInterval } = getState().ui.markets.detailedGraph
    const payload = {
      start: start || selectedInterval.start,
      end: end || selectedInterval.end
    }

    dispatch({ type: types.SET_DETAILED_GRAPH_INTERVAL, payload })
    if (payload.start && payload.end) {
      if (!isIntervalValid(payload.start, payload.end)) {
        dispatch(getMarketDetailedChart(MARKET_DATA_RANGE.SIX_MONTHES.name))
      } else {
        const interval = {
          start: dayjs(payload.start).startOf('day').unix(),
          end: dayjs(payload.end).endOf('day').unix()
        }
        dispatch(getMarketDetailedChart(null, interval))
      }
    }
  }

const initialState: MarketsStateType = {
  gridLook: GRID_LOOK.BLOCKS,
  graphGridShowed: false,
  grid: {
    list: [],
    total: CRYPTO_CURRENCY_ARRAY.length * PAPER_CURRENCY_ARRAY.length,
    pages: Math.ceil((CRYPTO_CURRENCY_ARRAY.length * PAPER_CURRENCY_ARRAY.length) / REQUESTED_LENGTH),
    page: 0
  },
  detailedGraph: {
    isModalOpen: false,
    rangeName: MARKET_DATA_RANGE.SIX_MONTHES.name,
    graphData: null,
    exchangePair: {
      from: CRYPTO_CURRENCY.BTC.id,
      to: PAPER_CURRENCY.USD.id
    },
    selectedInterval: {
      start: null,
      end: null
    }
  }
}

export function reducer(state: MarketsStateType = initialState, action: Object): MarketsStateType {
  switch (action.type) {
    case AUTH.SET_USER_DATA:
      return {
        ...state,
        graphGridShowed: getGraphGridShowed(LOCAL_STORAGE_ITEM.UI_MARKETS_GRAPH_GRID_SHOWED),
        gridLook: getGridLook(LOCAL_STORAGE_ITEM.UI_MARKETS_GRID_LOOK)
      }
    case types.SET_GRID_LOOK:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_MARKETS_GRID_LOOK, action.gridLook)
      return {
        ...state,
        gridLook: action.gridLook
      }
    case types.TOGGLE_GRAPH_GRID:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_MARKETS_GRAPH_GRID_SHOWED, action.isShowed)
      return {
        ...state,
        graphGridShowed: action.isShowed
      }
    case types.TOGGLE_DETAILED_GRAPH:
      return {
        ...state,
        detailedGraph: !action.isShowed ? initialState.detailedGraph : {
          ...state.detailedGraph,
          isModalOpen: action.isShowed,
          exchangePair: action.exchangePair
        }
      }
    case types.SET_DETAILED_GRAPH_DATA:
      return {
        ...state,
        detailedGraph: {
          ...state.detailedGraph,
          graphData: action.graphData
        }
      }
    case types.SET_DETAILED_GRAPH_RANGE_NAME:
      return {
        ...state,
        detailedGraph: {
          ...state.detailedGraph,
          rangeName: action.rangeName
        }
      }
    case types.SET_DETAILED_GRAPH_INTERVAL:
      return {
        ...state,
        detailedGraph: {
          ...state.detailedGraph,
          selectedInterval: action.payload
        }
      }
    case types.SET_GRID:
      return {
        ...state,
        grid: {
          ...state.grid,
          ...action.grid
        }
      }
    default:
      return state
  }
}
