// @flow
import styles from './markets.styles.scss'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import { REQUEST_NAME } from '@/redux/global/requests'
import WrapperProtected from '../_wrapper'
import {
  onMount, setGridLook, toggleGraphGrid, getMarketCharts,
  toggleDetailedGraph, getMarketDetailedChart, setSelectedInterval
} from './markets.redux'
import MarketsHeading from './blocks/heading'
import MarketsGrid from './blocks/grid'
import DetailedGraph from './blocks/detailed-graph'
import type { MarketsStateType } from '@/types/state/ui/pages-protected/markets'
import type { StateType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  requestPending: state.global.requests[REQUEST_NAME.UI.MARKETS.GET_LIST].pending,
  requestDetailedPending: state.global.requests[REQUEST_NAME.UI.MARKETS.GET_DETAILED_GRAPH].pending,
  marketsState: state.ui.markets
})

const mapDispatchToProps = {
  getMarketDetailedChart,
  setSelectedInterval,
  toggleDetailedGraph,
  toggleGraphGrid,
  getMarketCharts,
  setGridLook,
  onMount
}

type PropsType = {|
  requestPending: boolean,
  requestDetailedPending: boolean,
  marketsState: MarketsStateType,
  getMarketDetailedChart: Function,
  setSelectedInterval: Function,
  toggleDetailedGraph: Function,
  toggleGraphGrid: Function,
  getMarketCharts: Function,
  setGridLook: Function,
  onMount: Function,
|}

function Markets({
  requestPending, requestDetailedPending, marketsState: { gridLook, grid, graphGridShowed, detailedGraph },
  onMount, setGridLook, toggleGraphGrid, getMarketCharts,
  toggleDetailedGraph, setSelectedInterval, getMarketDetailedChart
}: PropsType) {
  useEffect(onMount, [])

  return (
    <WrapperProtected title="Markets">
      <div className={styles.wrap}>
        <MarketsHeading
          graphGridShowed={graphGridShowed}
          toggleGraphGrid={toggleGraphGrid}
          setGridLook={setGridLook}
          gridLook={gridLook}
        />

        <MarketsGrid
          requestPending={requestPending}
          toggleDetailedGraph={toggleDetailedGraph}
          getMarketCharts={getMarketCharts}
          graphGridShowed={graphGridShowed}
          gridLook={gridLook}
          grid={grid}
        />

        <DetailedGraph
          pending={requestDetailedPending}
          toggleDetailedGraph={toggleDetailedGraph}
          setSelectedInterval={setSelectedInterval}
          getMarketDetailedChart={getMarketDetailedChart}
          {...detailedGraph}
        />
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Markets)
