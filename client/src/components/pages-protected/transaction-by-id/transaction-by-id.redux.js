// @flow
import { TRANSACTION_BY_ID as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { ROUTES } from '@/utils/constants'
import { apiOwnService } from '@/api'
import type { TransactionByIdStateType } from '@/types/state/ui/pages-protected/transaction-by-id'
import type { GetStateType, TransactionType } from '@/types'

export const onMount = () => (dispatch: Function, getState: GetStateType) => {
  const id = getState().router.location.pathname.replace(ROUTES.TRANSACTIONS.default + '/', '')
  dispatch(getTransactionById(id))
}

export const onUnmount = () => ({ type: types.RESET_DATA })

const getTransactionById = (id: string) => (dispatch: Function) =>
  dispatch(apiOwnService.getTransactionById({
    stateName: REQUEST_NAME.UI.TRANSACTION_BY_ID.GET,
    args: { id }
  }))
  .then(({ transaction }: { transaction: TransactionType }) => dispatch({
    type: types.SET_DATA,
    transaction
  }))

const initialState: TransactionByIdStateType = {
  data: null
}

export function reducer(state: TransactionByIdStateType = initialState, action: Object): TransactionByIdStateType {
  switch (action.type) {
    case types.SET_DATA:
      return {
        data: action.transaction
      }
    case types.RESET_DATA:
      return {
        data: null
      }
    default:
      return state
  }
}
