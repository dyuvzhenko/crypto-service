// @flow
import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import { ROUTES } from '@/utils/constants'
import { REQUEST_NAME } from '@/redux/global/requests'
import { onMount, onUnmount } from './transaction-by-id.redux'
import WrapperProtected from '../_wrapper'
import RequestWrapper from '@/generic/request-wrapper'
import ShowRawObject from '@/generic/show-raw-object'
import type { StateType, TransactionType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  pending: state.global.requests[REQUEST_NAME.UI.TRANSACTION_BY_ID.GET].pending,
  data: state.ui.transactionById.data
})

const mapDispatchToProps = {
  onUnmount,
  onMount
}

type PropsType = {|
  onMount: Function,
  onUnmount: Function,
  data: TransactionType,
  pending: boolean
|}

function TransactionById({ pending, data, onMount, onUnmount }: PropsType) {
  useEffect(() => {
    onMount()
    return onUnmount
  }, [])
  return (
    <WrapperProtected
      title="Transactions"
      titleRoute={ROUTES.TRANSACTIONS.default}
      nestedTitle={data ? data.id : 'Finding...'}
    >
      <RequestWrapper pending={pending}>
        <ShowRawObject data={data} heading="Raw data about this transaction:" />
      </RequestWrapper>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionById)
