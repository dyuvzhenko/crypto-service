// @flow
import styles from '../transactions.styles.scss'
import React, { useState, useRef } from 'react'

import { TRANSACTION_STATUS } from '@/utils/constants'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import TimerPendingIcon from '@/assets/icons/timer-pending'
import CheckIcon from '@/assets/icons/check'
import CloseIcon from '@/assets/icons/close'
import type { TransactionType } from '@/types'

const mapStatusBlockStyle = {
  [TRANSACTION_STATUS.PENDING]: `${styles.status} ${styles.pending}`,
  [TRANSACTION_STATUS.REJECTED]: `${styles.status} ${styles.rejected}`,
  [TRANSACTION_STATUS.DONE]: `${styles.status} ${styles.completed}`
}

const mapStatus = {
  [TRANSACTION_STATUS.PENDING]: 'Pending',
  [TRANSACTION_STATUS.REJECTED]: 'Rejected',
  [TRANSACTION_STATUS.DONE]: 'Completed'
}

const mapStatusIcon = {
  [TRANSACTION_STATUS.PENDING]: <TimerPendingIcon />,
  [TRANSACTION_STATUS.REJECTED]: <CloseIcon />,
  [TRANSACTION_STATUS.DONE]: <CheckIcon />
}

type PropsType = {|
  transaction: TransactionType,
  removeFromHistory: Function,
  reject: Function
|}

export default function BlockStatus({ transaction, reject, removeFromHistory }: PropsType) {
  const [opened, toggle] = useState(false)
  const ref = useRef(null)
  useOutsideEffectByRef(ref, () => toggle(false), opened)

  return (
    <div className={mapStatusBlockStyle[transaction.status]} onClick={() => toggle(!opened)} ref={ref}>
      {mapStatus[transaction.status]}
      <div className={styles.icon}>
        {mapStatusIcon[transaction.status]}
      </div>
      {opened && (
        transaction.status === TRANSACTION_STATUS.PENDING ? (
          <div
            className={styles.dropdown}
            onClick={() => reject(transaction.id)}
          >Reject
          </div>
        ) : (
          <div
            className={styles.dropdown}
            onClick={() => removeFromHistory(transaction.id)}
          >Remove from History
          </div>
        )
      )}
    </div>
  )
}
