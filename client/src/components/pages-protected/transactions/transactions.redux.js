// @flow
import { apiOwnService } from '@/api'
import { getGridLook } from '@/utils/local-storage'
import { GRID_LOOK, LOCAL_STORAGE_ITEM } from '@/utils/constants'
import { AUTH, TRANSACTIONS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { scrollToTop } from '@/utils/common'
import { isIntervalValid } from '@/utils/dates'
import type { TransactionsStateType } from '@/types/state/ui/pages-protected/transactions'
import type { TransactionType, GetStateType } from '@/types'

export const REQUESTED_LENGTH_FOR_PAGE = 30

export const onMount = () => (dispatch: Function, getState: GetStateType) => {
  const { list, isIncome } = getState().ui.transactions.grid
  if (!list.length) {
    dispatch(getTransactions({ isIncome }))
  }
}

export const setGridLook = (gridLook: string) => ({ type: types.SET_GRID_LOOK, gridLook })
export const setGrid = (grid: Object) => ({ type: types.SET_GRID, grid })
export const resetState = () => ({ type: types.RESET_STATE })

export const setSelectedInterval = ({ start = null, end = null }: Object) =>
  (dispatch: Function, getState: GetStateType) => {
    const { selectedInterval, grid } = getState().ui.transactions
    const payload = {
      start: start || selectedInterval.start,
      end: end || selectedInterval.end
    }

    if (payload.start && payload.end) {
      if (!isIntervalValid(payload.start, payload.end)) {
        dispatch({
          type: types.SET_SELECTED_INTERVAL,
          payload: { start: null, end: null }
        })
        dispatch(getTransactions({ isIncome: grid.isIncome, page: 0 }))
        return
      } else {
        dispatch(getTransactions({ ...payload, isIncome: grid.isIncome, page: 0 }))
      }
    }

    dispatch({ type: types.SET_SELECTED_INTERVAL, payload })
  }

export const getTransactions = ({
  isIncome = null,
  page = 0,
  start = null,
  end = null
}: { isIncome: ?boolean, page?: number, start?: string, end?: string }) =>
  (dispatch: Function, getState: GetStateType) => {
    const { selectedInterval } = getState().ui.transactions
    dispatch(apiOwnService.getTransactions({
      stateName: REQUEST_NAME.UI.TRANSACTIONS.GET_LIST,
      args: {
        count: REQUESTED_LENGTH_FOR_PAGE,
        start: start || selectedInterval.start,
        end: end || selectedInterval.end,
        isIncome,
        page
      }
    }))
    .then(({ list, total, page }: { list: Array<TransactionType>, total: number, page: number }) => {
      if (page !== getState().ui.transactions.grid.page) {
        scrollToTop()
      }
      dispatch(setGrid({
        list,
        isIncome,
        total,
        pages: Math.ceil(total / REQUESTED_LENGTH_FOR_PAGE),
        page
      }))
    })
  }

export const reject = (id: string) => (dispatch: Function, getState: GetStateType) => {
  dispatch(apiOwnService.rejectTransaction({
    stateName: REQUEST_NAME.UI.TRANSACTIONS.EDIT_TRANSACTION,
    args: { id }
  }))
  .then(({ transaction }: { transaction: TransactionType }) => {
    const { grid } = getState().ui.transactions
    dispatch(setGrid({
      ...grid,
      list: grid.list.map((t: TransactionType) => t.id === transaction.id ? transaction : t)
    }))
  })
}

export const removeFromHistory = (id: string) => (dispatch: Function, getState: GetStateType) => {
  dispatch(apiOwnService.removeFromHistory({
    stateName: REQUEST_NAME.UI.TRANSACTIONS.EDIT_TRANSACTION,
    args: { id }
  }))
  .then(() => {
    const { grid } = getState().ui.transactions
    dispatch(setGrid({
      ...grid,
      total: grid.total - 1,
      list: grid.list.filter((t: TransactionType) => t.id !== id)
    }))
  })
}

const initialState: TransactionsStateType = {
  selectedInterval: {
    start: null,
    end: null
  },
  gridLook: GRID_LOOK.ROWS,
  grid: {
    list: [],
    isIncome: null,
    total: 0,
    pages: 0,
    page: 0
  }
}

export function reducer(state: TransactionsStateType = initialState, action: Object): TransactionsStateType {
  switch (action.type) {
    case AUTH.SET_USER_DATA:
      return {
        ...state,
        gridLook: getGridLook(LOCAL_STORAGE_ITEM.UI_TRANSACTIONS_GRID_LOOK)
      }
    case types.RESET_STATE:
      return {
        ...initialState,
        gridLook: state.gridLook
      }
    case types.SET_SELECTED_INTERVAL:
      return {
        ...state,
        selectedInterval: action.payload
      }
    case types.SET_GRID:
      return {
        ...state,
        grid: action.grid
      }
    case types.SET_GRID_LOOK:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_TRANSACTIONS_GRID_LOOK, action.gridLook)
      return {
        ...state,
        gridLook: action.gridLook
      }
    default:
      return state
  }
}
