// @flow
import styles from './transactions.styles.scss'
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { ROUTES, GRID_LOOK, COIN_VALUE_MAX_PRECISION, TRANSACTION_STATUS } from '@/utils/constants'
import { REQUEST_NAME } from '@/redux/global/requests'
import { pretifyNumber } from '@/utils/common'
import { getCoinIcon, getCoinAbbr } from '@/utils/wallets'
import { pretifyDate } from '@/utils/dates'
import {
  onMount, resetState,
  setGridLook, getTransactions,
  setSelectedInterval, reject, removeFromHistory,
  REQUESTED_LENGTH_FOR_PAGE
} from './transactions.redux'
import WrapperProtected from '../_wrapper'
import BlockStatus from './block-status'
import Pagination from '@/generic/pagination'
import RequestWrapper from '@/generic/request-wrapper'
import CalendarDayPicker from '@/generic/calendar-day-picker'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'
import GridLookRowsIcon from '@/assets/icons/grid-look-rows'
import GridLookBlocksIcon from '@/assets/icons/grid-look-blocks'
import ArrowLeftIcon from '@/assets/icons/arrow-left'
import ArrowRightIcon from '@/assets/icons/arrow-right'
import type { TransactionsStateType } from '@/types/state/ui/pages-protected/transactions'
import type { StateType, TransactionType } from '@/types'

const mapIncomeIconStyle = (transaction: TransactionType) => {
  const colorStyle = transaction.isIncome
    ? styles.red
    : transaction.status === TRANSACTION_STATUS.PENDING
      ? styles.blue
      : styles.green

  return `${styles.icon} ${colorStyle}`
}

const mapStateToProps = (state: StateType) => ({
  pending: state.global.requests[REQUEST_NAME.UI.TRANSACTIONS.GET_LIST].pending,
  editPending: state.global.requests[REQUEST_NAME.UI.TRANSACTIONS.EDIT_TRANSACTION].pending,
  transactionsState: state.ui.transactions
})

const mapDispatchToProps = {
  setGridLook,
  getTransactions,
  removeFromHistory,
  setSelectedInterval,
  reject,
  resetState,
  onMount
}

type PropsType = {|
  pending: boolean,
  editPending: boolean,
  transactionsState: TransactionsStateType,
  resetState: Function,
  onMount: Function,
  setGridLook: Function,
  getTransactions: Function,
  removeFromHistory: Function,
  setSelectedInterval: Function,
  reject: Function
|}

function Transactions({
  resetState, onMount, setGridLook, getTransactions, setSelectedInterval, removeFromHistory, reject,
  pending, editPending, transactionsState: { selectedInterval, gridLook, grid }
}: PropsType) {
  useEffect(() => {
    onMount()
    return resetState
  }, [])

  return (
    <WrapperProtected title="Transactions">
      <RequestWrapper pending={pending || editPending} fixed={true}>
        <div className={styles.wrap}>
          <div className={styles.topPanel}>
            <div className={styles.rowOverview}>
              <span className={styles.title}>
                Overview
              </span>
              <div className={styles.icon}>
                <DotsVerticalIcon />
              </div>
            </div>

            <div className={styles.rowInterval}>
              <div className={styles.blockMonth}>
                <div className={styles.desc}>From</div>
                <CalendarDayPicker
                  positionStyle={styles.positionForLeftCalendar}
                  selectedDate={selectedInterval.start}
                  showIntervalForwardTo={selectedInterval.end}
                  handleSelect={(date: string) => setSelectedInterval({ start: date })}
                />
              </div>
              <div className={styles.blockMonth}>
                <div className={styles.desc}>To</div>
                <CalendarDayPicker
                  positionStyle={styles.positionForRightCalendar}
                  selectedDate={selectedInterval.end}
                  showIntervalBackwardTo={selectedInterval.start}
                  handleSelect={(date: string) => setSelectedInterval({ end: date })}
                />
              </div>
            </div>

            <div className={styles.rowGridLook}>
              <div
                onClick={() => setGridLook(GRID_LOOK.ROWS)}
                className={`${styles.icon} ${gridLook === GRID_LOOK.ROWS ? styles.active : ''}`}
              ><GridLookRowsIcon />
              </div>
              <div
                onClick={() => setGridLook(GRID_LOOK.BLOCKS)}
                className={`${styles.icon} ${gridLook === GRID_LOOK.BLOCKS ? styles.active : ''}`}
              ><GridLookBlocksIcon />
              </div>
            </div>
          </div>
          <div className={styles.tableHeading}>
            <div className={styles.rowTotal}>
              <div className={styles.number}>{grid.total === 0 ? 'No' : pretifyNumber(grid.total)}</div>
              Transactions
            </div>
            <div className={styles.rowControls}>
              <div
                onClick={() => getTransactions({ isIncome: null, page: 0 })}
                className={`${styles.item} ${grid.isIncome === null ? styles.active : ''}`}
              >All
              </div>
              <div
                onClick={() => getTransactions({ isIncome: false, page: 0 })}
                className={`${styles.item} ${grid.isIncome === false ? styles.active : ''}`}
              >Sent
              </div>
              <div
                onClick={() => getTransactions({ isIncome: true, page: 0 })}
                className={`${styles.item} ${grid.isIncome === true ? styles.active : ''}`}
              >Received
              </div>
            </div>
          </div>

          <div className={`${styles.grid} ${gridLook === GRID_LOOK.ROWS ? '' : styles.lookBlocks}`}>
            {pending && !grid.list.length && Array(REQUESTED_LENGTH_FOR_PAGE).fill().map((_: any, i: number) =>
              <div className={`${styles.item} ${styles.skeleton}`} key={i} />
            )}
            {grid.list.map((transaction: TransactionType, index: number) =>
              <div className={styles.item} key={index}>
                <div className={styles.gridBlock}>
                  <div className={styles.overviewIcon}>
                    <DotsVerticalIcon />
                  </div>
                  <Link to={ROUTES.TRANSACTIONS.getById(transaction.id)} className={styles.date}>
                    {pretifyDate(transaction.date)}
                  </Link>
                  <div className={styles.walletTypeIcon}>
                    {getCoinIcon(transaction.coin_id)}
                  </div>
                </div>

                <div className={styles.gridBlock}>
                  <div className={styles.token}>
                    <div className={mapIncomeIconStyle(transaction)}>
                      {transaction.isIncome ? <ArrowLeftIcon /> : <ArrowRightIcon />}
                    </div>
                    <Link to={ROUTES.TRANSACTIONS.getById(transaction.id)} className={styles.text}>
                      {transaction.isIncome ? transaction.from_token : transaction.to_token}
                    </Link>
                  </div>
                </div>

                <div className={styles.gridBlock}>
                  <div className={styles.amount}>
                    <span className={styles.number}>
                      {pretifyNumber(transaction.amount, COIN_VALUE_MAX_PRECISION)}
                    </span>
                    <span className={styles.coinName}>
                      {getCoinAbbr(transaction.coin_id)}
                    </span>
                  </div>

                  <BlockStatus
                    transaction={transaction}
                    removeFromHistory={removeFromHistory}
                    reject={reject}
                  />
                </div>
              </div>
            )}
          </div>

          <div className={styles.wrapPagination}>
            <Pagination
              onPageChange={(page: number) => getTransactions({ isIncome: grid.isIncome, page })}
              pages={grid.pages}
              page={grid.page}
            />
          </div>
        </div>
      </RequestWrapper>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Transactions)
