// @flow
import styles from './wallets.styles.scss'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import {
  onMount, resetState,
  setGridLook, toggleGraphGrid, toggleWalletForm,
  changePage, createWallet
} from './wallets.redux'
import { REQUEST_NAME } from '@/redux/global/requests'
import WrapperProtected from '../_wrapper'
import WalletsHeading from './blocks/heading'
import WalletsGrid from './blocks/grid'
import type { UiWalletsStateType } from '@/types/state/ui/pages-protected/wallets'
import type { StateType } from '@/types'

const mapStateToProps = (state: StateType) => ({
  walletsState: state.ui.wallets,
  userBalances: state.stableData.balance.data,
  isDarkTheme: state.ui.wrapperProtected.isDarkTheme,
  addWalletPending: state.global.requests[REQUEST_NAME.UI.WALLETS.ADD_NEW_WALLET].pending,
  requestPending: state.global.requests[REQUEST_NAME.STABLE_DATA.WALLETS.GET_ALL].pending
    || state.global.requests[REQUEST_NAME.UI.WALLETS.GET_LIST].pending,
  exchangePending: state.global.requests[REQUEST_NAME.UI.WALLETS.GET_USD_EXCHANGE].pending
    || state.global.requests[REQUEST_NAME.UI.WALLETS.GET_EUR_EXCHANGE].pending
})

const mapDispatchToProps = {
  toggleWalletForm,
  toggleGraphGrid,
  setGridLook,
  createWallet,
  changePage,
  onMount,
  resetState
}

type PropsType = {|
  requestPending: boolean,
  addWalletPending: boolean,
  exchangePending: boolean,
  isDarkTheme: boolean,
  userBalances: Object,
  walletsState: UiWalletsStateType,
  toggleWalletForm: Function,
  toggleGraphGrid: Function,
  createWallet: Function,
  setGridLook: Function,
  changePage: Function,
  resetState: Function,
  onMount: Function
|}

function Wallets({
  requestPending, addWalletPending, exchangePending,
  isDarkTheme, userBalances, walletsState: { gridLook, grid, graphGridShowed, walletFormShowed, data },
  onMount, resetState, setGridLook, toggleGraphGrid, toggleWalletForm, changePage, createWallet
}: PropsType) {
  useEffect(() => {
    onMount()
    return resetState
  }, [])

  return (
    <WrapperProtected title="Wallets">
      <div className={styles.wrap}>
        <WalletsHeading
          graphGridShowed={graphGridShowed}
          toggleGraphGrid={toggleGraphGrid}
          setGridLook={setGridLook}
          gridLook={gridLook}
        />

        <WalletsGrid
          isDarkTheme={isDarkTheme}
          userWallets={data}
          userBalances={userBalances}
          walletFormShowed={walletFormShowed}
          toggleWalletForm={toggleWalletForm}
          createWallet={createWallet}
          requestPending={requestPending}
          addWalletPending={addWalletPending}
          exchangePending={exchangePending}
          changePage={changePage}
          graphGridShowed={graphGridShowed}
          gridLook={gridLook}
          grid={grid}
        />
      </div>
    </WrapperProtected>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Wallets)
