// @flow
import { apiOwnService } from '@/api'
import { AUTH, UI_WALLETS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import {
  GRID_LOOK, CRYPTO_CURRENCY_ARRAY, NOTIFICATION_TYPE,
  COIN_VALUE_MAX_PRECISION, LOCAL_STORAGE_ITEM
} from '@/utils/constants'
import { getGraphGridShowed, getGridLook } from '@/utils/local-storage'
import { getCoinsValueByUSD, getCoinsValueByEUR } from '@/redux/stable-data/exchange'
import { getAllBalance } from '@/redux/stable-data/balance'
import { getAllWallets } from '@/redux/stable-data/wallets'
import { addNotification } from '@/components/shared/notifications'
import { getBalanceAbbr } from '@/utils/wallets'
import { pretifyNumber } from '@/utils/common'
import { pretifyDate } from '@/utils/dates'
import type { UiWalletsStateType } from '@/types/state/ui/pages-protected/wallets'
import type { TransactionType, WalletWithHistoryType, GetStateType } from '@/types'

export const REQUESTED_LENGTH = 9
const NESTED_TRANSACTIONS_LENGTH = 30

const getPoints = (wallet: WalletWithHistoryType) => {
  let walletValue = wallet.value
  const points = wallet.lastTransactions.map((transaction: TransactionType) => {
    const walletValueAfterTransaction = walletValue
    const effectOnWallet = transaction.isIncome ? Number(transaction.amount) : (- Number(transaction.amount))
    walletValue = Number((walletValue - effectOnWallet).toFixed(COIN_VALUE_MAX_PRECISION))

    const walletValueBeforeTransaction = walletValue
    const dateOfTransaction = pretifyDate(transaction.date)

    return {
      token: transaction.isIncome ? transaction.to_token : transaction.from_token,
      walletValueBeforeTransaction,
      walletValueAfterTransaction,
      dateOfTransaction,
      effectOnWallet
    }
  })

  return { ...wallet, points: points.reverse() }
}

export const onMount = () => (dispatch: Function, getState: GetStateType) => {
  const { requests } = getState().global
  const vsCurrencyList = CRYPTO_CURRENCY_ARRAY.map(({ id }: { id: string }) => id)
  if (requests[REQUEST_NAME.UI.WALLETS.GET_USD_EXCHANGE].pending === null) {
    dispatch(getCoinsValueByUSD(vsCurrencyList, REQUEST_NAME.UI.WALLETS.GET_USD_EXCHANGE))
  }
  if (requests[REQUEST_NAME.UI.WALLETS.GET_EUR_EXCHANGE].pending === null) {
    dispatch(getCoinsValueByEUR(vsCurrencyList, REQUEST_NAME.UI.WALLETS.GET_EUR_EXCHANGE))
  }

  dispatch(apiOwnService.getAllWalletsWithHistory({
    stateName: REQUEST_NAME.UI.WALLETS.GET_LIST,
    args: {
      transactionsMaxLength: NESTED_TRANSACTIONS_LENGTH
    }
  }))
  .then(({ list }: { list: Array<WalletWithHistoryType> }) => {
    dispatch(setData(list.map(getPoints)))
    dispatch(changePage(0, false))
    dispatch(setGrid({
      total: list.length,
      pages: Math.ceil((list.length + 1) / REQUESTED_LENGTH)
    }))
  })
}

export const changePage = (page: number, closeForm: boolean = true) => (dispatch: Function, getState: GetStateType) => {
  const { data, walletFormShowed } = getState().ui.wallets
  const list = page === 0
    ? data.slice(0, REQUESTED_LENGTH - 1)
    : data.slice(page * REQUESTED_LENGTH - 1, (page + 1) * REQUESTED_LENGTH - 1)

  dispatch(setGrid({ list, page }))
  if (closeForm && walletFormShowed) {
    dispatch(toggleWalletForm(false))
  }
}

export const createWallet = (values: Object) =>
  (dispatch: Function, getState: GetStateType) => {
    const exchange = getState().stableData.exchange[values.balanceId][values.coinId]
    const requiredMoneyOnBalance = exchange * Number(values.purchasedAmount)
    const moneyOnBalance = getState().stableData.balance.data[values.balanceId]

    if (requiredMoneyOnBalance > moneyOnBalance) {
      const msg = `There must be ${pretifyNumber(requiredMoneyOnBalance)} ${getBalanceAbbr(values.balanceId)}`
        + ' in your balance for this transaction.'
      dispatch(addNotification({ msg, type: NOTIFICATION_TYPE.ERROR }))
      return
    }

    dispatch(apiOwnService.addNewWallet({
      stateName: REQUEST_NAME.UI.WALLETS.ADD_NEW_WALLET,
      args: {
        balanceEffect: requiredMoneyOnBalance,
        purchasedAmount: values.purchasedAmount,
        balanceId: values.balanceId,
        coinId: values.coinId
      }
    }))
    .then(({ wallet }: { wallet: WalletWithHistoryType }) => {
      const { data } = getState().ui.wallets
      const list = [wallet, ...data]
      dispatch(setData(list.map(getPoints)))
      dispatch(changePage(0))
      dispatch(setGrid({
        total: list.length,
        pages: Math.ceil((list.length + 1) / REQUESTED_LENGTH)
      }))
      dispatch(getAllBalance())
      dispatch(getAllWallets())
    })
  }

export const toggleGraphGrid = (isShowed: boolean) => ({ type: types.TOGGLE_GRAPH_GRID, isShowed })
export const toggleWalletForm = (isShowed: boolean) => ({ type: types.TOGGLE_WALLET_FORM, isShowed })
export const setGridLook = (gridLook: string) => ({ type: types.SET_GRID_LOOK, gridLook })
export const setGrid = (grid: Object) => ({ type: types.SET_GRID, grid })
export const setData = (data: Object) => ({ type: types.SET_DATA, data })
export const resetState = () => ({ type: types.RESET_STATE })

const initialState: UiWalletsStateType = {
  gridLook: GRID_LOOK.BLOCKS,
  graphGridShowed: false,
  walletFormShowed: false,
  data: [],
  grid: {
    list: [],
    total: 0,
    pages: 0,
    page: 0
  }
}

export function reducer(state: UiWalletsStateType = initialState, action: Object): UiWalletsStateType {
  switch (action.type) {
    case AUTH.SET_USER_DATA:
      return {
        ...state,
        graphGridShowed: getGraphGridShowed(LOCAL_STORAGE_ITEM.UI_WALLETS_GRAPH_GRID_SHOWED),
        gridLook: getGridLook(LOCAL_STORAGE_ITEM.UI_WALLETS_GRID_LOOK)
      }
    case types.RESET_STATE:
      return {
        ...initialState,
        gridLook: state.gridLook,
        graphGridShowed: state.graphGridShowed
      }
    case types.SET_DATA:
      return {
        ...state,
        data: action.data
      }
    case types.SET_GRID_LOOK:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_WALLETS_GRID_LOOK, action.gridLook)
      return {
        ...state,
        gridLook: action.gridLook
      }
    case types.TOGGLE_GRAPH_GRID:
      localStorage.setItem(LOCAL_STORAGE_ITEM.UI_WALLETS_GRAPH_GRID_SHOWED, action.isShowed)
      return {
        ...state,
        graphGridShowed: action.isShowed
      }
    case types.TOGGLE_WALLET_FORM:
      return {
        ...state,
        walletFormShowed: action.isShowed
      }
    case types.SET_GRID:
      return {
        ...state,
        grid: {
          ...state.grid,
          ...action.grid
        }
      }
    default:
      return state
  }
}
