// @flow
import commonStyles from '@/styles/common.scss'
import styles from './add-wallet-form.styles.scss'
import React from 'react'
import { Form, Field } from 'react-final-form'

import { CRYPTO_CURRENCY_ARRAY } from '@/utils/constants'
import { pretifyNumber, isValidNumber } from '@/utils/common'
import { getCoinAbbr, getBalanceAbbr } from '@/utils/wallets'
import RequestButton from '@/generic/request-button'
import RequestWrapper from '@/generic/request-wrapper'
import Select from '@/generic/select'
import type { WalletWithHistoryType } from '@/types'

type PropsType = {|
  exchangePending: boolean,
  addWalletPending: boolean,
  toggleWalletForm: Function,
  walletFormShowed: boolean,
  createWallet: Function,
  userWallets: Array<WalletWithHistoryType>,
  userBalances: Object
|}

const getWalletValue = (userBalances: Object, paperCoinId: string) =>
  `${pretifyNumber(userBalances[paperCoinId])} ${getBalanceAbbr(paperCoinId)}`

function AddWalletForm({
  exchangePending, addWalletPending,
  toggleWalletForm, walletFormShowed, createWallet,
  userWallets, userBalances
}: PropsType) {
  if (!walletFormShowed) {
    return (
      <div className={styles.wrap} onClick={() => toggleWalletForm(true)}>
        <span className={styles.plus}>+</span>
        <span className={styles.text}>Add wallet</span>
      </div>
    )
  }

  const availableBalances = Object.keys(userBalances).map((paperCoinId: string) => ({
    text: getWalletValue(userBalances, paperCoinId),
    value: paperCoinId
  }))

  const availableCoins = CRYPTO_CURRENCY_ARRAY
    .filter(({ id }: { id: string }) => !userWallets.some(({ coin_id }: { coin_id: string }) => id === coin_id))
    .map(({ id, abbr }: { id: string, abbr: string }) => ({ text: abbr, value: id }))

  return (
    <RequestWrapper pending={exchangePending}>
      <Form
        onSubmit={createWallet}
        initialValues={{ coinId: '', balanceId: '', purchasedAmount: '' }}
        render={({ form, handleSubmit, values }: Object) => (
          <div className={styles.wrapForm}>
            <h4 className={styles.title}>New Wallet</h4>
            <div className={styles.fieldInput}>
              <label>Purchased amount</label>
              <Field name="purchasedAmount">
                {({ input }: Object) => (
                  <input
                    {...input}
                    className={commonStyles.input}
                    onChange={(e: Object) => {
                      e.preventDefault()
                      const { value } = e.target
                      if (isValidNumber(value) || value === '') {
                        input.onChange(value)
                      }
                    }}
                  />
                )}
              </Field>
            </div>
            <div className={styles.wrapSelects}>
              <div className={styles.fieldSelect}>
                <label>Select coin</label>
                <Select
                  options={availableCoins}
                  handleClick={(val: string) => form.change('coinId', val)}
                  selectedItem={!availableCoins.length
                    ? 'No available coins'
                    : values.coinId
                      ? getCoinAbbr(values.coinId)
                      : 'Empty'
                  }
                />
              </div>
              <div className={styles.fieldSelect}>
                <label>Select balance</label>
                <Select
                  options={availableBalances}
                  handleClick={(val: string) => form.change('balanceId', val)}
                  selectedItem={!availableBalances.length
                    ? 'No available balances'
                    : values.balanceId
                      ? getWalletValue(userBalances, values.balanceId)
                      : 'Empty'
                  }
                />
              </div>
            </div>
            <RequestButton
              props={{
                disabled: !(values.coinId && values.balanceId && values.purchasedAmount),
                className: `${commonStyles.button} ${styles.submitBtn}`,
                onClick: handleSubmit
              }}
              pending={addWalletPending}
              text="Save"
            />
          </div>
        )}
      />
    </RequestWrapper>
  )
}

export default AddWalletForm
