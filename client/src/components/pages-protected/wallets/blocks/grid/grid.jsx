// @flow
/*eslint no-magic-numbers: ["off"]*/
import styles from './grid.styles.scss'
import commonStyles from '@/styles/common.scss'
import React from 'react'
import Chartist from 'chartist'
import ChartistTooltip from 'chartist-plugin-tooltips-updated'

import { GRID_LOOK } from '@/utils/constants'
import { REQUESTED_LENGTH } from '../../wallets.redux'
import { getCoinIcon, getCoinName, getCoinAbbr, getCoinColor } from '@/utils/wallets'
import AddWalletForm from './add-wallet-form'
import RequestWrapper from '@/generic/request-wrapper'
import Pagination from '@/generic/pagination'
import type { WalletWithHistoryType } from '@/types'

const getMeta = (point: Object, coinId: string) =>
  point.dateOfTransaction + '\n' +
  (point.effectOnWallet > 0 ? '+ ' : ' ') +
  point.effectOnWallet + ' ' + getCoinAbbr(coinId)

const getGraphOptions = (graphGridShowed: boolean) => ({
  plugins: [
    ChartistTooltip({
      class: commonStyles.tooltipOverwrite,
      transformTooltipTextFnc: () => null
    })
  ],
  fullWidth: true,
  showPoint: true,
  lineSmooth: false,
  height: 200,
  axisX: {
    showGrid: false,
    showLabel: false
  },
  axisY: {
    showGrid: graphGridShowed,
    showLabel: graphGridShowed,
    offset: graphGridShowed ? 40 : 20
  }
})

type PropsType = {|
  exchangePending: boolean,
  addWalletPending: boolean,
  requestPending: boolean,
  graphGridShowed: boolean,
  gridLook: string,
  grid: Object,
  changePage: Function,
  walletFormShowed: boolean,
  toggleWalletForm: Function,
  userWallets: Array<WalletWithHistoryType>,
  userBalances: Object,
  createWallet: Function,
  isDarkTheme: boolean
|}

function WalletsGrid({
  exchangePending, addWalletPending, requestPending,
  gridLook, graphGridShowed, grid, changePage,
  walletFormShowed, toggleWalletForm,
  userWallets, userBalances,
  createWallet,
  isDarkTheme
}: PropsType) {
  return (
    <RequestWrapper pending={requestPending} fixed={true}>
      <div className={`${styles.grid} ${gridLook === GRID_LOOK.ROWS ? styles.lookRows : styles.lookBlocks}`}>
        {!grid.list.length || grid.page === 0 && (
          <div className={`${styles.item} ${styles.addWallet} ${walletFormShowed ? styles.opened : ''}`}>
            <AddWalletForm
              userWallets={userWallets}
              userBalances={userBalances}
              toggleWalletForm={toggleWalletForm}
              walletFormShowed={walletFormShowed}
              exchangePending={exchangePending}
              createWallet={createWallet}
              addWalletPending={addWalletPending}
            />
          </div>
        )}
        {requestPending && !grid.list.length && Array(REQUESTED_LENGTH).fill().map((_: any, i: number) =>
          <div className={`${styles.item} ${styles.skeleton}`} key={i} />
        )}
        {grid.list.map((gridElem: Object, index: number) => {
          const coinColor = getCoinColor(gridElem.coin_id)
          const lastPoint = gridElem.points[gridElem.points.length - 1]
          const graphId = `ct-chart-${index}`
          setTimeout(() => {
            new Chartist.Line(
              `#${graphId}`,
              {
                series: [
                  gridElem.points.map((p: Object) => ({
                    value: p.walletValueAfterTransaction,
                    meta: getMeta(p, gridElem.coin_id)
                  }))
                ]
              },
              getGraphOptions(graphGridShowed)
            )
          })

          return (
            <div className={styles.item} key={index}>
              <div className={styles.heading}>
                <div className={styles.walletTypeIcon} style={{ backgroundColor: isDarkTheme ? coinColor : '#FFF' }}>
                  {getCoinIcon(gridElem.coin_id)}
                </div>
                <div className={styles.walletCoinName}>
                  {getCoinName(gridElem.coin_id)}
                </div>

                <div className={styles.blockValue}>
                  <div className={styles.walletCoinId}>
                    {getCoinAbbr(gridElem.coin_id)}
                  </div>
                  <div className={styles.walletValue}>
                    {gridElem.value}
                  </div>
                </div>
              </div>

              <div className={styles.wrapGraph}>
                <div className="ct-chart" id={graphId} style={{ stroke: coinColor }} />
              </div>

              <div className={styles.wrapBlockLastTransaction}>
                <div className={styles.blockLastTransaction}>
                  <div className={styles.dateAndEffect}>
                    <span className={styles.date}>
                      {lastPoint.dateOfTransaction}
                    </span>
                    <span className={styles.effect}>
                      {lastPoint.effectOnWallet > 0 ? '+' : ''}
                      {`${lastPoint.effectOnWallet} ${getCoinAbbr(gridElem.coin_id)}`}
                    </span>
                  </div>
                  <div className={styles.token}>
                    {lastPoint.token}
                  </div>
                </div>
              </div>
            </div>
          )
        })}
      </div>

      <div className={styles.wrapPagination}>
        {grid.pages > 0 && (
          <Pagination
            onPageChange={changePage}
            pages={grid.pages}
            page={grid.page}
          />
        )}
      </div>
    </RequestWrapper>
  )
}

export default React.memo<PropsType>(WalletsGrid)
