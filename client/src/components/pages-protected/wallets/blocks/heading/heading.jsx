// @flow
import styles from './heading.styles.scss'
import React from 'react'

import { GRID_LOOK } from '@/utils/constants'
import DotsVerticalIcon from '@/assets/icons/dots-vertical'
import GridLookRowsIcon from '@/assets/icons/grid-look-rows'
import GridLookBlocksIcon from '@/assets/icons/grid-look-blocks'

type PropsType = {|
  graphGridShowed: boolean,
  toggleGraphGrid: Function,
  setGridLook: Function,
  gridLook: string
|}

export default function WalletsHeading({
  graphGridShowed, toggleGraphGrid,
  gridLook, setGridLook
}: PropsType) {
  return (
    <div className={styles.topPanel}>
      <div className={styles.rowOverview}>
        <span className={styles.title}>
          Overview
        </span>
        <div className={styles.icon}>
          <DotsVerticalIcon />
        </div>
      </div>
      <div className={styles.rowEnableGraphGrid}>
        {graphGridShowed ? (
          <div onClick={() => toggleGraphGrid(false)}>
            Hide graph grid
          </div>
        ) : (
          <div onClick={() => toggleGraphGrid(true)}>
            Show graph grid
          </div>
        )}
      </div>
      <div className={styles.rowGridLook}>
        <div
          onClick={() => setGridLook(GRID_LOOK.ROWS)}
          className={`${styles.icon} ${gridLook === GRID_LOOK.ROWS ? styles.active : ''}`}
        ><GridLookRowsIcon />
        </div>
        <div
          onClick={() => setGridLook(GRID_LOOK.BLOCKS)}
          className={`${styles.icon} ${gridLook === GRID_LOOK.BLOCKS ? styles.active : ''}`}
        ><GridLookBlocksIcon />
        </div>
      </div>
    </div>
  )
}
