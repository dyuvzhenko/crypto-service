// @flow
import { request } from './request'
import { addNotification } from '@/components/shared/notifications'
import { startRequest, endRequestSuccessfully, endRequestWithError } from '@/redux/global/requests'
import { NOTIFICATION_TYPE } from '@/utils/constants'
import type { ApiRequestArgumentsType, ApiRequestFetchArgumentsType } from '@/types'

const wrapSingle = (params: ApiRequestFetchArgumentsType) =>
  (dispatch: Function) => {
    dispatch(startRequest(params.stateName))
    return request(params)
    .then((data: Object) => {
      dispatch(endRequestSuccessfully(params.stateName))
      // TODO: Normalizer with the ability to throw a error
      return Promise.resolve(data)
    })
    .catch((errMsg: string) => {
      dispatch(endRequestWithError(params.stateName, errMsg))
      dispatch(addNotification({ type: NOTIFICATION_TYPE.ERROR, msg: errMsg }))
      return Promise.reject(errMsg)
    })
  }

const wrapBatch = ({ stateName, params }: {| stateName: string, params: Array<ApiRequestFetchArgumentsType> |}) =>
  (dispatch: Function) => {
    dispatch(startRequest(stateName))
    return Promise.all(params.map((p: ApiRequestFetchArgumentsType) => request(p)))
    .then((data: Object) => {
      dispatch(endRequestSuccessfully(stateName))
      // TODO: Normalizer with the ability to throw a error
      return Promise.resolve(data)
    })
    .catch((errMsg: string) => {
      dispatch(endRequestWithError(stateName, errMsg))
      dispatch(addNotification({ type: NOTIFICATION_TYPE.ERROR, msg: errMsg }))
      return Promise.reject(errMsg)
    })
  }

export const getCoinsValue = ({
  stateName,
  args: { target, vsCurrencyList = [] } = {}
}: ApiRequestArgumentsType) => {
  const query = [
    'sparkline=false',
    `vs_currency=${target}`,
    `ids=${vsCurrencyList.join(',')}`
  ].join('&')

  return wrapSingle({ stateName, endpoint: `coins/markets?${query}`, method: 'GET' })
}

export const getEvents = ({
  stateName,
  args: { from = '', to = '', upcomingEventsOnly = false } = {}
}: ApiRequestArgumentsType) => {
  const query = [
    `upcoming_events_only=${upcomingEventsOnly}`,
    from ? `from_date=${from}` : '',
    to ? `to_date=${to}` : ''
  ].join('&')

  return wrapSingle({ stateName, endpoint: `events?${query}`, method: 'GET' })
}

export const getMarketChart = ({
  stateName,
  args: { from, to, coinsList } = {}
}: ApiRequestArgumentsType) => {
  const query = [
    `from=${from}`,
    `to=${to}`
  ].join('&')

  const params = coinsList.map(({ coinId, vsCurrency }: { coinId: string, vsCurrency: string }) => ({
    endpoint: `coins/${coinId}/market_chart/range?${query}&vs_currency=${vsCurrency}`,
    method: 'GET'
  }))

  return wrapBatch({ stateName, params })
}
