// @flow
import { API_ENDPOINT } from '@/utils/constants'
import type { ApiRequestFetchArgumentsType } from '@/types'

const SUCCESS_STATUS = 200

export const request = ({ endpoint, method }: ApiRequestFetchArgumentsType) => {
  const params = {
    method,
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  }

  return fetch(API_ENDPOINT.COIN_GECKO + endpoint, params)
  .then((response: Response) => response.json().catch(() => null))
  .then((jsonData: ?Object) => {
    if (typeof jsonData !== 'object' || jsonData === null) {
      throw new Error('Something went wrong.')
    }

    if (jsonData.hasOwnProperty('status') && jsonData.status !== SUCCESS_STATUS) {
      throw new Error('API Error.')
    }

    return Promise.resolve(jsonData)
  })
  .catch((err: Error) => Promise.reject(err.message))
}
