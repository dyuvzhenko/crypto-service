// @flow
import { request } from './request'
import { getFileForm } from '@/utils/common'
import { addNotification } from '@/components/shared/notifications'
import { startRequest, endRequestSuccessfully, endRequestWithError } from '@/redux/global/requests'
import { DEFAULT_TRANSACTIONS_REQUESTED_LIST_LENGTH, NOTIFICATION_TYPE } from '@/utils/constants'
import type { ApiRequestArgumentsType, ApiRequestFetchArgumentsType, GetStateType } from '@/types'

const wrap = (params: ApiRequestFetchArgumentsType) =>
  (dispatch: Function, getState: GetStateType) => {
    const { token } = getState().global.auth

    dispatch(startRequest(params.stateName))
    return request({ ...params, token })
    .then((data: Object) => {
      dispatch(endRequestSuccessfully(params.stateName))
      // TODO: Normalizer with the ability to throw a error
      return Promise.resolve(data)
    })
    .catch((errMsg: string) => {
      dispatch(endRequestWithError(params.stateName, errMsg))
      if (!params.noNotification) {
        dispatch(addNotification({ type: NOTIFICATION_TYPE.ERROR, msg: errMsg }))
      }
      return Promise.reject(errMsg)
    })
  }

export const ping = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/ping',
  method: 'GET'
})

export const verify = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  noNotification: true,
  endpoint: 'api/verify',
  method: 'GET'
})

export const doLogin = ({ stateName, args = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  // TODO: `normalize: normalize.loginResponse`
  endpoint: 'api/login',
  method: 'POST',
  body: {
    email: args.email,
    password: args.password
  }
})

export const doSignUp = ({ stateName, args = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/signup',
  method: 'POST',
  body: {
    nickname: args.nickname,
    email: args.email,
    password: args.password
  }
})

export const getAllUserBalance = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/balance',
  method: 'GET'
})

export const getAllUserWallets = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/wallets',
  method: 'GET'
})

export const getAllUserMessages = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/messages',
  method: 'GET'
})

export const getAllWalletsWithHistory = ({
  stateName,
  args: { transactionsMaxLength } = {}
}: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: `api/user/all-wallets-with-history?transactionsCount=${transactionsMaxLength}`,
  method: 'GET'
})

export const addNewWallet = ({
  stateName,
  args: { balanceEffect, purchasedAmount, balanceId, coinId } = {}
}: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/add-wallet',
  method: 'POST',
  body: {
    balanceEffect,
    purchasedAmount,
    balanceId,
    coinId
  }
})

export const getTransactions = ({
  stateName = '',
  args: {
    count = DEFAULT_TRANSACTIONS_REQUESTED_LIST_LENGTH,
    isIncome = null,
    start = null,
    end = null,
    page = 0
  } = {}
}: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: `api/user/transactions?count=${count}&isIncome=${isIncome}&page=${page}&start=${start}&end=${end}`,
  method: 'GET'
})

export const getTransactionById = ({ stateName, args: { id } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: `api/user/transactions/${id}`,
  method: 'GET'
})

export const rejectTransaction = ({ stateName, args: { id } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: `api/user/transactions/${id}/reject`,
  method: 'PUT'
})

export const removeFromHistory = ({ stateName, args: { id } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: `api/user/transactions/${id}`,
  method: 'DELETE'
})

export const getMoney = ({ stateName }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/get-money',
  method: 'GET'
})

export const sendCoins = ({ stateName, args: { walletId, amount, sendTo } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/transaction/send',
  method: 'POST',
  body: {
    walletId,
    amount,
    sendTo
  }
})

export const receiveCoins = ({ stateName, args: { walletId, amount } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/transaction/receive',
  method: 'POST',
  body: {
    walletId,
    amount
  }
})

export const updateUserInfo = ({ stateName, args = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/update-myself',
  method: 'POST',
  body: args
})

export const updateUserPassword = ({ stateName, args = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/update-my-password',
  method: 'POST',
  body: args
})

export const updateUserPicture = ({ stateName, args: { file } = {} }: ApiRequestArgumentsType) => wrap({
  stateName,
  endpoint: 'api/user/update-my-picture',
  method: 'FORM',
  body: getFileForm('avatar', file)
})
