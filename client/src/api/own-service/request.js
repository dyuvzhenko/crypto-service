// @flow
import { API_ENDPOINT } from '@/utils/constants'
import type { ApiRequestFetchArgumentsType } from '@/types'

export const request = ({ endpoint, method, body = undefined, token = '' }: ApiRequestFetchArgumentsType) => {
  const params = {
    method: method === 'FORM' ? 'POST' : method,
    body: method === 'FORM' ? body : JSON.stringify(body),
    headers: new Headers(Object.assign(
      {},
      { 'Accept': 'application/json' },
      method !== 'FORM' ? { 'Content-Type': 'application/json' } : {},
      token ? { 'Authorization': `Bearer ${token}` } : {}
    ))
  }

  return fetch(API_ENDPOINT.OWN_SERVICE + endpoint, params)
  .then((response: Response) => response.json().catch(() => null))
  .then((jsonData: ?Object) => {
    if (typeof jsonData !== 'object' || jsonData === null) {
      throw new Error('Something went wrong.')
    }

    if (jsonData.hasOwnProperty('error')) {
      throw new Error(jsonData.error.message)
    }

    /* The server guarantees to return an Object with `data` or `error` property. */
    if (!jsonData.hasOwnProperty('data')) {
      throw new Error('API Error.')
    }

    return Promise.resolve(jsonData.data)
  })
  .catch((err: Error) => Promise.reject(err.message))
}
