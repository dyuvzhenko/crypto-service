// @flow
import '@/styles/index.scss'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import Routes from '@/routes'
import store from '@/redux/store'

const root = document.getElementById('main')

if (root) {
  render(
    <Provider store={store}>
      <Routes />
    </Provider>,
    root
  )
}
