// @flow
import type { MessageType } from '@/types'

export type MessagesStateType = {|
  list: Array<MessageType>
|}
