// @flow
export type ExchangeStateType = {
  [string]: {
    [string]: number
  }
}
