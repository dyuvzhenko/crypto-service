// @flow
import type { WalletType } from '@/types'

export type WalletsStateType = {|
  list: Array<WalletType>
|}
