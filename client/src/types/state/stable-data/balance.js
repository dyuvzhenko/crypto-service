// @flow
export type BalanceStateType = {|
  data: {|
    usd: ?number,
    eur: ?number
  |}
|}
