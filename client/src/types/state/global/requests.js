// @flow
import type { TryRequestType } from '@/types'

export type RequestsStateType = {
  [string]: TryRequestType
}
