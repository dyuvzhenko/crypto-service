// @flow
export type ApiStatusStateType = {|
  apiAlive: ?boolean
|}
