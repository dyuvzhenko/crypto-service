// @flow
import type { UserInfoType } from '@/types'

export type AuthStateType = {|
  isAuthorized: ?boolean,
  userInfo: ?UserInfoType,
  token: ?string
|}
