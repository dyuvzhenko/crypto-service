// @flow
import type { PublicPreviewSlideType } from '@/types'

export type PublicPreviewStateType = {|
  slide: PublicPreviewSlideType,
  imagesPreloaded: boolean
|}
