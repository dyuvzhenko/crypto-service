// @flow
import type { NotificationType } from '@/types'

export type NotificationsStateType = {|
  id: number,
  list: Array<NotificationType>
|}
