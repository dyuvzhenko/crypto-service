// @flow
import type { TransactionType } from '@/types'

export type TransactionsStateType = {|
  selectedInterval: {|
    start: ?string,
    end: ?string
  |},
  gridLook: string,
  grid: {|
    list: Array<TransactionType>,
    isIncome: ?boolean,
    total: number,
    pages: number,
    page: number
  |}
|}
