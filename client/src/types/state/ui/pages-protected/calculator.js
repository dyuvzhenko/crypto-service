// @flow
import type { PointType } from '@/types'

export type CalculatorStateType = {|
  exchangePair: {
    from: ?string,
    to: ?string
  },
  rangeName: string,
  graphList: Array<PointType>,
  selectionStart: ?number,
  amount: string,
  result: string
|}
