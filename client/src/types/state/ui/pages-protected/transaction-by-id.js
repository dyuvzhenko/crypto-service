// @flow
import type { TransactionType } from '@/types'

export type TransactionByIdStateType = {|
  data: ?TransactionType
|}
