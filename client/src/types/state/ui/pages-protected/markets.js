// @flow
import type { MarketChartType } from '@/types'

export type MarketsStateType = {|
  gridLook: string,
  graphGridShowed: boolean,
  grid: {|
    list: Array<MarketChartType>,
    total: number,
    pages: number,
    page: number
  |},
  detailedGraph: {|
    isModalOpen: boolean,
    rangeName: string,
    graphData: ?MarketChartType,
    exchangePair: {
      from: string,
      to: string
    },
    selectedInterval: {
      start: ?string,
      end: ?string
    }
  |}
|}
