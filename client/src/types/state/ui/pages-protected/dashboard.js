// @flow
import type { GridType, WalletType, MarketChartType } from '@/types'

export type DashboardStateType = {|
  selectedWalletCoinId: ?string,
  walletForMakeTransaction: ?WalletType,
  allTransactionsTotal: ?number,
  transactionsGrid: GridType,
  upcomingEventsGrid: GridType,
  marketChartsGrid: {
    vsCurrency: string,
    list: Array<MarketChartType>
  },
  mainChart: {
    cryptoCoinId: string,
    paperCoinId: string,
    rangeName: string,
    chartData: ?MarketChartType
  }
|}
