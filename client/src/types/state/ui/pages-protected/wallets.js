// @flow
import type { WalletWithHistoryType } from '@/types'

export type UiWalletsStateType = {|
  gridLook: string,
  graphGridShowed: boolean,
  walletFormShowed: boolean,
  data: Array<WalletWithHistoryType>,
  grid: {
    list: Array<WalletWithHistoryType>,
    total: number,
    pages: number,
    page: number
  }
|}
