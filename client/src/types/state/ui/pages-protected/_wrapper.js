// @flow
export type WrapperProtectedStateType = {|
  isDarkTheme: boolean,
  mobileSideMenuOpened: boolean,
  menuOpened: boolean
|}
