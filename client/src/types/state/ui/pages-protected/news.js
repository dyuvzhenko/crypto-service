// @flow
import type { NewsType } from '@/types'

export type NewsStateType = {|
  list: Array<NewsType>,
  lastReceivedInterval: {|
    from: ?string,
    to: ?string
  |},
  showedLength: number,
  grid: Array<NewsType>
|}
