// @flow
import type { SortedEventType } from '@/types'

export type EventsStateType = {|
  selectedInterval: {|
    start: ?string,
    end: ?string
  |},
  showedMonth: ?string,
  list: Array<SortedEventType>
|}
