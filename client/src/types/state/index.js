// @flow
import type { ApiStatusStateType } from './global/api-status'
import type { RequestsStateType } from './global/requests'
import type { AuthStateType } from './global/auth'

import type { BalanceStateType } from './stable-data/balance'
import type { MessagesStateType } from './stable-data/messages'
import type { WalletsStateType } from './stable-data/wallets'
import type { ExchangeStateType } from './stable-data/exchange'
import type { MarketChartsStateType } from './stable-data/market-charts'

import type { NotificationsStateType } from './ui/shared/notifications'
import type { WrapperProtectedStateType } from './ui/pages-protected/_wrapper'
import type { PublicPreviewStateType } from './ui/pages-public/public-preview'
import type { DashboardStateType } from './ui/pages-protected/dashboard'
import type { UiWalletsStateType } from './ui/pages-protected/wallets'
import type { MarketsStateType } from './ui/pages-protected/markets'
import type { EventsStateType } from './ui/pages-protected/events'
import type { NewsStateType } from './ui/pages-protected/news'
import type { CalculatorStateType } from './ui/pages-protected/calculator'
import type { TransactionsStateType } from './ui/pages-protected/transactions'
import type { SettingsStateType } from './ui/pages-protected/settings'
import type { TransactionByIdStateType } from './ui/pages-protected/transaction-by-id'

export type StateType = {|
  router: any,
  global: {|
    apiStatus: ApiStatusStateType,
    requests: RequestsStateType,
    auth: AuthStateType,
  |},
  stableData: {|
    balance: BalanceStateType,
    messages: MessagesStateType,
    wallets: WalletsStateType,
    exchange: ExchangeStateType,
    marketCharts: MarketChartsStateType
  |},
  ui: {|
    notifications: NotificationsStateType,
    publicPreview: PublicPreviewStateType,
    wrapperProtected: WrapperProtectedStateType,
    dashboard: DashboardStateType,
    wallets: UiWalletsStateType,
    markets: MarketsStateType,
    events: EventsStateType,
    news: NewsStateType,
    calculator: CalculatorStateType,
    transactions: TransactionsStateType,
    settings: SettingsStateType,
    transactionById: TransactionByIdStateType
  |}
|}
