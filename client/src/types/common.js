// @flow
import { NOTIFICATION_TYPE } from '@/utils/constants'
import type { StateType } from '@/types/state'
import type { EventType } from '@/types'

export type GetStateType = void => StateType

export type PublicPreviewSlideType = {|
  num: number,
  bkgStyle: string,
  text: string
|}

export type NotificationType = {|
  id: number,
  type: $Values<typeof NOTIFICATION_TYPE>,
  duration: number,
  msg: string
|}

export type TryRequestType = {|
  pending: ?boolean,
  error: any
|}

export type ApiRequestArgumentsType = {|
  stateName: string,
  noNotification?: boolean,
  args?: Object
|}

export type ApiRequestFetchArgumentsType = {|
  token?: ?string,
  noNotification?: boolean,
  stateName: string,
  endpoint: string,
  method: string,
  body?: Object
|}

export type GridType = {|
  list: Array<any>,
  status?: string,
  isIncome?: ?boolean,
  total?: ?number,
  count?: ?number
|}

export type SortedEventType = {|
  day: string,
  date: string,
  selected: boolean,
  data: ?EventType
|}

export type PointType = {|
  date: string,
  value: number
|}

export type MarketChartType = {|
  paperCoinId: string,
  cryptoCoinId: string,
  lastValue: string,
  lastVolume: string,
  lastGrowth: number,
  lowerBorder: number,
  higherBorder: number,
  points: Array<PointType>
|}
