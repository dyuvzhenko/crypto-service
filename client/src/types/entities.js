// @flow
import { TRANSACTION_STATUS, CRYPTO_CURRENCY } from '@/utils/constants'

export type TransactionType = {|
  id: string,
  user_id: string,
  date: string,
  isIncome: boolean,
  status: $Values<typeof TRANSACTION_STATUS>,
  coin_id: $Keys<typeof CRYPTO_CURRENCY>,
  from_token: string,
  to_token: string,
  amount: number
|}

export type WalletType = {|
  id: string,
  user_id: string,
  coin_id: $Keys<typeof CRYPTO_CURRENCY>,
  value: number
|}

export type SortedWalletType = {|
  id: string,
  user_id: string,
  coin_id: $Keys<typeof CRYPTO_CURRENCY>,
  value: number,
  usdValue: number,
  percentage: number
|}

export type WalletWithHistoryType = {|
  id: string,
  user_id: string,
  coin_id: string,
  value: number,
  lastTransactions: Array<TransactionType>
|}

export type UserInfoType = {|
  id: string,
  image: ?string,
  nickname: string,
  first_name: ?string,
  last_name: ?string,
  birthdate: ?string,
  email: string
|}

export type MessageType = {|
  id: string,
  from_user_id: string,
  to_user_id: string,
  author: string,
  title: string,
  text: string
|}

export type EventType = {|
  type: string,
  title: string,
  description: string,
  organizer: string,
  start_date: string,
  end_date: string,
  website: string,
  email: string,
  venue: string,
  address: string,
  city: string,
  country: string,
  screenshot: string
|}

export type NewsType = EventType
