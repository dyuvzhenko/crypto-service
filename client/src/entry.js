// @flow
import { loadPolyfills } from '@/utils/polyfills'

loadPolyfills(() => {
  import('./app')
})
