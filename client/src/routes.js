// @flow
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'

import { history } from '@/utils/history'
import { ROUTES } from '@/utils/constants'
import type { StateType } from '@/types'

import { testAPI } from '@/redux/global/api-status'
import GlobalLoader from '@/generic/global-loader'
import GlobalError from '@/generic/global-error'

import Login from '@/components/pages-public/login'
import SignUp from '@/components/pages-public/sign-up'
import PublicPreview from '@/components/pages-public/public-preview'

import Dashboard from '@/components/pages-protected/dashboard'
import Wallets from '@/components/pages-protected/wallets'
import Markets from '@/components/pages-protected/markets'
import Events from '@/components/pages-protected/events'
import Transactions from '@/components/pages-protected/transactions'
import TransactionById from '@/components/pages-protected/transaction-by-id'
import Calculator from '@/components/pages-protected/calculator'
import News from '@/components/pages-protected/news'
import Settings from '@/components/pages-protected/settings'

function PublicRoutes() {
  return (
    <Switch>
      <Route exact path={ROUTES.ENTRY.default} component={PublicPreview} />
      <Route exact path={ROUTES.LOGIN.default} component={Login} />
      <Route exact path={ROUTES.SIGNUP.default} component={SignUp} />
      <Redirect to={ROUTES.ENTRY.default} />
    </Switch>
  )
}

function ProtectedRoutes() {
  return (
    <Switch>
      <Route exact path={ROUTES.DASHBOARD.default} component={Dashboard} />
      <Route exact path={ROUTES.WALLETS.default} component={Wallets} />
      <Route exact path={ROUTES.MARKETS.default} component={Markets} />
      <Route exact path={ROUTES.EVENTS.default} component={Events} />
      <Route exact path={ROUTES.TRANSACTIONS.default} component={Transactions} />
      <Route exact path={ROUTES.TRANSACTIONS.getById(':id')} component={TransactionById} />
      <Route exact path={ROUTES.CALCULATOR.default} component={Calculator} />
      <Route exact path={ROUTES.NEWS.default} component={News} />
      <Route sensitive path={ROUTES.SETTINGS.default} component={Settings} />
      <Redirect to={ROUTES.DASHBOARD.default} />
    </Switch>
  )
}

const mapStateToProps = (state: StateType) => ({
  isAuthorized: state.global.auth.isAuthorized,
  apiAlive: state.global.apiStatus.apiAlive
})

const mapDispatchToProps = { testAPI }

type PropsType = {|
  isAuthorized: ?boolean,
  apiAlive: ?boolean,
  testAPI: Function
|}

function Routes({ testAPI, isAuthorized, apiAlive }: PropsType) {
  useEffect(testAPI, [])

  const appIsBroken = apiAlive === false
  const appIsLoading = apiAlive === null || isAuthorized === null
  const appIsReady = apiAlive === true && isAuthorized !== null
  return (
    <ConnectedRouter history={history}>
      {appIsBroken && <GlobalError />}
      {appIsLoading && <GlobalLoader />}
      {appIsReady && (
        isAuthorized
          ? <ProtectedRoutes />
          : <PublicRoutes />
      )}
    </ConnectedRouter>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes)
