// @flow
import dayjs from 'dayjs'
import styles from './calendar-month-picker.styles.scss'
import React, { useRef, useState } from 'react'

import { ARR_MONTHS } from '@/utils/constants'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import ChevronLeftIcon from '@/assets/icons/chevron-left'
import ChevronRightIcon from '@/assets/icons/chevron-right'

const getTitle = (year: ?number, month: ?number) =>
  year && (month || month === 0) && dayjs(`${year}-${month + 1}`).format('MMMM YYYY')

type PropsType = {|
  selectedYear: ?number,
  selectedMonth: ?number,
  handleSelect: Function
|}

export default function CalendarMonthPicker({ selectedYear, selectedMonth, handleSelect }: PropsType) {
  const [opened, toggle] = useState(false)
  const ref = useRef(null)
  useOutsideEffectByRef(ref, () => {
    setShowedDate({
      year: selectedYear || dayjs().year(),
      month: selectedMonth || dayjs().month()
    })
    toggle(false)
  }, opened)

  const [showed, setShowedDate] = useState({
    year: selectedYear || dayjs().year(),
    month: selectedMonth || dayjs().month()
  })

  return (
    <div className={styles.wrap} ref={ref}>
      <div className={styles.preview} onClick={() => toggle(!opened)}>
        {showed && getTitle(selectedYear, selectedMonth)}
      </div>
      {opened && (
        <div className={styles.selectBox}>
          <div className={styles.selectYear}>
            <div
              className={styles.chevron}
              onClick={() => setShowedDate({ year: showed.year - 1, month: showed.month })}
            >
              <ChevronLeftIcon />
            </div>
            {showed.year}
            <div
              className={styles.chevron}
              onClick={() => setShowedDate({ year: showed.year + 1, month: showed.month })}
            >
              <ChevronRightIcon />
            </div>
          </div>
          <div className={styles.selectMonth}>
            {ARR_MONTHS.map((monthName: string, num: number) =>
              <div
                key={num}
                onClick={() => {
                  handleSelect({ year: showed.year, month: num })
                  toggle(false)
                }}
                className={
                  `${styles.item} ${selectedMonth === num && selectedYear === showed.year ? styles.active : ''}`
                }
              >{monthName}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  )
}
