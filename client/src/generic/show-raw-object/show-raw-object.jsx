// @flow
import styles from './show-raw-object.styles.scss'
import React from 'react'

type PropsType = {|
  heading: string,
  data: Object
|}

function ShowRawObject({ heading = '', data }: PropsType) {
  return data && (
    <div className={`col-main ${styles.wrap}`}>
      <div className="col-12 sol-sm-4">
        <div className={styles.heading}>{heading}</div>
        {Object.entries(data).map(([key, value]: [string, any], index: number) =>
          <div className={styles.item} key={index}>
            <div className={styles.key}>{key}:</div>
            {'    '}
            <div className={styles.value}>
              {typeof value === 'string' ? value : JSON.stringify(value)}
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default ShowRawObject
