// @flow
/*eslint no-magic-numbers: ["off"]*/
import dayjs from 'dayjs'
import styles from './calendar-day-picker.styles.scss'
import React, { useRef, useState } from 'react'

import { ARR_DAYS } from '@/utils/constants'
import { DATE_FORMAT, getCalendarMonthDays, isWeekendDayNum, isDateInTheIntervalExceptEdges } from '@/utils/dates'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import ChevronLeftIcon from '@/assets/icons/chevron-left'
import ChevronRightIcon from '@/assets/icons/chevron-right'

const getPretifiedMonth = (date: string) => dayjs(date).format('MMMM YYYY')

const getTitle = (selectedDate: ?string) => selectedDate ? dayjs(selectedDate).format('MMM DD, YYYY') : '-'

const getDays = (date: string) => {
  const selectedMonth = date.slice(5, 7)
  return getCalendarMonthDays(date).map((item: Object) => ({
    ...item,
    selected: selectedMonth === item.date.slice(5, 7)
  }))
}

const getItemStyles = (
  item: Object,
  selectedDate: ?string,
  showIntervalForwardTo: ?string,
  showIntervalBackwardTo: ?string
) => {
  let style = `${styles.item} ${item.selected ? styles.active : styles.inactive} ${item.date === selectedDate ? styles.selected : ''}`

  if (showIntervalForwardTo) {
    style = style + ` ${item.date === showIntervalForwardTo ? styles.selected : ''}`
    if (selectedDate && isDateInTheIntervalExceptEdges(item.date, { start: selectedDate, end: showIntervalForwardTo })) {
      style = style + ` ${styles.inInterval}`
    }
  }

  if (showIntervalBackwardTo) {
    style = style + ` ${item.date === showIntervalBackwardTo ? styles.selected : ''}`
    if (selectedDate && isDateInTheIntervalExceptEdges(item.date, { start: showIntervalBackwardTo, end: selectedDate })) {
      style = style + ` ${styles.inInterval}`
    }
  }

  return style
}

type PropsType = {|
  positionStyle: string,
  showIntervalForwardTo?: ?string,
  showIntervalBackwardTo?: ?string,
  handleSelect: Function,
  selectedDate: ?string
|}

function CalendarDayPicker({ positionStyle, selectedDate, handleSelect, showIntervalForwardTo, showIntervalBackwardTo }: PropsType) {
  const [opened, toggle] = useState(false)
  const ref = useRef(null)
  useOutsideEffectByRef(ref, () => {
    setShowedMonth(selectedDate ? dayjs(selectedDate) : dayjs())
    toggle(false)
  }, opened)

  const [showedMonth, setShowedMonth] = useState(selectedDate ? dayjs(selectedDate) : dayjs())
  const days = getDays(showedMonth.format(DATE_FORMAT.SYSTEM))

  return (
    <div className={styles.wrap} ref={ref}>
      <div className={styles.preview} onClick={() => toggle(!opened)}>
        {getTitle(selectedDate)}
      </div>
      {opened && (
        <div className={`${styles.selectBox} ${positionStyle}`}>
          <div className={styles.selectMonth}>
            <div
              className={styles.chevron}
              onClick={() => setShowedMonth(showedMonth.subtract(1, 'month'))}
            >
              <ChevronLeftIcon />
            </div>
            {getPretifiedMonth(showedMonth)}
            <div
              className={styles.chevron}
              onClick={() => setShowedMonth(showedMonth.add(1, 'month'))}
            >
              <ChevronRightIcon />
            </div>
          </div>
          <div className={styles.selectDayGrid}>
            {ARR_DAYS.map((dayName: string, i: number) =>
              <div
                className={`${styles.item} ${isWeekendDayNum(i) ? styles.blockWeekendName : styles.blockWeekdayName}`}
                key={i}
              >{dayName.slice(0, 2)}
              </div>
            )}
            {days.map((item: Object, i: number) =>
              <div
                key={i}
                className={getItemStyles(item, selectedDate, showIntervalForwardTo, showIntervalBackwardTo)}
                onClick={() => {
                  handleSelect(item.date)
                  toggle(false)
                }}
              >{item.day}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  )
}

export default React.memo<PropsType>(CalendarDayPicker)
