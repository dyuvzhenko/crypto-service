// @flow
import React from 'react'
import SpinnerIcon from '@/assets/icons/spinner'

type PropsType = {|
  pending: boolean,
  props: Object,
  text: string
|}

export default function RequestButton({ pending, props, text }: PropsType) {
  return (
    <button {...props}>
      {pending ? <SpinnerIcon /> : text}
    </button>
  )
}
