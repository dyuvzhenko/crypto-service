// @flow
import styles from './spinner-ellipsis.styles.scss'
import React from 'react'

export default function SpinnerEllipsis() {
  return (
    <div className={styles.ellipsis}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}
