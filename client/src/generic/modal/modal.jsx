// @flow
import styles from './modal.styles.scss'
import * as React from 'react'
import ReactModal from 'react-modal'
import CloseIcon from '@/assets/icons/close'

ReactModal.setAppElement('#main')

type PropsType = {|
  isOpen: boolean,
  onClose: Function,
  title: string,
  contentStyles?: string,
  children: ?React.Node
|}

function Modal({ isOpen, onClose, title = '', contentStyles = '', showHeading = true, children = null }: PropsType) {
  return (
    <ReactModal
      isOpen={isOpen}
      contentLabel={title}
      className={`${styles.content} ${contentStyles}`}
      overlayClassName={styles.overlay}
      shouldCloseOnOverlayClick={true}
      onRequestClose={onClose}
    >
      {showHeading && (
        <div className={styles.heading}>
          <div className={styles.title}>{title}</div>
          <div className={styles.closeIcon} onClick={onClose}>
            <CloseIcon />
          </div>
        </div>
      )}
      {children}
    </ReactModal>
  )
}
export default Modal
