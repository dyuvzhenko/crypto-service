// @flow
import styles from './select.styles.scss'
import React, { useState, useRef } from 'react'
import { useOutsideEffectByRef } from '@/utils/react-effects'
import ChevronUpAndDownIcon from '@/assets/icons/chevron-up-and-down'

type PropsType = {|
  setSelectedItem?: Function,
  selectedItem: ?string,
  options: Array<any>,
  handleClick: Function,
  parentStyles?: Object
|}

export default function Select({
  setSelectedItem, selectedItem,
  options, handleClick,
  parentStyles = {}
}: PropsType) {
  if (setSelectedItem && !selectedItem && options.length) {
    setSelectedItem(options[0].value)
  }

  const [opened, toggle] = useState(false)
  const ref = useRef(null)
  useOutsideEffectByRef(ref, () => toggle(false), opened)

  return (
    <div className={styles.wrap} ref={ref}>
      <div className={`${styles.optionsBox} ${opened ? styles.opened : ''}`} onClick={() => toggle(!opened)}>
        <span className={parentStyles.selectedItem || styles.selectedItem}>
          {selectedItem}
        </span>
        <div className={styles.chevronIcon}>
          <ChevronUpAndDownIcon />
        </div>
      </div>
      {opened && (
        <ul className={styles.optionsList}>
          {options.map(({ text, value }: Object, i: number) =>
            <li
              key={i}
              className={parentStyles.item || styles.item}
              onClick={() => {
                toggle(false)
                handleClick(value)
              }}
            >{text}
            </li>
          )}
        </ul>
      )}
    </div>
  )
}
