// @flow
import styles from './request-wrapper.styles.scss'
import * as React from 'react'
import SpinnerEllipsis from '@/generic/spinner-ellipsis'

type PropsType = {|
  children: React.Node,
  showSpinner?: boolean,
  fixed?: boolean,
  pending: boolean
|}

export default function RequestWrapper({ pending, fixed = false, showSpinner = true, children }: PropsType) {
  const style = fixed ? { position: 'fixed' } : {}
  return (
    <div className={styles.wrap}>
      {showSpinner && pending && (
        <div className={styles.wrapSpinner} style={style}>
          <SpinnerEllipsis />
        </div>
      )}
      <div className={pending ? styles.pendingWrapper : styles.fill}>
        {children}
      </div>
    </div>
  )
}
