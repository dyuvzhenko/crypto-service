// @flow
import styles from './global-error.styles.scss'
import React from 'react'

function GlobalError() {
  return (
    <div className={styles.wrap}>
      <div className={styles.header}>
        The application is not currently available.
      </div>
    </div>
  )
}
export default GlobalError
