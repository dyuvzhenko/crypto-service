// @flow
import styles from './pagination.styles.scss'
import React from 'react'

const space = 2

const getPagesPanel = (pages: number, page: number) => {
  if (pages <= 1) {
    return []
  }
  const arr = [{ num: 0, type: 'button' }]
  if (page > space) {
    arr.push({ type: 'delimiter' })
  }
  for (let num = Math.max(1, page - space); num < Math.min(pages - 1, page + space + 1); num++) {
    arr.push({ num, type: 'button' })
  }
  if (page < pages - space - 1) {
    arr.push({ type: 'delimiter' })
  }
  if (pages > 1) {
    arr.push({ num: pages - 1, type: 'button' })
  }
  return arr.map((p: Object) => ({ ...p, active: p.num === page }))
}

type PropsType = {|
  onPageChange: Function,
  pages: number,
  page: number
|}

export default function Pagination({ pages, page, onPageChange }: PropsType) {
  return (
    <div className={styles.wrap}>
      <ul className={styles.list}>
        {getPagesPanel(pages, page).map((item: Object, index: number) =>
          <li key={index}>
            {item.type === 'button' ? (
              <button
                className={`${styles.button} ${item.active ? styles.active : ''}`}
                onClick={() => onPageChange(item.num)}
              >{item.num + 1}
              </button>
            ) : (
              <span>{'...'}</span>
            )}
          </li>
        )}
      </ul>
    </div>
  )
}
