// @flow
import styles from './global-loader.styles.scss'
import React from 'react'
import SpinnerEllipsis from '@/generic/spinner-ellipsis'

function GlobalLoader() {
  return (
    <div className={styles.wrap}>
      <div className={styles.header}>
        Cryptonix
      </div>
      <div className={styles.wrapLoader}>
        <SpinnerEllipsis />
      </div>
    </div>
  )
}
export default GlobalLoader
