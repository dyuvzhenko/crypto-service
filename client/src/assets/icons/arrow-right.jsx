// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function ArrowRightIcon() {
  return (
    <svg width="12" height="8" viewBox="0 0 12 8">
      <g>
        <g>
          <path d="M0 3.333h9.447L7.06.94 8 0l4 4-4 4-.94-.94 2.387-2.393H0z" />
        </g>
      </g>
    </svg>
  )
}
