// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function GridLookBlocksIcon() {
  return (
    <svg width="17" height="13" viewBox="0 0 17 13">
      <g>
        <g>
          <path d="M12 0h5v6h-5zM6 6V0h5v6zm6 7V7h5v6zm-6 0V7h5v6zm-6 0V7h5v6zm0-7V0h5v6z" />
        </g>
      </g>
    </svg>
  )
}
