// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function DefaultUserPictureIcon() {
  return (
    <svg width="16" height="16" viewBox="0 0 16 16">
      <g>
        <g>
          <path d="M8 10c2.67 0 8 1.34 8 4v2H0v-2c0-2.66 5.33-4 8-4zm0-2C5.79 8 4 6.21 4 4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4z" />
        </g>
      </g>
    </svg>
  )
}
