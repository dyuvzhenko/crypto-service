// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function CheckIcon() {
  return (
    <svg width="18" height="14" viewBox="0 0 18 14">
      <g>
        <g>
          <path d="M5.6 10.85l-4.2-4.2L0 8.05l5.6 5.6 12-12-1.4-1.4-10.6 10.6z" />
        </g>
      </g>
    </svg>
  )
}
