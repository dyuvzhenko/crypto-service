// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function ChevronUpAndDownIcon() {
  return (
    <svg width="8" height="15" viewBox="0 0 8 15">
      <g>
        <g>
          <path d="M1.124 9.5L.008 10.616l3.626 3.634 3.634-3.634L6.143 9.5l-2.51 2.51zm5.02-4.75L7.26 3.634 3.634 0 0 3.634 1.124 4.75l2.51-2.51z" />
        </g>
      </g>
    </svg>
  )
}
