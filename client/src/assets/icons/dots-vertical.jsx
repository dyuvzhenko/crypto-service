// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function DotsVerticalIcon() {
  return (
    <svg width="4" height="16" viewBox="0 0 4 16">
      <g>
        <g>
          <path d="M2 12c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm0-6c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm0-2C.9 4 0 3.1 0 2s.9-2 2-2 2 .9 2 2-.9 2-2 2z" />
        </g>
      </g>
    </svg>
  )
}
