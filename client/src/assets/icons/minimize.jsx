// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function MinimizeIcon() {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14">
      <g>
        <g>
          <path d="M9 0h5v5h-2V2H9zm3 12V9h2v5H9v-2zM0 5V0h5v2H2v3zm2 4v3h3v2H0V9z" />
        </g>
      </g>
    </svg>
  )
}
