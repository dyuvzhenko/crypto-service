// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function ExclamationIcon() {
  return (
    <svg width="14" height="15" viewBox="0 0 14 15">
      <g>
        <g>
          <path d="M6.3 8.2V4h1.4v4.2zm0 2.8V9.6h1.4V11zM0 7.5c0 3.864 3.136 7 7 7s7-3.136 7-7-3.136-7-7-7-7 3.136-7 7z" />
        </g>
      </g>
    </svg>
  )
}
