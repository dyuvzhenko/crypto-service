// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function LongArrowRightIcon() {
  return (
    <svg width="77" height="10" viewBox="0 0 77 10">
      <defs>
        <clipPath id="idw6a">
          <path fill="#fff" d="M39.493 43.24h-1.5v-73.629l-2.432 2.432-1.061-1.06 3.182-3.182 1.06-1.061 1.061 1.06 3.182 3.183-1.06 1.06-2.432-2.432z" />
        </clipPath>
      </defs>
      <g>
        <g transform="rotate(90 38.5 5.5)">
          <path fill="#aa9191" d="M39.493 43.24h-1.5v-73.629l-2.432 2.432-1.061-1.06 3.182-3.182 1.06-1.061 1.061 1.06 3.182 3.183-1.06 1.06-2.432-2.432z" />
          <path fill="none" stroke="#cac9ca" strokeMiterlimit="50" strokeWidth="3" d="M39.493 43.24h-1.5v-73.629l-2.432 2.432-1.061-1.06 3.182-3.182h0l1.06-1.061h0l1.061 1.06h0l3.182 3.183-1.06 1.06-2.432-2.432z" clipPath="url(&quot;#idw6a&quot;)" />
        </g>
      </g>
    </svg>
  )
}
