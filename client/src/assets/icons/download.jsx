// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function DownloadIcon() {
  return (
    <svg width="14" height="18" viewBox="0 0 14 18">
      <g>
        <g>
          <path d="M0 17.3h14v-2H0zm4-4h6v-6h4l-7-7-7 7h4z" />
        </g>
      </g>
    </svg>
  )
}
