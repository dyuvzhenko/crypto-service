// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function PlusIcon() {
  return (
    <svg width="14" height="14" viewBox="0 0 14 14">
      <g>
        <g>
          <path d="M14 8H8v6H6V8H0V6h6V0h2v6h6v2z" />
        </g>
      </g>
    </svg>
  )
}
