// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function MenuIcon() {
  return (
    <svg width="18" height="12" viewBox="0 0 18 12">
      <g>
        <g>
          <path d="M0 0h18v2H0zm0 7V5h18v2zm0 5v-2h18v2z" />
        </g>
      </g>
    </svg>
  )
}
