// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function MarketsIcon() {
  return (
    <svg width="16" height="16" viewBox="0 0 16 16">
      <g>
        <g>
          <path d="M.86.98c0-.574.466-1.04 1.04-1.04h13.12c.574 0 1.04.466 1.04 1.04V14.1a1.04 1.04 0 0 1-1.04 1.04H1.9A1.04 1.04 0 0 1 .86 14.1zm2.473 11.444h1.92V3.642h-1.92zm4.167-.01h1.92V6.24H7.5zm4.167.01h1.92V8.84h-1.92z" />
        </g>
      </g>
    </svg>
  )
}
