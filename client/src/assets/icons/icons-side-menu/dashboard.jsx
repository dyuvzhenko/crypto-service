// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function DashboardIcon() {
  return (
    <svg width="16" height="16" viewBox="0 0 16 16">
      <g>
        <g>
          <path d="M.86 1.11C.86.536 1.326.07 1.9.07h5.41v8.647H.86zM9.61.07h5.41c.574 0 1.04.466 1.04 1.04v4.16H9.61zM.86 10.58h6.45v5.2H1.9a1.04 1.04 0 0 1-1.04-1.04zm8.75-3.446h6.45v7.606a1.04 1.04 0 0 1-1.04 1.04H9.61z" />
        </g>
      </g>
    </svg>
  )
}
