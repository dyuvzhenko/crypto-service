// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function WalletsIcon() {
  return (
    <svg width="16" height="17" viewBox="0 0 16 17">
      <g>
        <g>
          <path d="M.86 1.6c0-.574.466-1.04 1.04-1.04h12.721c.574 0 1.04.466 1.04 1.04v3.341c.243.19.399.486.399.819v5.2c0 .333-.156.629-.399.819v3.341a1.04 1.04 0 0 1-1.04 1.04H1.9a1.04 1.04 0 0 1-1.04-1.04zm5.076 10.92c0 .574.465 1.04 1.04 1.04h8.675v-1.773a1.035 1.035 0 0 1-.631.213H8.937a1.04 1.04 0 0 1-1.04-1.04v-5.2c0-.574.466-1.04 1.04-1.04h6.083c.237 0 .456.08.631.213V3.16H6.976a1.04 1.04 0 0 0-1.04 1.04zm4.008-3.332c0 .287.233.52.52.52h1.467a.52.52 0 0 0 .52-.52V7.54a.52.52 0 0 0-.52-.52h-1.467a.52.52 0 0 0-.52.52z" />
        </g>
      </g>
    </svg>
  )
}
