// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function MessagesIcon() {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20">
      <g>
        <g>
          <path d="M15 10c0 .55-.45 1-1 1H4l-4 4V1c0-.55.45-1 1-1h13c.55 0 1 .45 1 1zm4-6c.55 0 1 .45 1 1v15l-4-4H5c-.55 0-1-.45-1-1v-2h13V4z" />
        </g>
      </g>
    </svg>
  )
}
