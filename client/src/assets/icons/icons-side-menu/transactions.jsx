// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function TransactionsIcon() {
  return (
    <svg width="15" height="16" viewBox="0 0 15 16">
      <g>
        <g>
          <path d="M.26 8.853a7.202 7.202 0 0 1 5.459-6.985L5.9.578A.312.312 0 0 1 6.21.31h2.518c.156 0 .287.115.309.27l.178 1.29a7.202 7.202 0 0 1 5.45 6.983 7.2 7.2 0 0 1-7.202 7.198A7.2 7.2 0 0 1 .26 8.853zm6.239.984h1.923v.003l-1.917.003 5.149 1.874.66-1.812-3.887-1.412V4.357H6.5zm1.923 0l.001-1.345.004.001v1.344z" />
        </g>
      </g>
    </svg>
  )
}
