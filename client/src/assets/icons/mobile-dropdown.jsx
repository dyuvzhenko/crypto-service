// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function MobileDropdownIcon() {
  return (
    <svg width="37" height="15" viewBox="0 0 37 15">
      <g>
        <g transform="matrix(-1 0 0 1 37 0)">
          <g>
            <path d="M0 0h24.6v1.5H0z" />
          </g>
          <g>
            <path d="M0 13h8.6v1.5H0z" />
          </g>
          <g>
            <path d="M0 7h36.6v1.5H0z" />
          </g>
        </g>
      </g>
    </svg>
  )
}
