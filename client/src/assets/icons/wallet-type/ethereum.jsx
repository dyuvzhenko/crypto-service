// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function EthereumIcon() {
  return (
    <svg width="15" height="25" viewBox="0 0 15 25">
      <g>
        <g>
          <path d="M7.47.47l7.152 10.943-7.142-3.09-7.368 3.013zM.134 14.68l7.362 4.134 7.51-4.135L7.564 24.72zm7.38-4.884l7.459 3.222-7.46 4.048L-.16 12.93z" />
        </g>
      </g>
    </svg>
  )
}
