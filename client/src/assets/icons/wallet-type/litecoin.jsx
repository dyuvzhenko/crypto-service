// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function LitecoinIcon() {
  return (
    <svg width="16" height="19" viewBox="0 0 16 19">
      <g>
        <g>
          <path d="M1.4 13.015l-1.56.606.752-3.017 1.58-.635L4.448.75h5.61L8.396 7.527l1.543-.625-.744 3.008-1.56.625-.928 3.81h8.445l-.954 3.905H.116z" />
        </g>
      </g>
    </svg>
  )
}
