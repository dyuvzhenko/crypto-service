// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function DashIcon() {
  return (
    <svg width="22" height="13" viewBox="0 0 22 13">
      <g>
        <g>
          <path d="M19.162 9.562h-.01s-1.257 3.18-4.245 3.18H.495l1.028-3.18H14.59l2.058-6.381H21.2zM4.61 0h14.342C22.095 0 21.2 3.181 21.2 3.181H3.6zm-3.6 4.895h7.733l-.99 2.953H0z" />
        </g>
      </g>
    </svg>
  )
}
