// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function TimerPendingIcon() {
  return (
    <svg width="21" height="18" viewBox="0 0 21 18">
      <g>
        <g>
          <path d="M11 10l4.28 2.54.72-1.21-3.5-2.08V5H11zM3 9H0l3.89 3.89.07.14L8 9H5c0-3.87 3.13-7 7-7s7 3.13 7 7-3.13 7-7 7c-1.93 0-3.68-.79-4.94-2.06l-1.42 1.42A8.954 8.954 0 0 0 12 18a9 9 0 1 0-9-9z" />
        </g>
      </g>
    </svg>
  )
}
