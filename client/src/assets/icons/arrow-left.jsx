// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function ArrowLeftIcon() {
  return (
    <svg width="12" height="8" viewBox="0 0 12 8">
      <g>
        <g>
          <path d="M12 3.333H2.553L4.94.94 4 0 0 4l4 4 .94-.94-2.387-2.393H12z" />
        </g>
      </g>
    </svg>
  )
}
