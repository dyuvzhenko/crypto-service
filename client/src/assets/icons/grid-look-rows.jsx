// @flow
/*eslint max-len: ["off"]*/
import React from 'react'

export default function GridLookRowsIcon() {
  return (
    <svg width="17" height="13" viewBox="0 0 17 13">
      <g>
        <g>
          <path d="M0 0h17v6H0zm0 13V7h17v6z" />
        </g>
      </g>
    </svg>
  )
}
