// @flow
import React from 'react'

export default function ChevronRightIcon() {
  return (
    <svg width="6" height="10" viewBox="0 0 6 10">
      <g>
        <g>
          <path d="M.4 8.462L3.744 5.11.4 1.759 1.43.73l4.38 4.38-4.38 4.381z" />
        </g>
      </g>
    </svg>
  )
}
