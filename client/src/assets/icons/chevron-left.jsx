// @flow
import React from 'react'

export default function ChevronLeftIcon() {
  return (
    <svg width="6" height="10" viewBox="0 0 6 10">
      <g>
        <g>
          <path d="M5.41 1.76L2.066 5.11 5.41 8.463l-1.03 1.03L0 5.11 4.38.73z" />
        </g>
      </g>
    </svg>
  )
}
