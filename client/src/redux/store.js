// @flow
import thunk from 'redux-thunk'
import { compose, createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { routerMiddleware, connectRouter } from 'connected-react-router'

import { history } from '@/utils/history'
import { AUTH } from '@/redux/action-types'
import type { StateType } from '@/types'

import { reducer as apiStatus } from '@/redux/global/api-status'
import { reducer as requests } from '@/redux/global/requests'
import { reducer as auth } from '@/redux/global/auth'

import { reducer as messages } from '@/redux/stable-data/messages'
import { reducer as balance } from '@/redux/stable-data/balance'
import { reducer as wallets } from '@/redux/stable-data/wallets'
import { reducer as exchange } from '@/redux/stable-data/exchange'
import { reducer as marketCharts } from '@/redux/stable-data/market-charts'

import { reducer as notifications } from '@/components/shared/notifications'
import { reducer as publicPreview } from '@/components/pages-public/public-preview'
import { reducer as wrapperProtected } from '@/components/pages-protected/_wrapper'
import { reducer as dashboard } from '@/components/pages-protected/dashboard'
import { reducer as uiWallets } from '@/components/pages-protected/wallets'
import { reducer as markets } from '@/components/pages-protected/markets'
import { reducer as events } from '@/components/pages-protected/events'
import { reducer as transactions } from '@/components/pages-protected/transactions'
import { reducer as calculator } from '@/components/pages-protected/calculator'
import { reducer as news } from '@/components/pages-protected/news'
import { reducer as settings } from '@/components/pages-protected/settings'
import { reducer as transactionById } from '@/components/pages-protected/transaction-by-id'

const appReducer = (history: Object) => combineReducers({
  router: connectRouter(history),
  global: combineReducers({
    requests,
    apiStatus,
    auth
  }),
  stableData: combineReducers({
    messages,
    balance,
    wallets,
    exchange,
    marketCharts
  }),
  ui: combineReducers({
    wrapperProtected,
    notifications,
    publicPreview,
    dashboard,
    wallets: uiWallets,
    markets,
    events,
    transactions,
    calculator,
    news,
    settings,
    transactionById
  })
})

const createRootReducer = (history: Object) => (state: StateType, action: Object = {}) =>
  appReducer(history)(action.type === AUTH.LOG_OUT ? undefined : state, action)

const middlewares = [thunk, routerMiddleware(history)]

const store = compose(
  composeWithDevTools(applyMiddleware(...middlewares))
)(createStore)(createRootReducer(history))

export default store
