// @flow
import { getToken, removeToken, setThemeDark } from '@/utils/local-storage'
import { detectUserTheme } from '@/components/pages-protected/_wrapper'
import { getAllBalance } from '@/redux/stable-data/balance'
import { getAllMessages } from '@/redux/stable-data/messages'
import { getAllWallets } from '@/redux/stable-data/wallets'
import { appendBodyThemeClass } from '@/utils/common'
import { verifyToken, setTokenToState, tryAuthorization, cleanUserAuthData } from '@/redux/global/auth'
import type { GetStateType } from '@/types'

const VERIFY_TOKEN_ERRORS = ['Invalid token.', 'User doesn\'t exist.']

export const actionOnLogin = () => (dispatch: Function) => {
  dispatch(detectUserTheme())
  dispatch(getAllBalance())
  dispatch(getAllWallets())
  dispatch(getAllMessages())
}

export const actionOnAppStart = () => (dispatch: Function, getState: GetStateType) => {
  const token = getToken()
  if (!token) {
    dispatch(cleanAllUserData())
    return
  }

  dispatch(setTokenToState(token))
  dispatch(verifyToken())
  .then((data: Object) => {
    if (!data.valid) {
      throw VERIFY_TOKEN_ERRORS[0]
    }

    const { breakTokenAfterRelease } = data
    dispatch(tryAuthorization({ token, breakTokenAfterRelease }))
    if (!getState().global.auth.isAuthorized) {
      throw VERIFY_TOKEN_ERRORS[0]
    }

    dispatch(actionOnLogin())
  })
  .catch((err: string) => {
    if (VERIFY_TOKEN_ERRORS.some((msg: string) => msg === err)) {
      dispatch(cleanAllUserData())
    }
  })
}

export const cleanAllUserData = () => (dispatch: Function) => {
  setThemeDark(String(false))
  appendBodyThemeClass(false)
  dispatch(cleanUserAuthData())
  removeToken()
}
