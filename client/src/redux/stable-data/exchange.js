// @flow
import { apiCoinGecko } from '@/api'
import { EXCHANGE as types } from '@/redux/action-types'
import { PAPER_CURRENCY } from '@/utils/constants'
import type { ExchangeStateType } from '@/types/state/stable-data/exchange'

export const getCoinsValueByUSD = (vsCurrencyList: Array<string>, stateName: string) =>
  (dispatch: Function) => dispatch(apiCoinGecko.getCoinsValue({
    stateName,
    args: {
      target: PAPER_CURRENCY.USD.id,
      vsCurrencyList
    }
  }))
  .then((data: Array<Object>) => dispatch({
    type: types.SET_COINS_VALUE_BY_USD,
    data
  }))

export const getCoinsValueByEUR = (vsCurrencyList: Array<string>, stateName: string) =>
  (dispatch: Function) => dispatch(apiCoinGecko.getCoinsValue({
    stateName,
    args: {
      target: PAPER_CURRENCY.EUR.id,
      vsCurrencyList
    }
  }))
  .then((data: Array<Object>) => dispatch({
    type: types.SET_COINS_VALUE_BY_EUR,
    data
  }))

const initialState: ExchangeStateType = Object.values(PAPER_CURRENCY)
  .map(({ id }: Object) => id)
  .reduce((acc: Object, id: string) => {
    acc[id] = {}
    return acc
  }, {})
/* State will be looks like:
const initialState = {
  usd: {
    bitcoin: 12.12,
    dash: 34.34,
    ...
  },
  eur: {
    bitcoin: 10.12,
    dash: 30.34,
    ...
  }
}
*/

export function reducer(state: ExchangeStateType = initialState, action: Object): ExchangeStateType {
  switch (action.type) {
    case types.SET_COINS_VALUE_BY_USD:
      return {
        ...state,
        [PAPER_CURRENCY.USD.id]: action.data.reduce((acc: Object, curr: Object) => {
          acc[curr.id] = curr.current_price
          return acc
        }, {})
      }
    case types.SET_COINS_VALUE_BY_EUR:
      return {
        ...state,
        [PAPER_CURRENCY.EUR.id]: action.data.reduce((acc: Object, curr: Object) => {
          acc[curr.id] = curr.current_price
          return acc
        }, {})
      }
    default:
      return state
  }
}
