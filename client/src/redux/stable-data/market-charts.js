// @flow
/*eslint no-magic-numbers: ["off"]*/
import dayjs from 'dayjs'
import { apiCoinGecko } from '@/api'
import { pretifyNumber } from '@/utils/common'
import { MARKET_CHARTS as types } from '@/redux/action-types'
import { DATE_FORMAT } from '@/utils/dates'
import {
  PAPER_CURRENCY, MARKET_DATA_RANGE, COIN_VALUE_MAX_PRECISION,
  CRYPTO_CURRENCY_ARRAY, PAPER_CURRENCY_ARRAY, MARKET_DATA_RANGE_ARRAY
} from '@/utils/constants'
import type { MarketChartsStateType } from '@/types/state/stable-data/market-charts'

const pretifyPercentage = (num: number) => Math.ceil(num * 100) / 100

const getData = (data: Object, coinId: string, vsCurrency: string, rangeName: string, withoutReduceData: boolean) => {
  const { prices } = data
  let lowerBorder = prices[0][1]
  let higherBorder = prices[0][1]

  const points = withoutReduceData
    ? prices
      .map(([timestamp, value]: Array<number>) => ({
        date: timestamp,
        value: Number(value.toFixed(COIN_VALUE_MAX_PRECISION))
      }))
    : prices
      .filter(([timestamp]: Array<number>, index: number, arr: Array<Array<number>>) =>
        index === 0 || !dayjs(timestamp).isSame(dayjs(arr[index - 1][0]), 'day')
      )
      .map(([timestamp, value]: Array<number>) => ({
        date: dayjs(timestamp).format(DATE_FORMAT.SYSTEM),
        value: Number(value.toFixed(COIN_VALUE_MAX_PRECISION))
      }))

  prices.forEach((elem: Array<number>) => {
    if (elem[1] > higherBorder) {
      higherBorder = elem[1]
    }
    if (elem[1] < lowerBorder) {
      lowerBorder = elem[1]
    }
  })

  const lastPoint = points[points.length - 1].value
  const penultimatePoint = points[points.length - 2].value
  const lastGrowth = pretifyPercentage(((lastPoint - penultimatePoint) / penultimatePoint) * 100)

  const lastVolume = pretifyNumber(data.total_volumes[data.total_volumes.length - 1][1])
  const lastValue = pretifyNumber(points[points.length - 1].value, COIN_VALUE_MAX_PRECISION)

  return {
    paperCoinId: vsCurrency,
    cryptoCoinId: coinId,
    lastValue,
    lastVolume,
    lastGrowth,
    lowerBorder,
    higherBorder,
    rangeName,
    points
  }
}

export const getCoinsMarketChart = (
  stateName: string,
  rangeName: string,
  coinsIdList: Array<string>,
  paperCoinsIdList: Array<string> = [],
  withoutReduceData?: boolean = false,
  interval?: Object = {}
) =>
  (dispatch: Function) => {
    const { start, end } = (interval.start && interval.end ? interval : null) || MARKET_DATA_RANGE[rangeName].getInterval()
    const paperCoins = paperCoinsIdList.length ? paperCoinsIdList.map((id: string) => ({ id })) : PAPER_CURRENCY_ARRAY
    const coinsList = coinsIdList.map((coinId: string) =>
      paperCoins.map(({ id: vsCurrency }: { id: string }) => ({
        coinId, vsCurrency
      }))
    ).reduce((acc: Object, curr: Object) => {
      acc = [...acc, ...curr]
      return acc
    }, [])

    return dispatch(apiCoinGecko.getMarketChart({
      stateName,
      args: {
        coinsList,
        from: start,
        to: end
      }
    }))
    .then((data: Object) => {
      const list = coinsList.map(({ coinId, vsCurrency }: { coinId: string, vsCurrency: string }, index: number) =>
        getData(data[index], coinId, vsCurrency, rangeName, withoutReduceData)
      )
      if (!withoutReduceData) {
        list.forEach((payload: Object) => dispatch({ type: types.UPDATE_RANGE_FOR_COIN_ID, payload }))
      }

      return Promise.resolve(list)
    })
  }

const getRanges = ({ paperCoinId, cryptoCoinId }: { paperCoinId: string, cryptoCoinId: string }) =>
  MARKET_DATA_RANGE_ARRAY.reduce((acc: Object, { name }: { name: string }) => {
    acc[name] = {
      paperCoinId,
      cryptoCoinId,
      lastValue: '0',
      lastVolume: '0',
      lastGrowth: 0,
      lowerBorder: 0,
      higherBorder: 0,
      rangeName: name,
      points: []
    }
    return acc
  }, {})

const initialState: MarketChartsStateType = CRYPTO_CURRENCY_ARRAY
  .map(({ id }: Object) => id)
  .reduce((acc: Object, id: string) => {
    acc[id] = {
      [PAPER_CURRENCY.USD.id]: getRanges({
        paperCoinId: PAPER_CURRENCY.USD.id,
        cryptoCoinId: id
      }),
      [PAPER_CURRENCY.EUR.id]: getRanges({
        paperCoinId: PAPER_CURRENCY.EUR.id,
        cryptoCoinId: id
      })
    }
    return acc
  }, {})

export function reducer(state: MarketChartsStateType = initialState, action: Object): MarketChartsStateType {
  switch (action.type) {
    case types.UPDATE_RANGE_FOR_COIN_ID:
      const { cryptoCoinId, paperCoinId, rangeName } = action.payload
      return {
        ...state,
        [cryptoCoinId]: {
          ...state[cryptoCoinId],
          [paperCoinId]: {
            ...state[cryptoCoinId][paperCoinId],
            [rangeName]: action.payload
          }
        }
      }
    default:
      return state
  }
}
