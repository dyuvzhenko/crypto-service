// @flow
import { BALANCE as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { apiOwnService } from '@/api'
import type { BalanceStateType } from '@/types/state/stable-data/balance'

export const setData = ({ usd, eur }: { usd: number, eur: number }) => ({ type: types.SET_DATA, data: { usd, eur } })

export const getAllBalance = () => (dispatch: Function) =>
  dispatch(apiOwnService.getAllUserBalance({
    stateName: REQUEST_NAME.STABLE_DATA.BALANCE.GET_ALL
  }))
  .then((data: Object) => dispatch(setData(data)))

const initialState: BalanceStateType = {
  data: {
    usd: null,
    eur: null
  }
}

export function reducer(state: BalanceStateType = initialState, action: Object): BalanceStateType {
  switch (action.type) {
    case types.SET_DATA:
      return {
        ...state,
        data: {
          usd: action.data.usd,
          eur: action.data.eur
        }
      }
    default:
      return state
  }
}
