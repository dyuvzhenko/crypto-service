// @flow
import { WALLETS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { apiOwnService } from '@/api'
import type { WalletType } from '@/types'
import type { WalletsStateType } from '@/types/state/stable-data/wallets'

const setList = ({ wallets }: { wallets: Array<WalletType> }) => ({ type: types.SET_LIST, wallets })

export const getAllWallets = () => (dispatch: Function) =>
  dispatch(apiOwnService.getAllUserWallets({
    stateName: REQUEST_NAME.STABLE_DATA.WALLETS.GET_ALL
  }))
  .then((data: Object) => dispatch(setList(data)))

export const refreshListWithOneWallet = (wallet: WalletType) => ({ type: types.REFRESH_LIST_WITH_ONE_WALLET, wallet })

const initialState: WalletsStateType = {
  list: []
}

export function reducer(state: WalletsStateType = initialState, action: Object): WalletsStateType {
  switch (action.type) {
    case types.SET_LIST:
      return {
        ...state,
        list: action.wallets
      }
    case types.REFRESH_LIST_WITH_ONE_WALLET:
      return {
        ...state,
        list: state.list.map((wallet: WalletType) => wallet.id === action.wallet.id ? action.wallet : wallet)
      }
    default:
      return state
  }
}
