// @flow
import { MESSAGES as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { apiOwnService } from '@/api'
import type { MessageType } from '@/types'
import type { MessagesStateType } from '@/types/state/stable-data/messages'

const setList = ({ messages }: { messages: Array<MessageType> }) => ({ type: types.SET_LIST, messages })

export const getAllMessages = () => (dispatch: Function) =>
  dispatch(apiOwnService.getAllUserMessages({
    stateName: REQUEST_NAME.STABLE_DATA.MESSAGES.GET_ALL
  }))
  .then((data: Object) => dispatch(setList(data)))

const initialState: MessagesStateType = {
  list: []
}

export function reducer(state: MessagesStateType = initialState, action: Object): MessagesStateType {
  switch (action.type) {
    case types.SET_LIST:
      return {
        ...state,
        list: action.messages
      }
    default:
      return state
  }
}
