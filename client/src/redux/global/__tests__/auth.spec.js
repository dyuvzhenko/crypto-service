import expect from 'expect'
import { reducer, setUserAuthData } from '../auth'
import { AUTH as types } from '../../action-types'

const initialState = reducer(undefined, {})

describe('auth reducer', () => {
  it(`should handle ${types.SET_USER_DATA}`, () => {
    const userInfo = { user: 'name' }
    const token = 'token'
    const expectedState = {
      ...initialState,
      isAuthorized: true,
      userInfo,
      token
    }

    expect(reducer(initialState, setUserAuthData(userInfo, token))).toEqual(expectedState)
  })
})

describe('auth actions', () => {
  it('should handle setUserAuthData', () => {
    const userInfo = { user: 'name' }
    const token = 'token'
    const expectedAction = {
      type: types.SET_USER_DATA,
      userInfo,
      token
    }

    expect(setUserAuthData(userInfo, token)).toEqual(expectedAction)
  })
})
