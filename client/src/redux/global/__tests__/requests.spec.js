import expect from 'expect'
import { REQUESTS as types } from '../../action-types'
import {
  REQUEST_NAME, reducer,
  startRequest, endRequestSuccessfully, endRequestWithError
} from '../requests'

const initialState = reducer(undefined, {})

describe('requests reducer', () => {
  it(`should handle ${types.START}`, () => {
    const action = startRequest(REQUEST_NAME.LOGIN)
    const expectedState = {
      ...initialState,
      [REQUEST_NAME.LOGIN]: {
        ...initialState[REQUEST_NAME.LOGIN],
        pending: true
      }
    }

    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it(`should handle ${types.END_SUCCESSFULLY}`, () => {
    const action = endRequestSuccessfully(REQUEST_NAME.LOGIN)
    const expectedState = {
      ...initialState,
      [REQUEST_NAME.LOGIN]: {
        pending: false,
        error: null
      }
    }

    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it(`should handle ${types.START}`, () => {
    const error = { message: 'error_message' }
    const action = endRequestWithError(REQUEST_NAME.LOGIN, error)
    const expectedState = {
      ...initialState,
      [REQUEST_NAME.LOGIN]: {
        pending: false,
        error
      }
    }

    expect(reducer(initialState, action)).toEqual(expectedState)
  })
})

describe('requests actions', () => {
  it('should handle startRequest', () => {
    const name = REQUEST_NAME.LOGIN
    const expectedAction = { type: types.START, name }
    expect(startRequest(name)).toEqual(expectedAction)
  })

  it('should handle endRequestSuccessfully', () => {
    const name = REQUEST_NAME.LOGIN
    const expectedAction = { type: types.END_SUCCESSFULLY, name }
    expect(endRequestSuccessfully(name)).toEqual(expectedAction)
  })

  it('should handle endRequestSuccessfully', () => {
    const name = REQUEST_NAME.LOGIN
    const error = { message: 'error_message' }
    const expectedAction = { type: types.END_WITH_ERROR, name, error }
    expect(endRequestWithError(name, error)).toEqual(expectedAction)
  })
})
