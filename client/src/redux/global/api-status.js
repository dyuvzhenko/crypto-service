// @flow
import { apiOwnService } from '@/api'
import { AUTH, API_STATUS as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { actionOnAppStart } from '@/redux/scenarios'
import { detectUserTheme } from '@/components/pages-protected/_wrapper'
import type { ApiStatusStateType } from '@/types/state/global/api-status'

const EXPECTED_RESPONSE = {
  OWN_API: 'Server is fine!'
}

export const testAPI = () => (dispatch: Function) => {
  dispatch(detectUserTheme())
  dispatch(apiOwnService.ping({ stateName: REQUEST_NAME.GLOBAL.API_STATUS.PING_OWN_API }))
  .then((ownServiceResponse: string) => {
    const isAlive = ownServiceResponse === EXPECTED_RESPONSE.OWN_API

    if (!isAlive) {
      throw new Error()
    }

    dispatch(setApiStatus(true))
    dispatch(actionOnAppStart())
  })
  .catch(() => {
    dispatch(setApiStatus(false))
    dispatch({ type: AUTH.SET_UNAUTHORIZED })
  })
}

const setApiStatus = (status: boolean) => ({ type: types.SET_STATUS, status })

const initialState: ApiStatusStateType = {
  apiAlive: null
}

export function reducer(state: ApiStatusStateType = initialState, action: Object): ApiStatusStateType {
  switch (action.type) {
    case types.SET_STATUS:
      return {
        apiAlive: action.status
      }
    case AUTH.LOG_OUT:
      return {
        apiAlive: true
      }
    default:
      return state
  }
}
