// @flow
import { REQUESTS as types } from '@/redux/action-types'
import type { RequestsStateType } from '@/types/state/global/requests'

export const REQUEST_NAME = {
  GLOBAL: {
    AUTH: {
      VERIFY_TOKEN: '@global/auth/VERIFY_TOKEN',
      DO_SIGN_UP: '@global/auth/DO_SIGN_UP',
      DO_LOGIN: '@global/auth/DO_LOGIN'
    },
    API_STATUS: {
      PING_OWN_API: '@global/api-status/PING_OWN_API'
    }
  },
  STABLE_DATA: {
    BALANCE: {
      GET_ALL: '@stable-data/balance/GET_ALL'
    },
    MESSAGES: {
      GET_ALL: '@stable-data/messages/GET_ALL'
    },
    WALLETS: {
      GET_ALL: '@stable-data/wallets/GET_ALL'
    }
  },
  UI: {
    DASHBOARD: {
      GET_COINS_VALUE: '@ui/dashboardGET_COINS_VALUE',
      GET_TRANSACTIONS: '@ui/dashboard/GET_TRANSACTIONS',
      FILTER_TRANSACTIONS: '@ui/dashboard/FILTER_TRANSACTIONS',
      SEND_COINS: '@ui/dashboard/SEND_COINS',
      RECEIVE_COINS: '@ui/dashboard/RECEIVE_COINS',
      GET_UPCOMING_EVENTS: '@ui/dashboard/GET_UPCOMING_EVENTS',
      GET_MARKET_CHARTS: '@ui/dashboard/GET_MARKET_CHARTS',
      GET_MAIN_CHART: '@ui/dashboard/GET_MAIN_CHART',
      GET_MONEY: '@ui/dashboard/GET_MONEY'
    },
    WALLETS: {
      GET_LIST: '@ui/wallets/GET_LIST',
      GET_USD_EXCHANGE: '@ui/wallets/GET_USD_EXCHANGE',
      GET_EUR_EXCHANGE: '@ui/wallets/GET_EUR_EXCHANGE',
      ADD_NEW_WALLET: '@ui/wallets/ADD_NEW_WALLET'
    },
    MARKETS: {
      GET_LIST: '@ui/markets/GET_LIST',
      GET_DETAILED_GRAPH: '@ui/markets/GET_DETAILED_GRAPH'
    },
    EVENTS: {
      GET_LIST: '@ui/events/GET_LIST'
    },
    TRANSACTIONS: {
      GET_LIST: '@ui/transactions/GET_LIST',
      EDIT_TRANSACTION: '@ui/transactions/EDIT_TRANSACTION'
    },
    CALCULATOR: {
      GET_GRAPH_LIST: '@ui/calculator/GET_GRAPH_LIST',
      GET_USD_EXCHANGE: '@ui/calculator/GET_USD_EXCHANGE',
      GET_EUR_EXCHANGE: '@ui/calculator/GET_EUR_EXCHANGE'
    },
    NEWS: {
      GET_LIST: '@ui/news/GET_LIST'
    },
    TRANSACTION_BY_ID: {
      GET: '@ui/transaction-by-id/GET'
    },
    SETTINGS: {
      PROFILE: {
        SAVE_NICKNAME_AND_EMAIL: '@ui/settings/profile/SAVE_NICKNAME_AND_EMAIL',
        SAVE_REAL_NAME_AND_BIRTHDATE: '@ui/settings/profile/SAVE_REAL_NAME_AND_BIRTHDATE',
        SAVE_USER_PICTURE: '@ui/settings/profile/SAVE_USER_PICTURE',
        SAVE_NEW_PASSWORD: '@ui/settings/profile/SAVE_NEW_PASSWORD'
      }
    }
  }
}

export const startRequest = (name: string) =>
  ({ type: types.START, name })

export const endRequestSuccessfully = (name: string) =>
  ({ type: types.END_SUCCESSFULLY, name })

export const endRequestWithError = (name: string, error: any) =>
  ({ type: types.END_WITH_ERROR, name, error })

const getRequestObj = () => ({ pending: null, error: null })
const getArrayOfRequests = (requests: Object) => Object.values(requests).reduce(
  (acc: Array<string>, curr: any) => {
    typeof curr === 'object' ? acc.push(...getArrayOfRequests(curr)) : acc.push(curr)
    return acc
  }, []
)

const initialState: RequestsStateType = getArrayOfRequests(REQUEST_NAME).reduce(
  (acc: Object, curr: string) => {
    acc[curr] = getRequestObj()
    return acc
  }, {}
)

export function reducer(state: RequestsStateType = initialState, action: Object): RequestsStateType {
  switch (action.type) {
    case types.START:
      return {
        ...state,
        [action.name]: {
          ...state[action.name],
          pending: true
        }
      }
    case types.END_SUCCESSFULLY:
      return {
        ...state,
        [action.name]: {
          pending: false,
          error: null
        }
      }
    case types.END_WITH_ERROR:
      return {
        ...state,
        [action.name]: {
          pending: false,
          error: action.error
        }
      }
    default:
      return state
  }
}
