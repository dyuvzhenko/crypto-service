// @flow
import jwtDecode from 'jwt-decode'
import compareVersions from 'compare-versions'
import { apiOwnService } from '@/api'
import { AUTH as types } from '@/redux/action-types'
import { REQUEST_NAME } from '@/redux/global/requests'
import { setToken } from '@/utils/local-storage'
import { cleanAllUserData } from '@/redux/scenarios'
import type { AuthStateType } from '@/types/state/global/auth'
import type { UserInfoType } from '@/types'

const tryDecodeToken = (token: ?string) => {
  try { return jwtDecode(token) }
  catch { return null }
}

export const logout = () => ({ type: types.LOG_OUT })

export const cleanUserAuthData = () => ({ type: types.CLEAN_USER_DATA })

export const setUserAuthData = (userInfo: UserInfoType, token: string) =>
  ({ type: types.SET_USER_DATA, userInfo, token })

export const setTokenToState = (token: ?string) => ({ type: types.SET_TOKEN, token })

export const tryAuthorization = ({
  token,
  breakTokenAfterRelease = null,
  shouldSaveToken = false
}: Object) =>
  (dispatch: Function) => {
    const userInfo = tryDecodeToken(token)
    if (userInfo) {
      if (breakTokenAfterRelease && compareVersions.compare(userInfo.release, breakTokenAfterRelease, '<')) {
        dispatch(doLogout())
        return
      }

      dispatch(setUserAuthData(userInfo, token))
      if (shouldSaveToken) {
        setToken(token)
      }
    }
  }

export const verifyToken = () => apiOwnService.verify({
  stateName: REQUEST_NAME.GLOBAL.AUTH.VERIFY_TOKEN
})

export const doLogout = () => (dispatch: Function) => {
  dispatch(cleanAllUserData())
  dispatch(logout())
}

const initialState: AuthStateType = {
  isAuthorized: null,
  userInfo: null,
  token: null
}

export function reducer(state: AuthStateType = initialState, action: Object): AuthStateType {
  switch (action.type) {
    case types.SET_TOKEN:
      return {
        ...state,
        token: action.token
      }
    case types.SET_USER_DATA:
      return {
        ...state,
        isAuthorized: true,
        userInfo: action.userInfo,
        token: action.token
      }
    case types.CLEAN_USER_DATA:
      return {
        isAuthorized: false,
        userInfo: null,
        token: null
      }
    case types.LOG_OUT:
      return {
        ...initialState,
        isAuthorized: false
      }
    case types.SET_UNAUTHORIZED:
      return {
        ...state,
        isAuthorized: false
      }
    default:
      return state
  }
}
