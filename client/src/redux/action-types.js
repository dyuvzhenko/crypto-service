// @flow
/* Global */
export const REQUESTS = {
  START: '@global/requests/START',
  END_SUCCESSFULLY: '@global/requests/END_SUCCESSFULLY',
  END_WITH_ERROR: '@global/requests/END_WITH_ERROR'
}

export const AUTH = {
  LOG_OUT: '@global/auth/LOG_OUT',
  SET_UNAUTHORIZED: '@global/auth/SET_UNAUTHORIZED',
  SET_TOKEN: '@global/auth/SET_TOKEN',
  SET_USER_DATA: '@global/auth/SET_USER_DATA',
  CLEAN_USER_DATA: '@global/auth/CLEAN_USER_DATA'
}

export const API_STATUS = {
  SET_STATUS: '@global/api-status/SET_STATUS'
}

/* Stable data */
export const MESSAGES = {
  SET_LIST: '@stable-data/messages/SET_LIST'
}

export const BALANCE = {
  SET_DATA: '@stable-data/balance/SET_DATA'
}

export const WALLETS = {
  SET_LIST: '@stable-data/wallets/SET_LIST',
  REFRESH_LIST_WITH_ONE_WALLET: '@stable-data/wallets/REFRESH_LIST_WITH_ONE_WALLET'
}

export const EXCHANGE = {
  SET_COINS_VALUE_BY_USD: '@stable-data/exchange/SET_COINS_VALUE_BY_USD',
  SET_COINS_VALUE_BY_EUR: '@stable-data/exchange/SET_COINS_VALUE_BY_EUR'
}

export const MARKET_CHARTS = {
  UPDATE_RANGE_FOR_COIN_ID: '@stable-data/market-charts/UPDATE_RANGE_FOR_COIN_ID'
}

/* UI */
export const WRAPPER_PROTECTED = {
  SET_DARK_THEME: '@ui/wrapper-protected/SET_DARK_THEME',
  TOGGLE_MOBILE_SIDE_MENU: '@ui/wrapper-protected/TOGGLE_MOBILE_SIDE_MENU',
  TOGGLE_MENU: '@ui/wrapper-protected/TOGGLE_MENU'
}

export const NOTIFICATIONS = {
  CLOSE: '@ui/notifications/CLOSE',
  ADD: '@ui/notifications/ADD'
}

export const PUBLIC_PREVIEW = {
  SET_SLIDE: '@ui/public-preview/SET_SLIDE',
  SET_NEXT_SLIDE: '@ui/public-preview/SET_NEXT_SLIDE',
  IMAGES_PRELOADED: '@ui/public-preview/IMAGES_PRELOADED'
}

export const DASHBOARD = {
  SET_ALL_TRANSACTIONS_TOTAL: '@ui/dashboard/SET_ALL_TRANSACTIONS_TOTAL',
  SET_LIST_TRANSACTIONS: '@ui/dashboard/SET_LIST_TRANSACTIONS',
  SET_LIST_UPCOMING_EVENTS: '@ui/dashboard/SET_LIST_UPCOMING_EVENTS',
  SET_LIST_MARKET_CHARTS: '@ui/dashboard/SET_LIST_MARKET_CHARTS',
  SET_VS_CURRENCY_FOR_MARKET_CHARTS: '@ui/dashboard/SET_VS_CURRENCY_FOR_MARKET_CHARTS',
  SET_TRANSACTIONS_INCOME_STATUS: '@ui/dashboard/SET_TRANSACTIONS_INCOME_STATUS',
  SET_WALLET_FOR_MAKE_TRANSACTION: '@ui/dashboard/SET_WALLET_FOR_MAKE_TRANSACTION',
  SELECT_WALLET_COIN_ID: '@ui/dashboard/SELECT_WALLET_COIN_ID',
  SET_MAIN_CHART: '@ui/dashboard/SET_MAIN_CHART'
}

export const UI_WALLETS = {
  RESET_STATE: '@ui/wallets/RESET_STATE',
  TOGGLE_GRAPH_GRID: '@ui/wallets/TOGGLE_GRAPH_GRID',
  TOGGLE_WALLET_FORM: '@ui/wallets/TOGGLE_WALLET_FORM',
  SET_GRID_LOOK: '@ui/wallets/SET_GRID_LOOK',
  SET_GRID: '@ui/wallets/SET_GRID',
  SET_DATA: '@ui/wallets/SET_DATA'
}

export const MARKETS = {
  SET_DETAILED_GRAPH_DATA: '@ui/markets/SET_DETAILED_GRAPH_DATA',
  SET_DETAILED_GRAPH_RANGE_NAME: '@ui/markets/SET_DETAILED_GRAPH_RANGE_NAME',
  SET_DETAILED_GRAPH_INTERVAL: '@ui/markets/SET_DETAILED_GRAPH_INTERVAL',
  TOGGLE_DETAILED_GRAPH: '@ui/markets/TOGGLE_DETAILED_GRAPH',
  TOGGLE_GRAPH_GRID: '@ui/markets/TOGGLE_GRAPH_GRID',
  SET_GRID_LOOK: '@ui/markets/SET_GRID_LOOK',
  SET_GRID: '@ui/markets/SET_GRID'
}

export const EVENTS = {
  SET_LIST: '@ui/events/SET_LIST',
  SET_SHOWED_MONTH: '@ui/events/SET_SHOWED_MONTH',
  SET_SELECTED_INTERVAL: '@ui/events/SET_SELECTED_INTERVAL'
}

export const TRANSACTIONS = {
  RESET_STATE: '@ui/transactions/RESET_STATE',
  SET_GRID_LOOK: '@ui/transactions/SET_GRID_LOOK',
  SET_SELECTED_INTERVAL: '@ui/transactions/SET_SELECTED_INTERVAL',
  SET_GRID: '@ui/transactions/SET_GRID'
}

export const CALCULATOR = {
  RESET_STATE: '@ui/calculator/RESET_STATE',
  SET_EXCHANGE_PAIR: '@ui/calculator/SET_EXCHANGE_PAIR',
  SET_SELECTION_START: '@ui/calculator/SET_SELECTION_START',
  SET_GRAPH_LIST: '@ui/calculator/SET_GRAPH_LIST',
  CHANGE_AMOUNT: '@ui/calculator/CHANGE_AMOUNT'
}

export const NEWS = {
  SET_GRID: '@ui/news/SET_GRID',
  SET_LIST: '@ui/news/SET_LIST',
  SET_LAST_RECEIVED_INTERVAL: '@ui/news/SET_LAST_RECEIVED_INTERVAL',
  SET_SHOWED_LENGTH: '@ui/news/SET_SHOWED_LENGTH'
}

export const SETTINGS = {
  SET_ACCOUNT_PROGRESS: '@ui/settings/SET_ACCOUNT_PROGRESS'
}

export const NEWS_BY_ID = {
  RESET_DATA: '@ui/news-by-id/RESET_DATA',
  SET_DATA: '@ui/news-by-id/SET_DATA'
}

export const TRANSACTION_BY_ID = {
  RESET_DATA: '@ui/transaction-by-id/RESET_DATA',
  SET_DATA: '@ui/transaction-by-id/SET_DATA'
}
