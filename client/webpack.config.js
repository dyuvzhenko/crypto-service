const { resolve } = require('path')
const packageJSON = require('./package.json')
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const version = 'v' + packageJSON.version

module.exports = ({ mode, analyze = false }) => ({
  target: 'web',
  mode: mode,
  watch: mode === 'development',
  entry: resolve(__dirname, 'src', 'entry.js'),
  output: {
    publicPath: '/',
    path: resolve(__dirname, 'build'),
    filename: `[name].${version}.js`
  },
  devServer: mode === 'production' ? {} : {
    port: 3001,
    stats: 'errors-only',
    contentBase: resolve(__dirname, 'dist'),
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:3002'
    }
  },
  stats: {
    children: false
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: mode === 'development'
            },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              localsConvention: 'camelCaseOnly',
              modules: {
                mode: 'local',
                localIdentName: mode === 'development' ? '[name]__[local][hash:base64:5]' : '[hash:base64:5]',
                context: resolve(__dirname, 'src')
              }
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                require('autoprefixer')({ grid: true }),
                require('cssnano')
              ]
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|webp|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: `[name].${version}.[ext]`,
          outputPath: 'assets'
        }
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({ filename: `[name].${version}.css` }),
    new HtmlPlugin({
      template: resolve(__dirname, 'public', 'index.html'),
      meta: {
        robots: 'noindex',
        viewport: 'width=device-width, initial-scale=1',
        description: 'Demo app to show info about cryptocurrencies'
      }
    }),
    new CopyPlugin([
      { from: resolve(__dirname, 'public', 'fonts'), to: './fonts' },
      { from: resolve(__dirname, 'public', 'favicon'), to: './favicon' },
      { from: resolve(__dirname, 'public', 'manifest.json'), to: './manifest.json' }
    ]),
    analyze && new BundleAnalyzerPlugin()
  ].filter(Boolean),
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
    alias: {
      ['@']: resolve(__dirname, 'src')
    }
  },
  optimization: mode === 'development' ? {} : {
    splitChunks: {
      chunks: 'async',
      maxAsyncRequests: 10,
      maxInitialRequests: 3,
      minSize: 160000,
			maxSize: 200000,
      name: mode === 'development',
      cacheGroups: {
        vendors: {
          name: 'vendors',
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
        default: {
          name: 'main',
          minChunks: 2,
          priority: -20
        }
      }
    }
  }
})
