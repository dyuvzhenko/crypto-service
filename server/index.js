const path = require('path');
const cors = require('cors');
const express = require('express');
const app = express();

const apiPing = require('./api/ping');
const apiAuth = require('./api/auth');
const apiTransactions = require('./api/transactions');
const apiMessages = require('./api/messages');
const apiWallets = require('./api/wallets');
const apiBalance = require('./api/balance');

app.use(express.json());
app.use(express.static(__dirname + '/uploads'));

const DOMAINS = ['https://am4dev.com', 'https://www.am4dev.com'];
app.use(cors({ origin: DOMAINS }));

app.post('/api/login', apiAuth.onLogin);
app.post('/api/signup', apiAuth.onSignUp);
app.get('/api/verify', apiAuth.onVerifyToken);
app.post('/api/user/update-myself', apiAuth.updateMyself);
app.post('/api/user/update-my-picture', apiAuth.updateMyPicture);
app.post('/api/user/update-my-password', apiAuth.updateMyPassword);

app.get('/api/user/transactions', apiTransactions.getAll);
app.get('/api/user/transactions/:id', apiTransactions.getById);
app.post('/api/user/transaction/send', apiTransactions.send);
app.post('/api/user/transaction/receive', apiTransactions.receive);
app.put('/api/user/transactions/:id/reject', apiTransactions.reject);
app.delete('/api/user/transactions/:id', apiTransactions.remove);

app.get('/api/user/wallets', apiWallets.getAll);
app.get('/api/user/all-wallets-with-history', apiWallets.getAllWithHistory);
app.post('/api/user/add-wallet', apiWallets.addWallet);

app.get('/api/user/messages', apiMessages.getAll);
app.get('/api/user/balance', apiBalance.getAll);
app.get('/api/user/get-money', apiBalance.getMoney);

app.get('/api/ping', apiPing);

// (!) In a good way, statics should live separately with their 'nginx' config.
const staticPath = path.join(__dirname, '/static');
app.use(express.static(staticPath));
app.get('*', (req, res) => res.sendFile(path.join(staticPath, '/index.html')));

app.listen(process.env['PORT'] || 3002);
