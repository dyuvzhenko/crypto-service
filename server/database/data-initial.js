const jsonwebtoken = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const faker = require('faker')
const dayjs = require('dayjs')

const CRYPTO_CURRENCY = {
  BTC: { id: 'bitcoin', name: 'Bitcoin' },
  LTC: { id: 'litecoin', name: 'Litecoin' },
  DASH: { id: 'dash', name: 'Dash' },
  XRP: { id: 'ripple', name: 'Ripple' },
  ETH: { id: 'ethereum', name: 'Ethereum' }
}

const arrWalletTypes = ['bitcoin', 'litecoin', 'dash', 'ripple', 'ethereum']

const arrMockTokens = [
  '1PRj85hu9RXPZTzxtko9stfs6nRo1vyrQB',
  '14FuM1aBgcNc4Y5TskDBqhwknVpRnkwjbx',
  '1M5TdPV8G6YfcKkrNEyhPu3nBpWtGXJGho',
  '1P9RQEr2XeE3PEb44ZE35sfZRRW1JHU8qx',
  '1AGm6Jc43FUaYRKm8wj2cBpJ7FWgjxwCXW'
]

const initialUser = {
  id: 'users-id-0',
  image: null,
  nickname: 'Mock User',
  first_name: 'Ivan',
  last_name: 'Ivanov',
  email: 'user@mail.com',
  birthdate: '1995-12-08',
  hash: bcrypt.hashSync('pass', 10)
}

const initialUserBalances = [
  { id: 'userBalances-id-0', user_id: initialUser.id, usd: 34330, eur: 27995 }
]

let initialUserWallets = [
  { id: 'wallets-id-0', user_id: initialUser.id, coin_id: CRYPTO_CURRENCY.BTC.id, value: 0, token: 'wallet-bitcoin-hash' },
  { id: 'wallets-id-1', user_id: initialUser.id, coin_id: CRYPTO_CURRENCY.ETH.id, value: 0, token: 'wallet-ethereum-hash' },
  { id: 'wallets-id-2', user_id: initialUser.id, coin_id: CRYPTO_CURRENCY.LTC.id, value: 0, token: 'wallet-litecoin-hash' },
  { id: 'wallets-id-3', user_id: initialUser.id, coin_id: CRYPTO_CURRENCY.DASH.id, value: 0, token: 'wallet-dash-hash' },
  { id: 'wallets-id-4', user_id: initialUser.id, coin_id: CRYPTO_CURRENCY.XRP.id, value: 0, token: 'wallet-ripple-hash' }
]

let initialUserTransactions = []

initialUserWallets = initialUserWallets.map(wallet => {
  let walletValue = 0
  let date = dayjs().subtract(320, 'days')
  const transactions = new Array(300).fill().map((_, i) => {
    const amount = Number((faker.random.number({ min: 0, max: 100, precision: 0.001 })).toFixed(4))
    const isIncome = walletValue < amount

    date = date.add(1, 'days')
    walletValue = Number((isIncome ? walletValue + amount : walletValue - amount).toFixed(4))
    return {
      id: `transactions-id-${initialUserTransactions.length + i}`,
      user_id: initialUser.id,
      date: date.toISOString(),

      isIncome,
      amount,
      status: isIncome ? 'DONE' : ((i % 2 === 0) ? 'PENDING' : 'DONE'),

      from_token: isIncome ? arrMockTokens[i % arrMockTokens.length] : wallet.token,
      to_token: isIncome ? wallet.token : arrMockTokens[i % arrMockTokens.length],
      coin_id: wallet.coin_id
    }
  })

  initialUserTransactions = [...initialUserTransactions, ...transactions]

  return {
    ...wallet,
    value: walletValue
  }
})

const initialUserMessages = new Array(9).fill().map((_, i) => ({
  id: `messages-id-${i}`,
  from_user_id: 'mock-user-id',
  to_user_id: initialUser.id,
  date: faker.date.past(2),
  author: 'mock-author',
  title: `mock-title-${i}`,
  text: `mock-text-${i}`
}))

module.exports = {
  users: [initialUser],
  userBalances: initialUserBalances,
  messages: initialUserMessages,
  wallets: initialUserWallets,
  transactions: initialUserTransactions
}
