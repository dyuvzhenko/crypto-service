const fs = require('fs');
const path = require('path');
const dataInitial = require('./data-initial');

class Database {
  constructor() {
    this.getDatabase = this.getDatabase.bind(this)
    this.rewriteDatabase = this.rewriteDatabase.bind(this)
    this.dbPath = path.join(__dirname, 'data-used.json')
    this.getDatabase(data => {
      this.data = data
    })

    this.getAll = this.getAll.bind(this)
    this.addRowToTable = this.addRowToTable.bind(this)
    this.editRowInTable = this.editRowInTable.bind(this)
    this.deleteRowInTable = this.deleteRowInTable.bind(this)
  }

  rewriteDatabase(data, callback) {
    fs.writeFile(this.dbPath, JSON.stringify(data), err => {
      if (!err) {
        this.data = data
      }
      callback(err)
    })
  }

  getDatabase(callback) {
    try {
      const data = fs.readFileSync(this.dbPath, 'utf8')
      const jsonData = JSON.parse(data)
      if (typeof jsonData === 'object') {
        Object.keys(dataInitial).forEach(tableName => {
          if (jsonData.hasOwnProperty(tableName) && Array.isArray(jsonData[tableName])) {
            return
          } else {
            throw new Error
          }
        })
        callback(jsonData)
      }
    } catch (err) {
      this.rewriteDatabase(dataInitial, () => callback(dataInitial))
    }
  }

  getAll(field) {
    return this.data[field] || []
  }

  addRowToTable(tableName, value, callback) {
    const id = `${tableName}-id-${this.data[tableName].length}`
    const newElem = { ...value, id }
    this.data = {
      ...this.data,
      [tableName]: [
        ...this.data[tableName],
        newElem
      ]
    }
    this.rewriteDatabase(this.data, err => callback(err, newElem))
  }

  editRowInTable(tableName, newRow, callback) {
    this.data = {
      ...this.data,
      [tableName]: this.data[tableName].map(row => row.id === newRow.id ? newRow : row)
    }
    this.rewriteDatabase(this.data, err => callback(err, newRow))
  }

  deleteRowInTable(tableName, id, callback) {
    this.data = {
      ...this.data,
      [tableName]: this.data[tableName].filter(row => row.id !== id)
    }
    this.rewriteDatabase(this.data, err => callback(err))
  }
}


module.exports = { Database }
