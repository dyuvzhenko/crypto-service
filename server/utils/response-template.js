module.exports.getSuccessResponse = data => ({ data: data });
module.exports.getErrorResponse = (message = 'Server error') => ({ error: { message } });
