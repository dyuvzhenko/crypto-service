const fs = require('fs');
const path = require('path');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const multer = require('multer');
const appRelease = require('../package.json').version;

const { getErrorResponse, getSuccessResponse } = require('../utils/response-template');
const { Database } = require('../database');

const pathToAvatars = path.join(__dirname, '..', 'uploads', 'avatar');
const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, pathToAvatars),
  filename: (req, file, cb) => {
    const ext = file.originalname.split('.').pop();
    const list = fs.readdirSync(pathToAvatars);

    const foundImageName = list.find(f => (new RegExp(req.params.userId)).test(f));
    if (!foundImageName) {
      cb(null, req.params.userId + '.' + 0 + '.' + ext);
      return
    }

    const counter = Number(foundImageName.split('.').slice(-2, -1)[0]) + 1;
    fs.unlinkSync(path.join(pathToAvatars, foundImageName));
    cb(null, req.params.userId + '.' + counter + '.' + ext);
  }
})
const upload = multer({ storage }).single('avatar');

const createToken = data =>
  jsonwebtoken.sign({ ...data, release: appRelease }, 'password-key-cryptonix-server');

const decodeTokenFromHeaders = ({ authorization = '' }) => {
  if (typeof authorization !== 'string') {
    return null
  }
  try {
    return jsonwebtoken.verify(
      authorization.replace(/^Bearer[ ]/g, ''),
      'password-key-cryptonix-server'
    )
  } catch(err) {
    return null
  }
}

const onLogin = (req, res) => {
  const db = new Database();
  const { email, password } = req.body;
  const foundUser = db.getAll('users').find(user => user.email === email);

  if (foundUser && bcrypt.compareSync(password, foundUser.hash)) {
    delete foundUser.hash
    res.send(getSuccessResponse({ token: createToken(foundUser) }));
  }
  else {
    res.status(401).send(getErrorResponse('Authorization error. Please try again.'));
  }
}

const onSignUp = (req, res) => {
  const db = new Database();
  const { nickname, email, password } = req.body;

  if (db.getAll('users').some(user => user.email === email)) {
    res.status(401).send(getErrorResponse('This email has already been taken.'))
    return
  }

  const newUser = {
    hash: bcrypt.hashSync(password, 10),
    image: null,
    first_name: '',
    last_name: '',
    birthdate: '',
    email,
    nickname
  };

  db.addRowToTable('users', newUser, (err, data) => {
    if (err) {
      res.status(500).end()
      return
    } else {
      const balanceForNewUser = { user_id: data.id, usd: 0, eur: 0 }
      db.addRowToTable('userBalances', balanceForNewUser, err => {
        res.send(getSuccessResponse({ email }))
      })
    }
  });
};

const onVerifyToken = (req, res) => {
  const userInfo = decodeTokenFromHeaders(req.headers);
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token.'));
    return
  }

  const db = new Database();
  const foundUser = db.getAll('users').find(user => user.email === userInfo.email);
  if (!foundUser) {
    res.status(401).send(getErrorResponse('User doesn\'t exist.'));
    return
  }

  res.send(getSuccessResponse({ valid: true, breakTokenAfterRelease: '0.0.5' }))
}

const updateMyself = (req, res) => {
  const userInfo = decodeTokenFromHeaders(req.headers);
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token.'));
    return
  }

  const db = new Database();
  const allUsers = db.getAll('users');
  const foundUser = allUsers.find(user => user.email === userInfo.email);
  if (!foundUser) {
    res.status(401).send(getErrorResponse('User doesn\'t exist.'));
    return
  }

  if (req.body.hasOwnProperty('email') && allUsers.find(e => e.email === req.body.email)) {
    if (req.body.email !== userInfo.email) {
      res.status(401).send(getErrorResponse('The user with such mail already exists.'));
      return
    }
  }

  const editedUser = {
    ...foundUser,
    ...req.body
  }

  db.editRowInTable('users', editedUser, (err, data) => {
    if (err) {
      res.status(401).send(getErrorResponse())
      return
    }

    delete editedUser.hash
    res.send(getSuccessResponse({ token: createToken(editedUser), userInfo: editedUser }))
  });
}

const updateMyPicture = (req, res) => {
  const userInfo = decodeTokenFromHeaders(req.headers);
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token.'));
    return
  }

  const db = new Database();
  const allUsers = db.getAll('users');
  const foundUser = allUsers.find(user => user.email === userInfo.email);
  if (!foundUser) {
    res.status(401).send(getErrorResponse('User doesn\'t exist.'));
    return
  }

  req.params.userId = foundUser.id;
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError || err) {
      res.status(401).send(getErrorResponse())
      return
    }

    const editedUser = {
      ...foundUser,
      image: req.file.filename
    }

    db.editRowInTable('users', editedUser, (err, data) => {
      if (err) {
        res.status(401).send(getErrorResponse())
        return
      }

      delete editedUser.hash
      res.send(getSuccessResponse({ token: createToken(editedUser), userInfo: editedUser }))
    });
  })
}

const updateMyPassword = (req, res) => {
  const userInfo = decodeTokenFromHeaders(req.headers);
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token.'));
    return
  }

  const db = new Database();
  const allUsers = db.getAll('users');
  const foundUser = allUsers.find(user => user.email === userInfo.email);
  if (!foundUser) {
    res.status(401).send(getErrorResponse('User doesn\'t exist.'));
    return
  }

  if (!bcrypt.compareSync(req.body.currentPassword, foundUser.hash)) {
    res.status(401).send(getErrorResponse('Invalid password.'));
    return
  }

  const editedUser = {
    ...foundUser,
    hash: bcrypt.hashSync(req.body.newPassword, 10)
  }

  db.editRowInTable('users', editedUser, (err, data) => {
    if (err) {
      res.status(401).send(getErrorResponse())
      return
    }

    delete editedUser.hash
    res.send(getSuccessResponse({ token: createToken(editedUser), userInfo: editedUser }))
  });
}

module.exports = {
  decodeTokenFromHeaders,
  onLogin,
  onSignUp,
  onVerifyToken,
  updateMyself,
  updateMyPicture,
  updateMyPassword
}
