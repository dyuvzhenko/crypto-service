const { getSuccessResponse } = require('../utils/response-template');

module.exports = (req, res) => res.status(200).send(
  getSuccessResponse('Server is fine!')
);
