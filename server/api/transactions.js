const dayjs = require('dayjs');
const utcPlugin = require('dayjs/plugin/utc');
dayjs.extend(utcPlugin)

const { getErrorResponse, getSuccessResponse } = require('../utils/response-template');
const { decodeTokenFromHeaders } = require('./auth');
const { Database } = require('../database');

module.exports.getAll = (req, res) => {
  const db = new Database();
  const list = db.getAll('transactions').sort((a, b) => (new Date(b.date)) - (new Date(a.date)));
  const count = req.query.count || 20
  const page = Number(req.query.page || 0)
  const isIncome = req.query.isIncome && (
    req.query.isIncome === 'null' ? null : req.query.isIncome
  ) || null
  const start = req.query.start && (
    req.query.start === 'null' ? null : req.query.start
  ) || null
  const end = req.query.end && (
    req.query.end === 'null' ? null : req.query.end
  ) || null

  let filteredList = list

  if (start && end) {
    filteredList = filteredList.filter(transaction => {
      const date = dayjs(transaction.date);
      const _start = dayjs(start).startOf('day');
      const _end = dayjs(end).endOf('day');
      return (date.isAfter(_start) && date.isBefore(_end)) || date.isSame(_start) || date.isSame(_end)
    })
  }

  if (isIncome !== null) {
    filteredList = filteredList.filter(transaction => String(transaction.isIncome) === isIncome)
  }

  res.send(getSuccessResponse({
    list: filteredList.slice(page * count, (page + 1) * count),
    total: filteredList.length,
    page
  }));
}

module.exports.getById = (req, res) => {
  const db = new Database();
  const list = db.getAll('transactions')
  const transaction = list.find(transaction => transaction.id === req.params.id)

  if (transaction) {
    res.send(getSuccessResponse({ transaction }));
  } else {
    res.status(404).send(getErrorResponse('The transaction could not be found.'));
  }
}

module.exports.send = (req, res) => {
  const { walletId, amount, sendTo } = req.body
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
    return
  }

  const db = new Database();
  const list = db.getAll('wallets')
  const wallet = list.find(wallet => wallet.id === walletId)

  if (!wallet) {
    res.status(500).end();
    return
  }
  if (Number(amount) > wallet.value) {
    res.status(401).send(getErrorResponse('Not enough funds to complete this transaction.'))
  } else {
    const newWallet = { ...wallet, value: Number((wallet.value - amount).toFixed(6)) }
    const newTransaction = {
      user_id: userInfo.id,
      date: new Date(),
      isIncome: false,
      status: 'PENDING',
      from_token: newWallet.token,
      to_token: sendTo,
      coin_id: wallet.coin_id,
      amount
    }

    db.addRowToTable('transactions', newTransaction, (err) => {
      if (err) {
        res.status(401).send(getErrorResponse('Can\'t create transaction'))
        return
      }
      db.editRowInTable('wallets', newWallet, (err, data) => err
        ? res.status(401).send(getErrorResponse())
        : res.send(getSuccessResponse({ refreshedWallet: data }))
      );
    })
  }
}

module.exports.receive = (req, res) => {
  const { walletId, amount } = req.body
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
    return
  }

  const db = new Database();
  const list = db.getAll('wallets')
  const wallet = list.find(wallet => wallet.id === walletId)

  if (!wallet) {
    res.status(500).end();
    return
  }

  const newWallet = { ...wallet, value: Number((wallet.value + Number(amount)).toFixed(6)) }
  const newTransaction = {
    user_id: userInfo.id,
    date: new Date(),
    isIncome: true,
    status: 'DONE',
    from_token: 'generous server',
    to_token: newWallet.token,
    coin_id: wallet.coin_id,
    amount
  }

  db.addRowToTable('transactions', newTransaction, (err) => {
    if (err) {
      res.status(401).send(getErrorResponse('Can\'t create transaction'))
      return
    }
    db.editRowInTable('wallets', newWallet, (err, data) => err
      ? res.status(401).send(getErrorResponse())
      : res.send(getSuccessResponse({ refreshedWallet: data }))
    );
  })
}

module.exports.reject = (req, res) => {
  const { id } = req.params
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
    return
  }

  const db = new Database();
  const list = db.getAll('transactions')
  const transaction = list.find(t => t.id === id)

  if (!transaction) {
    res.status(500).end();
    return
  }

  const editedTransaction = {
    ...transaction,
    status: 'REJECTED'
  }

  db.editRowInTable('transactions', editedTransaction, (err, data) => {
    if (err) {
      res.status(401).send(getErrorResponse('Can\'t reject transaction'))
      return
    }
    res.send(getSuccessResponse({ transaction: data }))
  })
}

module.exports.remove = (req, res) => {
  const { id } = req.params
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
    return
  }

  const db = new Database();
  const list = db.getAll('transactions')
  const transaction = list.find(t => t.id === id)

  if (!transaction) {
    res.status(500).end();
    return
  }

  db.deleteRowInTable('transactions', id, (err, data) => {
    if (err) {
      res.status(401).send(getErrorResponse('Can\'t reject transaction'))
      return
    }
    res.send(getSuccessResponse(true))
  })
}
