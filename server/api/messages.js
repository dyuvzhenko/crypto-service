const { getErrorResponse, getSuccessResponse } = require('../utils/response-template');
const { Database } = require('../database');

module.exports.getAll = (req, res) => {
  const db = new Database();
  res.send(getSuccessResponse({ messages: db.getAll('messages') }));
}
