const { getErrorResponse, getSuccessResponse } = require('../utils/response-template');
const { decodeTokenFromHeaders } = require('./auth');
const { Database } = require('../database');

module.exports.getAll = (req, res) => {
  const db = new Database();
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
  } else {
    const balance = db.getAll('userBalances').find(({ user_id }) => user_id === userInfo.id)
    if (balance) {
      res.send(getSuccessResponse({ usd: balance.usd, eur: balance.eur }));
    } else {
      res.status(404).send(getErrorResponse());
    }
  }
}

module.exports.getMoney = (req, res) => {
  const db = new Database();
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
  } else {
    const balance = db.getAll('userBalances').find(({ user_id }) => user_id === userInfo.id)
    if (balance) {
      const newBalance = { ...balance, usd: balance.usd + 1000, eur: balance.eur + 1000 };
      db.editRowInTable('userBalances', newBalance, (err, data) => err
        ? res.status(401).send(getErrorResponse())
        : res.send(getSuccessResponse({ usd: data.usd, eur: data.eur }))
      );
    } else {
      res.status(404).send(getErrorResponse());
    }
  }
}
