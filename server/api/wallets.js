const dayjs = require('dayjs');
const { decodeTokenFromHeaders } = require('./auth');
const { getErrorResponse, getSuccessResponse } = require('../utils/response-template');
const { Database } = require('../database');

module.exports.getAll = (req, res) => {
  const db = new Database();
  res.send(getSuccessResponse({ wallets: db.getAll('wallets') }));
}

module.exports.getAllWithHistory = (req, res) => {
  const db = new Database();
  const count = req.query.transactionsCount || 20

  const wallets = db.getAll('wallets');
  const transactions = db.getAll('transactions').sort((a, b) => (new Date(b.date)) - (new Date(a.date)));

  const walletsWithHistory = wallets.map(wallet => ({
    ...wallet,
    lastTransactions: transactions.filter(transaction => transaction.coin_id === wallet.coin_id).slice(0, count)
  }));

  res.send(getSuccessResponse({ list: walletsWithHistory }));
}

module.exports.addWallet = (req, res) => {
  const userInfo = decodeTokenFromHeaders(req.headers)
  if (!userInfo) {
    res.status(401).send(getErrorResponse('Invalid token'));
    return
  }

  const db = new Database();
  const { balanceEffect, purchasedAmount, balanceId, coinId } = req.body;

  const balance = db.getAll('userBalances').find(({ user_id }) => user_id === userInfo.id);
  if (!balance) {
    res.status(404).send(getErrorResponse());
    return
  }

  const editedBalance = {
    ...balance,
    [balanceId]: Number((balance[balanceId] - balanceEffect).toFixed(2))
  }

  const newWallet = {
    user_id: userInfo.id,
    coin_id: coinId,
    token: `wallet-${coinId}-hash`,
    value: Number(purchasedAmount)
  }

  const firstTransaction = {
    user_id: userInfo.id,
    date: dayjs().subtract(1, 'minutes').toISOString(),
    isIncome: true,
    status: 'DONE',
    from_token: newWallet.token,
    to_token: newWallet.token,
    coin_id: coinId,
    amount: 0
  }

  const secondTransaction = {
    user_id: userInfo.id,
    date: dayjs().toISOString(),
    isIncome: true,
    status: 'DONE',
    from_token: newWallet.token,
    to_token: newWallet.token,
    coin_id: coinId,
    amount: Number(purchasedAmount)
  }

  db.addRowToTable('wallets', newWallet, (err, data) => {
    if (err) {
      res.status(401).send(getErrorResponse('Can\'t create wallet'))
      return
    }
    db.editRowInTable('userBalances', editedBalance, (err) => {
      if (err) {
        res.status(401).send(getErrorResponse());
        return
      }
      db.addRowToTable('transactions', firstTransaction, (err, createdFirstTransaction) => {
        if (err) {
          res.status(401).send(getErrorResponse())
          return
        }
        db.addRowToTable('transactions', secondTransaction, (err, createdSecondTransaction) => {
          if (err) {
            res.status(401).send(getErrorResponse())
            return
          }
          res.send(getSuccessResponse({
            wallet: {
              ...data,
              lastTransactions: [createdSecondTransaction, createdFirstTransaction]
            }
          }))
        });
      });
    });
  })
}
