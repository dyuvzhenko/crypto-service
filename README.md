# Crypto-service

### Prerequisites
- Install [Git](git-scm.com), [Node.js](nodejs.org), [Yarn](yarnpkg.com).
- The command `yarn install` must be run in both directories, `client` and `server`.
- Credentials to log in: `user@mail.com`/`pass`

### Commands
```bash
$ cd client; yarn start      # Start developing with hot-reloading (port 3001)
$ cd client; yarn analyze    # Start analyzing bundle size
$ cd client; yarn test       # Start tests
```

### Deploy client

- For each deploy static files should be build with new version in package.json (in `client` and `server` folders).
- Clean all files inside `root@{ip}:/var/www/am4dev.com`
- After `yarn build:prod` move to directory `client/build` and run `scp -r * root@{ip}:/var/www/am4dev.com`

### Deploy server

- Just run next command: `git subtree push --prefix server heroku master`

### Prepare environment for client

- Create [droplet](https://www.digitalocean.com/products/droplets/) or another VM.

- Point domain name to the server. Create an `A record` in hosting provider’s DNS settings pointing domain name to the server IP address.

- Install NGINX on VM:

```bash
$ apt-get update
$ apt-get install nginx
```

- Create folder for static

```bash
$ cd /var/www
$ mkdir -p am4dev.com
```

- Add line `font/opentype				  otf;` to `/etc/nginx/mime.types`.

- Remove files `/etc/nginx/sites-available/default` and `/etc/nginx/sites-enabled/default`. Then move file `client/nginx.conf` (named as `default`) to `/etc/nginx/sites-available` and run `ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default`.

- HTTPS. Create `am4dev.crt` (certificate + Intermediate certificate + Root certificate) and `am4dev.key` (Only private key) files and move them to `/etc/ssl/`.

- Reload nignx: `nginx -s reload`.
